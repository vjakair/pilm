/*****************************************************************
 * Proprietary & Confidential  |  © 2017 PhaseZero Ventures LLC  *
 * This is part of PhaseZero Ventures LLC and cannot be copied,  *
 * modified and/or distributed without the express permission of *
 * PhaseZero Ventures LLC                                        *
 *****************************************************************/
/**
 * @author: Irshadahmed
 */
/**
 * Angular 2
 */
import {
    enableDebugTools,
    disableDebugTools
} from '@angular/platform-browser';
import {
    ApplicationRef, /*A reference to an Angular application running on a page.*/
    enableProdMode,
    Provider
} from '@angular/core';
import { CommonUtilService } from './core/commonUtil.service';
import {
    PromotionSettings
} from './core/promotionSettings.provider';
/**
 * Environment Providers
 */
let PROVIDERS: any[] = [
    /**
     * Common env directives
     */
    PromotionSettings
];

/**
 * Angular debug tools in the dev console
 * https://github.com/angular/angular/blob/86405345b781a9dc2438c0fbe3e9409245647019/TOOLS_JS.md
 */
let _decorateModuleRef = <T>(value: T): T => value;

if ('production' === ENV) {
    enableProdMode();

    /**
     * Production
     */
    _decorateModuleRef = (modRef: any) => {
        const commonUtil = modRef.injector.get(CommonUtilService);
        disableDebugTools();
        setTimeout(() => {
            commonUtil.resizeWindow();
        }, 10);
        return modRef;
    };

    PROVIDERS = [
        ...PROVIDERS,
        /**
         * Custom providers in production.
         */
    ];

} else {

    _decorateModuleRef = (modRef: any) => {
        const appRef = modRef.injector.get(ApplicationRef);
        const cmpRef = appRef.components[0];
        const commonUtil = modRef.injector.get(CommonUtilService);
        enableDebugTools(cmpRef);
        setTimeout(() => {
            commonUtil.resizeWindow();
        }, 10);
        return modRef;
    };

    /**
     * Development
     */
    PROVIDERS = [
        ...PROVIDERS,
        /**
         * Custom providers in development.
         */
    ];

}

export const decorateModuleRef = _decorateModuleRef;

export const ENV_PROVIDERS = [
    ...PROVIDERS
];
