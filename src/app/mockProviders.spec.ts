/*****************************************************************
 * Proprietary & Confidential  |  © 2017 PhaseZero Ventures LLC  *
 * This is part of PhaseZero Ventures LLC and cannot be copied,  *
 * modified and/or distributed without the express permission of *
 * PhaseZero Ventures LLC                                        *
 *****************************************************************/
/**
 * @author: Irshadahmed
 */
/**
 * Angular 2
 */
import { AppState } from './app.service';
import { TranslateService } from '@ngx-translate/core';
import { TranslateStore } from '@ngx-translate/core/src/translate.store';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { Observable } from 'rxjs/Observable';

/**
 * Environment Providers
 */
const PROVIDERS: any[] = [
    /**
     * Common translate providers
     */
    AppState,
    TranslateService,
    TranslateStore
];

export const TRANSLATE_PROVIDERS = [
    ...PROVIDERS
];

const translations = {};
class FakeLoader implements TranslateLoader {
    public getTranslation(lang: string): Observable<any> {
        return Observable.of(translations);
    }
}

const IMPORTS: any[] = [
    TranslateModule.forRoot({
        loader: { provide: TranslateLoader, useClass: FakeLoader }
    })

];

export const TRANSLATE_CONFIG = [
    ...IMPORTS
];
