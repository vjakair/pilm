/*****************************************************************
 * Proprietary & Confidential  |  © 2017 PhaseZero Ventures LLC  *
 * This is part of PhaseZero Ventures LLC and cannot be copied,  *
 * modified and/or distributed without the express permission of *
 * PhaseZero Ventures LLC                                        *
 *****************************************************************/
/**
 * @author: Irshadahmed
 */
import {
    AbstractControl,
    ValidatorFn
} from '@angular/forms';

export function forbiddenDateValidator(): ValidatorFn {
    const _pattern: string = `/^(?:(?:31(\/|-|\.)(?:0?[13578]|1[02]))\1|
    (?:(?:29|30)(\/|-|\.)(?:0?[1,3-9]|1[0-2])\2))(?:(?:1[6-9]|[2-9]\d)?\d{2})$|
    ^(?:29(\/|-|\.)0?2\3(?:(?:(?:1[6-9]|[2-9]\d)?(?:0[48]|[2468][048]|[13579][26])|
    (?:(?:16|[2468][048]|[3579][26])00))))$|^(?:0?[1-9]|1\d|2[0-8])(\/|-|\.)
    (?:(?:0?[1-9])|(?:1[0-2]))\4(?:(?:1[6-9]|[2-9]\d)?\d{2})$/`;
    return (control: AbstractControl): { [key: string]: any } => {
        const val: string = control.value;
        const _dateRegEx: RegExp = new RegExp(_pattern);
        if ('string' !== typeof val) {
            return null;
        }
        const values: string[] = val.split('/');
        let isDateValid: boolean = false;
        if (values instanceof Array && 3 === values.length) {
            const dateStr = values[1] + '/' + values[0] + '/' + values[2];
            isDateValid = _dateRegEx.test(dateStr);
        }
        return isDateValid ? null : { forbiddenDate: { value: val } };
    };
}
