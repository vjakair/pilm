/*****************************************************************
 * Proprietary & Confidential  |  © 2017 PhaseZero Ventures LLC  *
 * This is part of PhaseZero Ventures LLC and cannot be copied,  *
 * modified and/or distributed without the express permission of *
 * PhaseZero Ventures LLC                                        *
 *****************************************************************/
/**
 * @author: Irshadahmed
 */
export class SubscriptionHistory {
    public subscriptions: any[] = [];
    public formFields: any[] = [];
    public editableFormFields: any[] = [];
    public hasEditable: boolean = true;
    public setFormFields(formFields: any): void {
        this.formFields = formFields;
    }
    public setEditableFormFields(formFields: any, editable: boolean): void {
        // console.log("formFields => (%o) and editable =>(%o)", formFields, editable);
        this.editableFormFields = formFields;
        this.hasEditable = editable;
    }
    public unsubscribe() {
        this.subscriptions.forEach((subscription: any) => {
            subscription.unsubscribe();
        });
    }
    protected _hasFormField(fieldName: string): boolean {
        try {
            return -1 !== this.formFields.indexOf(fieldName);
        } catch (e) {
            return true;
        }
    }
    protected _hasEditableField(fieldName: string): boolean {
        if (!this.hasEditable) {
            return true;
        }
        try {
            return this.editableFormFields.indexOf(fieldName) !== -1;
        } catch (e) {
            return false;
        }
    }
}
