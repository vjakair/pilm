/*****************************************************************
 * Proprietary & Confidential  |  © 2017 PhaseZero Ventures LLC  *
 * This is part of PhaseZero Ventures LLC and cannot be copied,  *
 * modified and/or distributed without the express permission of *
 * PhaseZero Ventures LLC                                        *
 *****************************************************************/
/**
 * @author: Irshadahmed
 */
import CryptoJS from 'crypto-js';
import {
    CommonUtilService
} from '../core/commonUtil.service';

export class LocalStorageService {
    private _AES: any = CryptoJS.AES;
    private _secKEY: string = BUILD.SECKEY;
    constructor(public commonUtil: CommonUtilService) {
    }
    /**
     * Delete given key from local storage
     * @param key
     */
    public deleteItem(key: string): void {
        localStorage.removeItem(key);
    }
    /**
     * Delete all the items present in local storage
     */
    public deleteAllItems(): void { 
        // localStorage.clear();
        var arr = [];
        for (var i = 0; i < localStorage.length; i++){
            if (localStorage.key(i).substring(0,5) == 'pilm_') {
                arr.push(localStorage.key(i));
            }
        }
        for (var i = 0; i < arr.length; i++) {
            localStorage.removeItem(arr[i]);
        }
    }
    /**
     * Return a give item from local strage
     * @param key
     * @param secure
     */
    public getItem(key: string, secure?: boolean): any {
        if (!this.commonUtil.isValidString(key)) {
            return null;
        }
        if (secure) {
            return this.getSecureItem(key);
        }
        return this.commonUtil.parseJson(localStorage.getItem(key));
    }
    /**
     * Store a given item to local storage
     * @param key
     * @param value
     * @param secure
     */
    public setItem(key: string, value: any, secure?: boolean): void {
        if (!this.commonUtil.isValidString(key)) {
            return;
        }
        if (secure) {
            this.setSecureItem(
                key,
                value
            );
        } else {
            localStorage.setItem(
                key,
                this.commonUtil.isObject(value)
                    ? JSON.stringify(value)
                    : value
            );
        }
    }
    /**
     * Store a given in local storage with encryption
     * @param secureKey
     * @param value
     */
    private setSecureItem(secureKey: string, value: any): void {
        localStorage.setItem(
            secureKey,
            this._AES.encrypt(
                JSON.stringify(value),
                this._secKEY
            )
        );
    }
    /**
     * Return a given item from local storage with decryption
     * @param secureKey
     */
    private getSecureItem(secureKey: string): any {
        const ciphertext: string = localStorage.getItem(secureKey);
        if (!this.commonUtil.isValidString(ciphertext)) {
            return null;
        }
        try {
            const bytes = this._AES.decrypt(
                ciphertext.toString(),
                this._secKEY
            );
            const plaintext = bytes.toString(CryptoJS.enc.Utf8);
            const item: any = this.commonUtil.parseJson(
                plaintext
            );
            return item;
        } catch (e) {
            return null;
        }
    }
}
