/*****************************************************************
 * Proprietary & Confidential  |  © 2017 PhaseZero Ventures LLC  *
 * This is part of PhaseZero Ventures LLC and cannot be copied,  *
 * modified and/or distributed without the express permission of *
 * PhaseZero Ventures LLC                                        *
 *****************************************************************/
/**
 * @author: Irshadahmed
 */
import {
    Component,
    OnInit,
    OnDestroy,
    ChangeDetectorRef,
    ViewChild
} from '@angular/core';
import {
    NgForm
} from '@angular/forms';
import {
    ActivatedRoute
} from '@angular/router';
import {
    CommonUtilService
} from '../../core/commonUtil.service';
import {
    EventEmitterService
} from '../../core/event-emitter';
import {
    NavigationService
} from '../../core/navigation.service';
import {
    PromotionService
} from '../promotions.service';
import {
    SubscriptionHistory
} from '../../helper/subscriptionHistory.helper';
import {
    TitleService
} from '../../core/title';
@Component({
    selector: 'configure-rules',
    templateUrl: './configureRules.component.html',
    styleUrls: ['./configureRules.style.scss']
})
export class ConfigureRulesComponent extends SubscriptionHistory implements OnInit, OnDestroy {
    public isTemplateReady: boolean = false;
    public targetProdType: string = 'byList';
    public promoDetails: any = {
        multiUse: true
    };
    @ViewChild('promoFrm')
    public promoFrm: NgForm;
    public promoValPattern: string;
    public maxVal: number;
    public frmControls: any = {
        VALUE: true,
        THRESHOLD: true
    };
    constructor(
        private _changeRef: ChangeDetectorRef,
        private _commonUtil: CommonUtilService,
        private _eventEmitter: EventEmitterService,
        private _navigationSvc: NavigationService,
        private _promoSvc: PromotionService,
        private _route: ActivatedRoute,
        titleSvc: TitleService
    ) {
        super();
        titleSvc.setTitle('CONFIGURE_RULES');
    }
    public ngOnInit() {
        this._promoSvc.resetTargetProdList();
        this._promoSvc.unSetOrderList();
        this._subscribeEvents();
    }
    public hasFormField(fieldName: string) {
        return super._hasFormField(fieldName);
    }
    public hasEditableField(fieldName: string) {
        return super._hasEditableField(fieldName);
    }
    public ngOnDestroy() {
        this.unsubscribe();
        this._promoSvc.setTargetProdList('byList', []);
    }
    public onMultiUse():void {
        this.promoDetails.threshold = null;
        this._changeRef.detectChanges();
    }
    public removeFormField(fieldName): void {
        this.frmControls[fieldName] = false;
    }
    public firstOrderChange(): void {
        if (this.promoDetails.firstOrder) {
            this.promoDetails.multiUse = true;
            this.promoDetails.threshold = 1;
        } else {
            this.promoDetails.threshold = null;
        }
    }
    private _subscribeEvents() {
        // Set tempalte ready flag
        this.subscriptions.push(
            this._promoSvc
                .onTemplateReady
                .subscribe((isTemplateReady: boolean) => {
                    if (!isTemplateReady) {
                        return;
                    }
                    const templateFields: any[] = this._promoSvc.getTemplateFields('rules');
                    this.setFormFields(templateFields);
                    this.isTemplateReady = isTemplateReady;
                    const targetUrl: string = this._navigationSvc.targetPageUrl;
                    if (-1 !== targetUrl.indexOf('/by-category')) {
                        this.targetProdType = 'byCategory';
                    } else if (-1 !== targetUrl.indexOf('/by-brand')) {
                        this.targetProdType = 'byBrand';
                    }
                })
        );
        this.subscriptions.push(
            this._promoSvc
                .onPromotionLoaded
                .subscribe((isPromoLoaded: boolean) => {
                    if (isPromoLoaded) {
                        this.setEditableFormFields(this._promoSvc.getEditableFields('rules'),
                            this._promoSvc.isPromoConfigEditable());
                        this._setPromoDetails();
                    }
                })
        );
        // Validate promotion form if save button is clicked
        this.subscriptions.push(
            this._eventEmitter
                .onValidatePromoDetails
                .subscribe(() => this._validatePromoDetails())
        );
    }
    /**
     * @todo: Irshad
     */
    private _navigateToSubView(): void {
        /*         const targetUrl: string = this._navigationSvc.targetPageUrl;
                const isByList: boolean = -1 !== targetUrl.indexOf('/by-list');
                const isByCat: boolean = -1 !== targetUrl.indexOf('/by-list');
                this._navigationSvc.navigate(['../']);
         */
        if (!this._promoSvc.hasField('rules', 'BY_LIST')) {
            this._navigationSvc.navigate('./by-category', this._route);
            return;
        }
        if (this._promoSvc.hasField('rules', 'BY_CATEGORY')) {
            return;
        }
    }
    private _setPromoDetails(): void {
        const promoDetails = this._promoSvc.getPromoDetails();
        if (!this._commonUtil.isObject(promoDetails)) {
            return;
        }
        for (const key in promoDetails) {
            if (
                !promoDetails.hasOwnProperty(key)
            ) {
                continue;
            }

            if ('targetProductsModel' === key) {
                const targetProductsModel: any = promoDetails[key];
                if (!this._commonUtil.isObject(targetProductsModel)) {
                    continue;
                }
                if (this._commonUtil.isValidArray(targetProductsModel.byCategory)) {
                    this.targetProdType = 'byCategory';
                    this._navigationSvc.navigate('./by-category', undefined, this._route);
                } else if (this._commonUtil.isValidArray(targetProductsModel.byBrand)) {
                    this.targetProdType = 'byBrand';
                    this._navigationSvc.navigate('./by-brand', undefined, this._route);
                }
            }

            if ('multiUse' === key) {
                this.promoDetails[key] = false === promoDetails[key] ? false : true;
            } else {
                this.promoDetails[key] = promoDetails[key];
            }
        }
        const promoType: string = this._promoSvc.promotionType;
        if ('% OFF Promotion' === promoType) {
            this.promoValPattern = '^[1-9][0-9]?$|^100$';
            this.maxVal = 100;
        }
    }
    private _validatePromoDetails(): void {
        let valid: boolean = !this.promoFrm.invalid;
        if (this.promoDetails.multiUse && 10 < this.promoDetails.threshold) {
            valid = false;
        }
        if (!valid) {
            this._commonUtil.setFormDirty(this.promoFrm.controls);
            return;
        }
        const targetProdList: any = this._promoSvc.getTargetProdList(this.targetProdType);
        const targetProductsModel = {};
        if ('byList' === this.targetProdType) {
            if (!this._commonUtil.isValidArray(targetProdList)) {
                this._eventEmitter
                    .onAlert
                    .emit({
                        type: 'alert-warning',
                        msgKey: 'CONFIGURE_RULES.ALERTS.SEL_LIST'
                    });
                return;
            }
            targetProductsModel[this.targetProdType] = {
                listId: targetProdList[0],
                parts: null
            };
        } else if ('byCategory' === this.targetProdType) {
            const byCat: any[] = [];
            let totalCounnt = 0;
            if (!this._commonUtil.isValidArray(targetProdList)) {
                this._eventEmitter
                    .onAlert
                    .emit({
                        type: 'alert-warning',
                        msgKey: 'CONFIGURE_RULES.ALERTS.SEL_PROD_LIST'
                    });
                return;
            }
            targetProdList.map((prodList) => {
                if (
                    !this._commonUtil.isObject(prodList) ||
                    !this._commonUtil.isValidArray(prodList.lobList)) {
                    return;
                }
                if (this.lookupAllPartSelected(prodList)) {
                    byCat.push({
                        lob: 'ALL',
                        productClass: 'ALL',
                        productLines: ['ALL'],
                        exclude: [],
                        category: []
                    });
                } else {
                    prodList.lobList.map((cat1) => {
                        if (cat1.selected) {
                            cat1.children.map((cat2) => {
                                const prodLine = {lob: cat1.code, productClass: '', productLines: [], exclude: []};
                                if (cat2.selected) {
                                    prodLine.productClass = cat2.code;
                                    prodLine.productLines = [];
                                    if (this._commonUtil.isValidArray(cat2.exclude)) {
                                        prodLine.exclude = cat2.exclude;
                                    }
                                    cat2.children.map((cat3) => {
                                        if (cat3.selected) {
                                            prodLine.productLines.push(cat3.code);
                                        }
                                    });
                                }
                                if (prodLine.productClass !== '') {
                                    byCat.push(prodLine);
                                }
                            });
                        }
                    });
                }
            });

            if (byCat.length === 0) {
                this._eventEmitter
                    .onAlert
                    .emit({
                        type: 'alert-warning',
                        msgKey: 'CONFIGURE_RULES.ALERTS.SEL_PROD_LIST'
                    });
                return;
            }
            targetProductsModel[this.targetProdType] = byCat;
        } else if ('byBrand' === this.targetProdType) {
            if (!this._commonUtil.isValidArray(targetProdList)) {
                this._eventEmitter
                    .onAlert
                    .emit({
                        type: 'alert-warning',
                        msgKey: 'CONFIGURE_RULES.ALERTS.SEL_LIST'
                    });
                return;
            }
            targetProductsModel[this.targetProdType] = targetProdList;
        }
        this.promoDetails.targetProductsModel = targetProductsModel;
        this.promoDetails.threshold = parseInt(this.promoDetails.threshold, 0);
        this.promoDetails.qualifyingAmount = parseFloat(this.promoDetails.qualifyingAmount);
        if (!this.promoDetails.multiUse) {
            this.promoDetails.threshold = null;
        }
        this._promoSvc.updateLocalPromoDraft(this.promoDetails);
        this._eventEmitter
            .onPromoSave
            .emit(true);
    }

    private lookupAllPartSelected(prodList): boolean {
        const category = prodList.category;
        if (category.length !== prodList.lobList.length) {
            return false;
        } else {
            for (const cat1 of prodList.lobList) {
                for (const cat1bk of category) {
                    if (this._commonUtil.isValidArray(cat1bk.children) &&
                        this._commonUtil.isValidArray(cat1.children)
                    ) {
                        if (cat1bk.children.length === cat1.children.length) {
                            if (this._commonUtil.isValidArray(cat1bk.children) &&
                                this._commonUtil.isValidArray(cat1.children)
                            ) {
                                for (const cat2 of prodList.lobList) {
                                    for (const cat2bk of category) {
                                        if (this._commonUtil.isValidArray(cat2bk.children) &&
                                            this._commonUtil.isValidArray(cat2.children)
                                        ) {
                                        } else {
                                            return false;
                                        }
                                    }
                                }
                            }
                        } else {
                            return false;
                        }
                    }
                }
            }
        }
        for (const cat1 of prodList.lobList) {
            if (!cat1.selected) {
                return false;
            } else {
                if (this._commonUtil.isValidArray(cat1.children)) {
                    for (const cat2 of cat1.children) {
                        if (!cat2.selected) {
                            return false;
                        } else {
                            if (this._commonUtil.isValidArray(cat2.children)) {
                                for (const cat3 of cat2.children) {
                                    if (!cat3.selected) {
                                        return false;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        return true;
    }
}
