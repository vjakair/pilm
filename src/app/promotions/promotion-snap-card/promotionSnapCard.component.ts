/*****************************************************************
 * Proprietary & Confidential  |  © 2017 PhaseZero Ventures LLC  *
 * This is part of PhaseZero Ventures LLC and cannot be copied,  *
 * modified and/or distributed without the express permission of *
 * PhaseZero Ventures LLC                                        *
 *****************************************************************/
/**
 * @author: Irshadahmed
 */
import {
    Component,
    ElementRef,
    EventEmitter,
    Input,
    Output,
    AfterViewInit
} from '@angular/core';
// Import Custom Services
import {
    NavigationService
} from '../../core/navigation.service';
import {
    PromotionService
} from '../promotions.service';

@Component({
    selector: 'promotion-snap-card',
    template: `
        <div class="promo-snap clickable card col-md-3 pull-left"
        (click)="selectPromotion($event)"
        [ngClass]="{'apply-border': applyBorder, 'active': promoStatus === promotion.status}">
            <h5>{{'HOME.SNAPSHOT.'+promotion.key | translate}}
                <i *ngIf="expandLink" class="expand pull-right"></i>
            </h5>
            <img class="pull-left"
            src="assets/img/{{getPromoImagePath(promotion.status)}}" />
            <span class="count pull-right">
                {{promotion.count}}
            </span>
        </div>
    `,
    styleUrls: ['./promotionSnapCard.style.scss']
})
export class PromotionSnapCardComponent implements AfterViewInit {
    @Input()
    public applyBorder: boolean;
    @Input()
    public expandLink: boolean;
    public key: string = null;
    @Input()
    public promotion: any;
    @Input()
    public promoStatus: string;
    @Output()
    public promoSnapSelected: EventEmitter<any> = new EventEmitter();
    constructor(
        private _eleRef: ElementRef,
        private _promotionSvc: PromotionService,
        private _navigation: NavigationService
    ) {
    }
    public ngAfterViewInit() {
        if (this.promoStatus === this.promotion.status) {
            this._eleRef.nativeElement.scrollIntoView();
        }
    }
    public getPromoImagePath(key: string): string {
        return this._promotionSvc.getPromoImagePath(key);
    }
    public selectPromotion($event: any): void {
        /* if (this.expandLink) {
            const className: string = $event.target.className;
            if (-1 === className.indexOf('expand')) {
                return;
            }
        } */
        this.promoSnapSelected.emit(this.promotion);
    }
}
