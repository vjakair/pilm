/*****************************************************************
 * Proprietary & Confidential  |  © 2017 PhaseZero Ventures LLC  *
 * This is part of PhaseZero Ventures LLC and cannot be copied,  *
 * modified and/or distributed without the express permission of *
 * PhaseZero Ventures LLC                                        *
 *****************************************************************/
/**
 * @author: Irshadahmed
 */
import {
    Component,
    EventEmitter,
    Input,
    Output
} from '@angular/core';
@Component({
    selector: 'template-card',
    template: `
        <div class="template-card card {{template.type}}"
        [ngClass]="{'active': template.selected}"
        (click)="selectTemplate.emit(template)">
            <div class="template-img"></div>
            <div class="template-title" translate="{{template.templateName}}"></div>
        </div>
    `,
    styleUrls: ['./templateCard.style.scss']
})
export class TemplateCardComponent {
    @Input()
    public template: any;
    @Output()
    public selectTemplate: EventEmitter<any> = new EventEmitter();
}
