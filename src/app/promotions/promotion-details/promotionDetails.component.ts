/*****************************************************************
 * Proprietary & Confidential  |  © 2017 PhaseZero Ventures LLC  *
 * This is part of PhaseZero Ventures LLC and cannot be copied,  *
 * modified and/or distributed without the express permission of *
 * PhaseZero Ventures LLC                                        *
 *****************************************************************/
/**
 * @author: Irshadahmed
 */
// import * as mockData from '../../../assets/data.json';
import {
    ComponentRef,
    EventEmitter,
    ViewEncapsulation,
    Component,
    OnDestroy
} from '@angular/core';
import {
    BsModalService
} from 'ngx-bootstrap/modal';
import {
    BsModalRef
} from 'ngx-bootstrap/modal/modal-options.class';
import {
    AuthService
} from '../../auth/auth.service';
import {
    ActionModalComponent
} from '../action-modal/actionModal.component';
import {
    AddNoteModalComponent
} from '../add-note-modal/addNoteModal.component';
import {
    EmailPreviewModalComponent
} from '../email-preview-modal/emailPreviewModal.component';
import {
    EventEmitterService
} from '../../core/event-emitter';
import {
    Location
} from '@angular/common';

import {
    CommonUtilService
} from '../../core/commonUtil.service';
import {
    NavigationService
} from '../../core/navigation.service';
import {
    PartListModalComponent
} from '../part-list-modal/partListModal.component';
import {
    PreviewImagesComponent
} from '../../components/preview-images/previewImages.component';
import {
    PromotionService
} from '../promotions.service';
import {
    SubscriptionHistory
} from '../../helper/subscriptionHistory.helper';
import {
    TitleService
} from '../../core/title';
import {
    ViewCustListModalComponent
} from '../view-cust-list-modal/viewCustListModal.component';
import {
    DateUtilService
} from '../../core/dateUtil.service';
@Component({
    selector: 'promo-details',
    templateUrl: './promotionDetails.component.html',
    styleUrls: ['promotionDetails.style.scss'],
    encapsulation: ViewEncapsulation.None
})
export class PromotionDetailsComponent extends SubscriptionHistory implements OnDestroy {
    public actionModal: BsModalRef;
    public approvalPendingGroupId: string;
    public approvalProcess: boolean;
    public catCards: any[];
    public brandCards: any[];
    public emailAttachment: string;
    public emailBody: string;
    public emailSubject: string;
    public preivewImages: any[];
    public promoApprovers: any[];
    public promoDetails: any;
    public promoNotes: any[] = [
    ];
    public rejectedGroupId:string;
    public showLongDesc: boolean;
    public status: any = {
        approversList: {
            isOpen: false
        },
        fyiList: {
            isOpen: false
        },
        notes: {
            isOpen: false
        },
        productList: {
            isOpen: false
        },
        targetMarket: {
            isOpen: false
        }
    };
    public promoCode: string;
    public stepSettings: any;
    public targetMarket: any;
    public targetProdCategory: any;
    private _notificationId: string;
    private _promotionId: string;
    private _section: string;
    private _templateId: string;
    private onActionComplete: EventEmitter<any> = new EventEmitter();
    constructor(
        private _authService: AuthService,
        private _commonUtil: CommonUtilService,
        private _eventEmitter: EventEmitterService,
        public location: Location,
        private _modalService: BsModalService,
        private _navigation: NavigationService,
        private _promotionSvc: PromotionService,
        private _titleSvc: TitleService,
        private _dateUtilSvc: DateUtilService
    ) {
        super();
        _navigation.toggleMainContainer(true);
        _titleSvc.setTitle('PROMOTION_DETAILS');
        this._promotionId = _navigation.getRouteParam('promoId');
        this._notificationId = _navigation.getRouteParam('notificationId');
        this._setPromotionDetails();
        this._subscribeEvents();
    }
    public ngOnDestroy() {
        this._navigation.toggleMainContainer(false);
        this.unsubscribe();
    }
    public hasFormField(fieldName: string) {
        return this._hasFormField(fieldName);
    }
    public addNewNote() {
        const addNoteModal: BsModalRef = this._modalService
            .show(AddNoteModalComponent, {
                class: 'pilm-popup action-modal'
            });
        addNoteModal.content.promoId = this._promotionId;
        addNoteModal.content.onNoteAdded
            .subscribe(
            (note: any) => this._onNoteAdded(note)
            );
    }
    public onAction(button: any): void {
        const action: string = button.action;
        const actionKey: string = button.actionKey;
        if ('edit' === action) {
            this._navigateToCreatePromotion();
        } else {
            this._showModal();
            this.actionModal.content.reasonReqd = 'approved' !== action;
            this.actionModal.content.actionType = action;
            this.actionModal.content.actionKey = actionKey;
        }
    }
    public preivewEmailBody(): void {
        const emailModal: BsModalRef = this._modalService.show(
            EmailPreviewModalComponent, {
                class: 'pilm-popup email-preview'
            }
        );
        emailModal.content.emailSubject = this.emailSubject;
        emailModal.content.emailBody = this.emailBody;
    }
    public previewPromoImages(): void {
        const previewImgModal: BsModalRef = this._modalService.show(
            PreviewImagesComponent, {
                class: 'modal-lg pilm-popup promo-images'
            }
        );
        previewImgModal.content.setImagePreview(this.preivewImages);
    }
    public showNotes(): void {
        this._section = 'notes';
        if (this._commonUtil.isValidArray(this.promoNotes)) {
            this._toggleSelectedSection();
            return;
        }
        this._promotionSvc
            .getPromoNotes(this._promotionId)
            .subscribe((response: any): void => {
                if (response.errorCode) {
                    this._eventEmitter
                        .onAlert
                        .emit({
                            type: 'alert-warning',
                            msgKey: 'PROMOTION.ERROR.NOTES_ERR'
                        });
                    return;
                }
                if (this._commonUtil.isValidArray(response)) {
                    this.promoNotes = response;
                    this._toggleSelectedSection();
                } else {
                    this._eventEmitter
                        .onAlert
                        .emit({
                            type: 'alert-warning',
                            msgKey: 'PROMOTION.ERROR.NOTES_NOT_AVL'
                        });
                }
            });
    }
    public showParts(part= []): void {
        let cls: string = 'pilm-popup part-list-modal';
        let parts: any[] = this.targetProdCategory.parts;
        if (this._commonUtil.isValidArray(part)) {
            parts = part;
        }
        if (8 < parts.length) {
            cls += ' full-screen';
        }
        const partListMod: BsModalRef = this._modalService.show(PartListModalComponent, {
            class: cls
        });
        partListMod.content.titleKey = 'PROMOTION_DETAILS.PART_LIST';
        partListMod.content.parts = parts;
    }
    public showTargetProdCategory(): void {
        this._section = 'productList';
        if (!this._commonUtil.isObject(this.targetProdCategory)) {
            this._eventEmitter
                .onAlert
                .emit({
                    type: 'alert-warning',
                    msgKey: 'PROMOTION_DETAILS.MESSAGES.PROD_LIST'
                });
            return;
        }

        if (this._commonUtil.isValidArray(this.targetProdCategory.parts)) {
            this._toggleSelectedSection();
            return;
        }

        const selectedOption: string = this.targetProdCategory.option;
        if ('byBrand' === selectedOption) {
            this._setBrands();
        } else if ('byCategory' === selectedOption) {
            this._setProductCategories();
        } else {
            this._setProductList();
        }
    }

    public showPromoApprovers(): void {
        this._section = 'approversList';
        if (this._commonUtil.isValidArray(this.promoApprovers)) {
            this._toggleSelectedSection();
        } else {
            this._setApproverGroups(true);
        }
    }

    public showTargetMarket(): void {
        try {
            const targetMarket: any = this.promoDetails.targets.targetMarket;
            if (!this._commonUtil.isObject(targetMarket)) {
                this._eventEmitter
                    .onAlert
                    .emit({
                        type: 'alert-warning',
                        msgKey: 'PROMOTION_DETAILS.MESSAGES.TM_INFO_NOT_AVL'
                    });
                return;
            }
        } catch (e) {
            return;
        }
        this._section = 'targetMarket';
        if (this._commonUtil.isObject(this.targetMarket)) {
            this._toggleSelectedSection();
            return;
        }
        this._setTargetMarketData();
    }

    public viewSelectedCustomers(): void {
        let customers: any[];
        try {
            customers = this.promoDetails.targets.targetMarket.customers;
        } catch (e) {
            return;
        }
        if (!this._commonUtil.isValidArray(customers)) {
            return;
        }
        const custListModal: BsModalRef = this._modalService.show(ViewCustListModalComponent, {
            class: 'pilm-popup cust-list-modal'
        });
        custListModal.content.customers = customers;
    }

    public viewExclusionList(parts: any[]): void {
        if (!this._commonUtil.isValidArray(parts)) {
            return;
        }
        this.targetProdCategory.parts = parts;
        this.showParts();
    }

    private _getActionButtons(stepButtons: any[], labels: any[]): any[] {
        const buttons: any[] = [];
        if (!this._commonUtil.isValidArray(stepButtons)) {
            return;
        }
        stepButtons
            .forEach((button: any) => {
                if (
                    !this._commonUtil.isObject(button) ||
                    -1 === labels.indexOf(button.text)
                ) {
                    return;
                }
                if (this._authService.userHasPermissions(button.permission)) {
                    buttons.push(button);
                }
            });
        return buttons;
    }

    private _navigateToCreatePromotion(): void {
        const stage: string = this.promoDetails.stage;
       /* if(this.promoDetails.status === 'LAUNCHED'){
            stage = 'DEFINE_PROMOTION';
        }*/
        const stgComplete: boolean = this.promoDetails.stgComplete;
        let stepNo: number = this._promotionSvc.getCurrentStepByStage(stage);
        if (4 > stepNo && stgComplete) { //  && this.promoDetails.status !== 'LAUNCHED'
            stepNo = stepNo + 1;
        }
        const stepSettings: any = this._promotionSvc.getStepSettingsByStepNo(stepNo);
        if (!this._commonUtil.isObject(stepSettings)) {
            return;
        }
        const tplId: number = this.promoDetails.templateId;
        const routerLink: string = stepSettings.currentRouterLink;
        const navUrl: string = this._commonUtil.replaceUrlParams(
            routerLink,
            {
                promotionId: this._promotionId,
                templateId: tplId
            }
        );
        this._navigation.navigate(navUrl);
    }

    private _onNoteAdded(note: any): void {
        if (!this._commonUtil.isObject(note) ||
            !this._commonUtil.isValidArray(this.promoNotes)
        ) {
            return;
        }
        this.promoNotes.unshift(note);
    }

    private _setPromotionDefinition() {
        this._promotionSvc
            .getTemplateDefinition(this._templateId)
            .subscribe((response) => {
                // response = mockData.template;
                // console.log('############# response #############', response);

                if (response.errorCode || 'error' === response.type) {
                    return;
                }
                let tplFrmFields: any[] = [];
                const templateFields: any = response.templateFields;
                for (const prop in templateFields) {
                    if (!templateFields.hasOwnProperty(prop)) {
                        continue;
                    }
                    const formFields: any[] = templateFields[prop];
                    if (this._commonUtil.isValidArray(formFields)) {
                        tplFrmFields = tplFrmFields.concat(formFields);
                    }
                }
                this.setFormFields(tplFrmFields);
            });
    }

    private _setApproverGroups(toggle: boolean = false): void {
        this._promotionSvc
            .getApproverGroupData()
            .subscribe((approverGroups: any[]): void => {
                const promoApprovers: any[] = this._promotionSvc
                    .getSelectedApprovers();
                if (this._commonUtil.isValidArray(promoApprovers)) {
                    this._setApprovalStage(promoApprovers);
                    this.promoApprovers = promoApprovers;
                    if (toggle) {
                        this._toggleSelectedSection();
                    }
                } else {
                    this.promoApprovers = undefined;
                }
            }, (error: any) => {
                this._eventEmitter
                    .onAlert
                    .emit({
                        type: 'alert-warning',
                        msgKey: 'PROMOTION.ERROR.SERVER'
                    });
            });
    }

    private _setApprovalStage(promoApprovers: any[]): void {
        const promoStatus: string = this.promoDetails.status;
        let approvedGroup: any;
        if ('WAITING_FOR_LAUNCH' === promoStatus || 'LAUNCHED' === promoStatus) {
            this.approvalProcess = true;
            return;
        }
        this._setApprovalProcessStatus(
            promoApprovers,
            promoStatus
        );
        if (!this.approvalProcess) {
            return;
        }
        for (const promoApprover of promoApprovers) {
            if (!this._commonUtil.isObject(promoApprover)) {
                continue;
            }
            if (this.approvalPendingGroupId === promoApprover.groupId) {
                break;
            }
            approvedGroup = promoApprover;
        }
        this._doSetApprovalStatus(promoStatus, approvedGroup);
    }
    // In case of ARCHIVED / REJECTED / PENDING_APPROVAL
    private _doSetApprovalStatus(promoStatus: string, approvedGroup: any): void {
        const rejectedGroupId:string = this.promoDetails.candidateGroups.rejectedGroup;
        if (this._commonUtil.isObject(approvedGroup)) {
            approvedGroup.status = 'APPROVED';
        } else {
            if ('PENDING_APPROVAL' === promoStatus) {
                this.approvalProcess = false;
            } else {
                if ('ARCHIVED' === promoStatus) {
                    try {
                        const approvedGroups: any[] = this.promoDetails.candidateGroups.approvedGroups;
                        this.approvalProcess = approvedGroups.length ? true : false;
                    } catch (e) {
                        this.approvalProcess = false;
                    }
                } else {
                    this.approvalProcess = false;
                }
            }
        }
        if (this._commonUtil.isValidString(rejectedGroupId)) {
            this.rejectedGroupId = rejectedGroupId;
        }
    }

    private _setApprovalProcessStatus(
        promoApprovers: any[],
        promoStatus: string
    ): void {
        if (!this._commonUtil.isValidArray(promoApprovers)) {
            return;
        }
        if (
            -1 === [
                'ARCHIVED',
                'PENDING_APPROVAL',
                'REJECTED'
            ].indexOf(promoStatus)
        ) {
            this.approvalProcess = false;
            return;
        }
        let pendingGroupId: string;
        let candidateGroups: any = this.promoDetails.candidateGroups;
        if (!this._commonUtil.isObject(candidateGroups)) {
            candidateGroups = {};
        }
        let pendingGroups: string[] = candidateGroups.pendingGroups;
        if (this._commonUtil.isValidArray(pendingGroups)) {
            pendingGroupId = pendingGroups[0];
        } else {
            const promoApprover: any = promoApprovers[0];
            pendingGroupId = promoApprover.groupId;
            candidateGroups.pendingGroups = [
                pendingGroupId
            ];
            this.promoDetails.candidateGroups = candidateGroups;
        }
        this.approvalProcess = true;
        this.approvalPendingGroupId = pendingGroupId;
    }

    private _setApprovedGroupStatus(approvedGroup: any): void {
        if (!this._commonUtil.isObject(approvedGroup)) {
            return;
        }
        const promoStatus: string = this.promoDetails.status.toLowerCase();
        approvedGroup.status = 'rejected' === promoStatus ?
            'REJECTED' : 'APPROVED';
    }

    private _setEmailLink(): void {
        try {
            const email: any = this.promoDetails.targets.email;
            this.emailSubject = email.subject;
            this.emailBody = email.body;
            const attachments: any[] = email.attachments;
            if ( this._commonUtil.isValidArray(attachments) &&
                this._commonUtil.isValidArray(attachments[0].files)) {
                this.emailAttachment = attachments[0].files[0].absolutePath;
            }
        } catch (e) {
            console.log(e);
        }
    }

    private _setLobNames(lobs): void {
        const targetMarket: any = this.promoDetails.targets.targetMarket;
        let tmLobs: any[] = [];
        let tmLobNames: string = '';
        if (
            this._commonUtil.isObject(targetMarket) &&
            this._commonUtil.isValidArray(targetMarket.lineOfBusiness)
        ) {
            tmLobs = targetMarket.lineOfBusiness;
        }
        lobs
            .forEach((lobItem: any) => {
                if (tmLobs.indexOf(lobItem.code)) {
                    tmLobNames += '' === tmLobNames ? lobItem.name : ', ' + lobItem.name;
                }
            });
        if (this._commonUtil.isValidString(tmLobNames)) {
            this.targetMarket.lineOfBusiness = tmLobNames;
        }
    }

    private _setPromoCode(): void {
        const promoDetails: any = this.promoDetails;
        const promoCode: any = promoDetails.headers.promoCode;
        this.promoCode = promoCode.isDefault ? promoCode.defaultCode : promoCode.customCode;
    }

    private _setPromoStatus(): void {
        const promoStatus: string = this.promoDetails.status;
        const promoStage: string = this.promoDetails.stage;
        this._promotionSvc.promoStageObj = {_promoStatus: promoStatus, _promoStage: promoStage};
    }

    private _setPromotionDetails() {
        this._promotionSvc
            .getPromotionById(this._promotionId, this._notificationId)
            .subscribe((response: any) => {
                // response = mockData.promoDetails;
                if (response.errorCode) {
                    this._eventEmitter
                        .onAlert
                        .emit({
                            type: 'alert-warning',
                            msgKey: 'PROMOTION.ERROR.GETTING'
                        });
                    return;
                }
                const promoDetails: any = response;
                if (
                    !this._commonUtil.isObject(promoDetails) ||
                    !this._commonUtil.isObject(promoDetails.headers)
                ) {
                    this._eventEmitter
                        .onAlert
                        .emit({
                            type: 'alert-warning',
                            msgKey: 'PROMOTION_DETAILS.MESSAGES.INVALID_PROMO'
                        });
                    return;
                }
                promoDetails.headers.endDate = this._dateUtilSvc.convertUTCDateToLocalDate(promoDetails.headers.endDate);
                this.promoDetails = promoDetails;
                this._setApproverGroups();
                this._templateId = promoDetails.templateId;
                this._setPromotionDefinition();
                this._setPromoCode();
                this._setPromoStatus();
                this._setTargetProdOption();
                this._setEmailLink();
                this._setupStepSettings(this.promoDetails.status);
                this._setPromotionImages();
            });
    }

    private _setPromotionImages(): void {
        let media: any[];
        const previewImages: any[] = [];
        try {
            media = this.promoDetails.headers.media;
        } catch (e) {
            console.log(e);
            return;
        }
        if (!this._commonUtil.isValidArray(media)) {
            return;
        }
        media.forEach((item: any, index: number) => {
            const mediaFiles: any[] = this._commonUtil.isObject(item) && item.files || [];
            if (!this._commonUtil.isValidArray(mediaFiles)) {
                return;
            }
            const preivewImg: any = {
                label: item.label,
                images: []
            };
            mediaFiles.forEach((file: any) => {
                const imgPath: string = this._commonUtil.isObject(file) && file.absolutePath || undefined;
                if (this._commonUtil.isValidString(imgPath)) {
                    preivewImg.images.push(imgPath);
                }
            });
            if (this._commonUtil.isValidArray(preivewImg.images)) {
                previewImages.push(preivewImg);
            }
        });
        this.preivewImages = previewImages;
    }

    private _setupStepSettings(status: string): void {
        if (!this._commonUtil.isValidString(status)) {
            return;
        }
        if ('ARCHIVED' === status) {
            this.stepSettings = undefined;
            return;
        }
        const actionable: boolean = this.promoDetails.actionable;
        const stepSettings: any = this._promotionSvc
            .getStepSettingsByStepId('PROMO_DETAILS');
        if (!this._commonUtil.isObject(stepSettings)) {
            return;
        }
        const labels: any[] = [];
        // console.log('this._promotionSvc.isPromoConfigEditable() ==>>', this._promotionSvc.isPromoEditable());
        if ('PENDING_APPROVAL' === status && actionable) {
            labels.push(
                'BUTTONS.APPROVE',
                'BUTTONS.ARCHIVE',
                'BUTTONS.REJECT',
                'BUTTONS.RFI',
                'BUTTONS.EDIT'
            );
        } else if ('SAVED_AS_DRAFT' === status) {
            labels.push(
                'BUTTONS.EDIT',
                'BUTTONS.ARCHIVE'
            );
        } else if (this._promotionSvc.isPromoEditable()) {
            labels.push(
                'BUTTONS.ARCHIVE',
                'BUTTONS.EDIT',
            );
        } else {
            labels.push(
                'BUTTONS.ARCHIVE'
            );
        }
        this.stepSettings = {
            BUTTONS: this._getActionButtons(stepSettings.BUTTONS, labels),
            id: 'choose',
            title: 'PROMO_DETAILS'
        };
    }

    private _setTargetMarket(metadata: any): void {
        if (
            this._commonUtil.isObject(this.targetMarket) ||
            !this._commonUtil.isObject(this.promoDetails)
        ) {
            return;
        }
        // this.targetMarket[prop]
        this.targetMarket = {};
        const targetMarket: any = this.promoDetails.targets.targetMarket;
        if (!targetMarket) {
            return;
        }
        for (const prop in metadata) {
            if (
                !metadata.hasOwnProperty(prop)
            ) {
                continue;
            }
            const selectedMetaOptions: any[] = targetMarket[prop];
            const items: any[] = metadata[prop];
            if (!this._commonUtil.isValidArray(selectedMetaOptions)) {
                continue;
            }
            let displayText: string = '';
            items.forEach((item: any) => {
                if (-1 === selectedMetaOptions.indexOf(item.code)) {
                    return;
                }
                displayText += '' === displayText ? item.name : ', ' + item.name;
            });
            this.targetMarket[prop] = displayText;
        }
        const customers: any[] = targetMarket.customers;
        if (this._commonUtil.isValidArray(customers)) {
            this.targetMarket['customers'] = customers;
        }
    }

    private _setTargetProdCategory(lobListTree: any): void {
        if (
            !this._commonUtil.isObject(this.targetProdCategory) ||
            !this._commonUtil.isObject(this.promoDetails) ||
            !this._commonUtil.isValidArray(lobListTree)
        ) {
            return;
        }

        const newCards: any[] = [];
        let catCards: any[];
        try {
            catCards = this.promoDetails.rules.targetProductsModel.byCategory;
            if (!this._commonUtil.isValidArray(catCards)) {
                return;
            }
        } catch (e) {
            return;
        }
        // Traverse lob tree to map names for the selected LOB, CLASSESS, PRODUCT LINES
        lobListTree.forEach((lob) => {
            // Get the lobs added from promo details
            const selectedCatCards: any[] = this._commonUtil.getFilteredItem(
                catCards,
                'lob',
                lob.code,
                true
            );
            if (!this._commonUtil.isValidArray(selectedCatCards)) {
                return;
            }
            selectedCatCards.forEach((selectedCatCard: any): void => {
                if (!this._commonUtil.isObject(selectedCatCard)) {
                    return;
                }
                const selectedCls: any = selectedCatCard.productClass === 'ALL' && lob.children ? lob.children[0] :
                    this._commonUtil.getFilteredItem(
                    lob.children,
                    'code',
                    selectedCatCard.productClass
                    );
                if (!this._commonUtil.isObject(selectedCls)) {
                    return;
                }

                const card: any = {
                    lob: lob.name,
                    class: selectedCls.name,
                    parts: selectedCatCard.exclude
                };
                let selectedProdLines: string = '';

                if (
                    !this._commonUtil.isValidArray(selectedCls.children) ||
                    !this._commonUtil.isValidArray(selectedCatCard.productLines)
                ) {
                    return;
                }

                selectedCls.children.forEach((prodLine: any): void => {
                    if (!this._commonUtil.isObject(prodLine)) {
                        return;
                    }
                    if (-1 !== selectedCatCard.productLines.indexOf(prodLine.code)) {
                        selectedProdLines += '' === selectedProdLines ?
                            prodLine.name : ', ' + prodLine.name;
                    }
                });

                card.productLines = selectedProdLines;
                newCards.push(card);
            });
        });
        this.catCards = newCards;
    }

    private _setTargetMarketData(): void {
        this._promotionSvc
            .getMetadata()
            .subscribe((response: any): void => {
                if (response.errorCode) {
                    this._eventEmitter
                        .onAlert
                        .emit({
                            type: 'alert-warning',
                            msgKey: 'PROMOTION.ERROR.GETTING'
                        });
                } else {
                    this._setTargetMarket(response);
                    this._toggleSelectedSection();
                }
            });
    }

    private _setProductCategories(): void {
        this._promotionSvc
            .getLobList(true)
            .subscribe((response: any): void => {
                if (response.errorCode) {
                    this._eventEmitter
                        .onAlert
                        .emit({
                            type: 'alert-warning',
                            msgKey: 'PROMOTION.ERROR.GETTING'
                        });
                } else {
                    this._setTargetProdCategory(response.cats);
                    this._toggleSelectedSection();
                }
            });
    }

    private _setParts(parts: any[]): void {
        if (!this._commonUtil.isValidArray(parts)) {
            return;
        }
        const targetParts: any[] = [];
        parts.forEach((part) => {
            if (!this._commonUtil.isObject(part)) {
                return;
            }
            targetParts.push({
                shortDescription: part.partDesc || part.partName,
                partNumber: part.partNumber,
                originalPartNumber: part.originalPartNumber
            });
        });
        this.targetProdCategory.parts = targetParts;
    }

    private _setProductList(): void {
        if (
            !this._commonUtil.isObject(this.targetProdCategory)
        ) {
            return;
        }
        const listName: string = this.targetProdCategory.listName;
        if (this._commonUtil.isValidString(listName)) {
            this._toggleSelectedSection();
            return;
        }
        this._promotionSvc
            .getPartsByListId(this.targetProdCategory.listId)
            .subscribe((response: any) => {
                if (response.errorCode) {
                    return;
                }
                this.targetProdCategory.listName = response.listName;
                this._setParts(response.parts);
                this._toggleSelectedSection();
            });
    }

    private _setBrands(): void {
        let brandCards: any[];
        try {
            brandCards = this.targetProdCategory.brands;
            if (!this._commonUtil.isValidArray(brandCards)) {
                return;
            }
        } catch (e) {
            return;
        }
        const newCards: any[] = [];
        for (const selectedBrand of brandCards) {
            newCards.push({
                name: selectedBrand.brand,
                parts: this._commonUtil.isValidArray(selectedBrand.exclude) ? selectedBrand.exclude : []
            });
        }
        this.brandCards = newCards;
        this._toggleSelectedSection();
    }

    private _setTargetProdOption(): void {
        let targetProductsModel: any;
        try {
            targetProductsModel = this.promoDetails.rules.targetProductsModel;
            if (this._commonUtil.isObject(targetProductsModel.byList)) {
                this.targetProdCategory = {
                    option: 'byList',
                    listId: targetProductsModel.byList.listId
                };
            } else if (this._commonUtil.isObject(targetProductsModel.byCategory)) {
                this.targetProdCategory = {
                    option: 'byCategory'
                };
            } else if (this._commonUtil.isObject(targetProductsModel.byBrand)) {
                this.targetProdCategory = {
                    option: 'byBrand',
                    brands: targetProductsModel.byBrand
                };
            }
        } catch (e) {
            return;
        }
    }

    private _showModal(): void {
        this.actionModal = this._modalService.show(ActionModalComponent, {
            class: 'pilm-popup action-modal'
        });
        this.actionModal.content.actionComplete = this.onActionComplete;
        const promoDetails: any = this.promoDetails;
        this.actionModal.content.promotion = {
            creatorName: promoDetails.creatorName,
            createdOn: promoDetails.createdOn,
            promoCode: this.promoCode,
            promoDocNumber: promoDetails.promoDocNumber,
            promoName: promoDetails.headers.promoName,
            promoId: promoDetails.promoId,
            taskId: promoDetails.taskId
        };
    }

    private _subscribeEvents(): void {
        this.subscriptions.push(
            this.onActionComplete
                .subscribe((actionDetails: any) => {
                    const actionType: string = actionDetails.actionType;
                    const response: any = actionDetails.response;
                    let promoDetails: any = this._commonUtil.isValidArray(response) ?
                        response[0] : response;
                    if (!this._commonUtil.isObject(promoDetails)) {
                        promoDetails = {};
                    }
                    const note: string = promoDetails.note;
                    const actionable: boolean = promoDetails.actionable;
                    if (this._commonUtil.isValidString(note)) {
                        this._onNoteAdded({
                            comments: note,
                            createdOn: new Date().getTime(),
                            createdBy: this._authService.getUserDetails('fullName')
                        });
                    }
                    this.promoDetails.actionable = actionable;
                    this.promoDetails.taskId = promoDetails.taskId;
                    if (-1 !== ['approved', 'rejected'].indexOf(actionType)) {
                        this._updateApprovalSequence(actionable, actionType);
                    } else if ('archived' === actionType) {
                        this.promoDetails.status = 'ARCHIVED';
                    }
                    this._setupStepSettings(this.promoDetails.status);
                })
        );

        this.subscriptions.push(
            this._eventEmitter
                .onApproverGroupChange
                .subscribe(() => this._setApproverGroups())
        );
    }

    private _toggleSelectedSection(): void {
        if (!this._commonUtil.isValidString(this._section)) {
            return;
        }
        const targetSection: any = this.status[this._section];
        targetSection.isOpen = !targetSection.isOpen;

        if ('notes' === this._section || 'productList' === this._section) {
            this._eventEmitter
                .onPageScroll
                .emit(true);
        }
    }

    private _setPrompApproved(promoApprovers: any[]): void {
        const lastApproverPosition: number = promoApprovers.length - 1;
        const approvedGroup: any = promoApprovers[lastApproverPosition];
        this._setApprovedGroupStatus(approvedGroup);
        this.approvalPendingGroupId = approvedGroup.groupId;
        this.approvalProcess = true;
    }

    private _updateApprovalSequence(actionable: boolean, actionType: string) {
        let promoStatus: string;
        let promoApprovers: any[] = this.promoApprovers;
        if (actionable) {
            promoStatus = this.promoDetails.status;
        } else {
            promoStatus = 'rejected' === actionType ? 'REJECTED' : 'WAITING_FOR_LAUNCH';
            this.promoDetails.status = promoStatus;
            if ('WAITING_FOR_LAUNCH' === promoStatus) {
                this.approvalProcess = true;
            } else if ('REJECTED' === promoStatus) {
                this.promoDetails.candidateGroups.rejectedGroup = this.approvalPendingGroupId;
                this._setApprovalStage(promoApprovers);
                return;
            }
        }
        let approvedGroup: any;
        for (const promoApprover of promoApprovers) {
            if (!this._commonUtil.isObject(promoApprover)) {
                continue;
            }
            if (this._commonUtil.isObject(approvedGroup)) {
                this.approvalPendingGroupId = promoApprover.groupId;
                try {
                    this.promoDetails.candidateGroups.pendingGroups = [
                        this.approvalPendingGroupId
                    ];
                } catch (e) {
                    this.promoDetails.candidateGroups = {
                        pendingGroups: [
                            this.approvalPendingGroupId
                        ]
                    };
                }
                this.approvalProcess = true;
                break;
            }
            if (this.approvalPendingGroupId === promoApprover.groupId) {
                approvedGroup = promoApprover;
                approvedGroup.status = 'APPROVED';
            } else {
                delete promoApprover.status;
            }
        }
    }
}
