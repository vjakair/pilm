/*****************************************************************
 * Proprietary & Confidential  |  © 2017 PhaseZero Ventures LLC  *
 * This is part of PhaseZero Ventures LLC and cannot be copied,  *
 * modified and/or distributed without the express permission of *
 * PhaseZero Ventures LLC                                        *
 *****************************************************************/
/**
 * @author: Irshadahmed
 */
import {
    Component,
    EventEmitter,
    Input,
    OnDestroy,
    ViewChild
} from '@angular/core';
import {
    NgForm
} from '@angular/forms';
import {
    BsModalRef
} from 'ngx-bootstrap/modal/modal-options.class';
import {
    CommonUtilService
} from '../../core/commonUtil.service';
import {
    EventEmitterService
} from '../../core/event-emitter';
import {
    SubscriptionHistory
} from '../../helper/subscriptionHistory.helper';
import {
    PromotionService
} from '../promotions.service';
@Component({
    selector: 'action-modal',
    template: `
    <div class="modal-dialog">
    <form class="pilm-form" #actionFrm="ngForm" role="form">
    <div class="modal-header">
    <div class="action-img {{actionType}}"></div>
    <h4 class="modal-title" translate="{{actionKey}}"></h4>
</div>
<div class="modal-body">
    <div class="promo-details">
        <div class="row">
            <h5 translate="COMMON_MODAL.PROMOCODE"></h5>
            <p>{{promotion.promoCode}}</p>
        </div>
        <div class="row">
            <h5 translate="COMMON_MODAL.DOC_NO"></h5>
            <p>{{promotion.promoDocNumber}}</p>
        </div>

        <div class="row">
            <h5 translate="COMMON_MODAL.PROMONAME"></h5>
            <p>{{promotion.promoName}}</p>
        </div>
        <div class="row">
            <h5 translate="COMMON_MODAL.CREATED_BY"></h5>
            <p>{{promotion.creatorName}}</p>
        </div>
        <div class="row">
            <h5 translate="COMMON_MODAL.CREATED_ON"></h5>
            <p>{{promotion.createdOn | myFormatDate}}</p>
        </div>
    </div>
    <div class="reason"
    [ngClass]="{'invalid': comments.invalid && (comments.dirty || comments.touched)}">
        <h5>
            <span translate="COMMON.ADD_REASON">
            </span>
            (<span translate="{{reasonReqd ? 'COMMON.MAND' : 'COMMON.OPT'}}">
                </span>)
            </h5>
        <textarea class="form-control pilm-form-control"
        maxlength="250"
        [required]="reasonReqd" name="comments" id="comments"
        [(ngModel)]="reason" #comments="ngModel"></textarea>
        <div class="char-count"><span>{{reason ? reason.length : 0}}/250</span></div>
    </div>
    <div class="actions">
        <button type="button" (click)="closePopover()"
        class="btn btn-default" translate="BUTTONS.CANC"></button>
        <button type="submit" (click)="actionConfirmed()"
        class="btn btn-primary" translate="BUTTONS.CONF"></button>
    </div>
</div>
</form>
</div>
    `
})
export class ActionModalComponent extends SubscriptionHistory implements OnDestroy {
    public reason: string;
    public promotion: any = {};
    public actionKey: string;
    public actionType: string;
    public reasonReqd: boolean;
    @ViewChild('actionFrm')
    public actionFrm: NgForm;
    public actionComplete: EventEmitter<any>;
    constructor(
        public bsModalRef: BsModalRef,
        private _commonUtil: CommonUtilService,
        private _eventEmitter: EventEmitterService,
        private _promotionSvc: PromotionService
    ) {
        super();
        this.subscriptions.push(
            _eventEmitter.onNavigationSuccess.subscribe(
                () => this.closePopover()
            )
        );
    }
    public ngOnDestroy() {
        this.unsubscribe();
    }
    public closePopover() {
        this.bsModalRef.hide();
    }
    public actionConfirmed() {
        if (this.actionFrm.invalid) {
            this._commonUtil.setFormDirty(this.actionFrm.controls);
            return;
        }
        this._promotionSvc
            .performSelectedAction(this.actionType,
            this.promotion.taskId,
            this.promotion.promoId,
            this.reason
            ).subscribe((res: any) => {
                if (res.errorCode) {
                    this._eventEmitter
                        .onAlert
                        .emit({
                            type: 'alert-warning',
                            msgKey: 'PROMOTION.ERROR.SERVER'
                        });
                    return;
                }
                const promo: any = res[0];
                this.promotion.taskId = promo.taskId;
                this.closePopover();
                this.actionComplete.emit({
                    actionType: this.actionType,
                    response: promo
                });
            });
    }
}
