/*****************************************************************
 * Proprietary & Confidential  |  © 2017 PhaseZero Ventures LLC  *
 * This is part of PhaseZero Ventures LLC and cannot be copied,  *
 * modified and/or distributed without the express permission of *
 * PhaseZero Ventures LLC                                        *
 *****************************************************************/
/**
 * @author: Irshadahmed
 */
import {
    Component,
    ChangeDetectorRef,
    EventEmitter,
    OnInit,
    OnDestroy,
    ViewChild
} from '@angular/core';
import {
    NgForm
} from '@angular/forms';

import {
    BsModalService
} from 'ngx-bootstrap/modal';
import {
    BsModalRef
} from 'ngx-bootstrap/modal/modal-options.class';

import {
    CommonUtilService
} from '../../core/commonUtil.service';
import {
    CustomerListModalComponent
} from '../customer-list-modal/customerListModal.component';
import {
    EventEmitterService
} from '../../core/event-emitter';
import {
    PromotionService
} from '../promotions.service';
import {
    SubscriptionHistory
} from '../../helper/subscriptionHistory.helper';
import {
    TitleService
} from '../../core/title';
import {
    TranslationService
} from '../../core/translation.service';
import {
    ViewCustListModalComponent
} from '../view-cust-list-modal/viewCustListModal.component';
import { AppConfig } from '../../config/app.config';
@Component({
    selector: 'goals',
    templateUrl: './targetMarket.component.html',
    styleUrls: ['./targetMarket.style.scss']
})
export class TargetMarketComponent extends SubscriptionHistory implements OnInit, OnDestroy {
    public defaultText: string;
    public errors: any = {
        lineOfBusiness: {},
        channel: {},
        customers: {},
        buyingGroup: {},
        rtb: {},
        emailBody: {},
        pdf: {},
        contactType: {}
    };
    public isTemplateReady: boolean;
    public metadata: any = {};
    public metaRes: any;
    public pdfName: string;
    public isCustomerVal = false;
    public promoDetails: any = {
        promoNotificationMethod: 'Email',
        email: {
            subject: '',
            body: ``,
            attachments: [],
            sendEmail: false
        }
    };
    @ViewChild('promoFrm')
    public promoFrm: NgForm;
    @ViewChild('metaAction')
    public metaActionModal: any;
    public selectedCount: number = 0;
    public targetMarket: any = {
        lineOfBusiness: [],
        channel: [],
        buyingGroup: [],
        rtb: [],
        customers: [],
        contactType: []
    };
    public uploadConfig: any = {
        pdfOpt: {
            type: 'PDF'
        }
    };
    public isAutoCustomerList: boolean = false;
    public uploadedFiles: any = {};
    public isEmailSend: boolean = false;
    public isAllCustomer: boolean = true;
    public isAllCustomerEnable: boolean = true;
    private _bsModRef: BsModalRef;
    private _isFileUploading: boolean;
    private _isPromoLoaded: boolean;
    private _selectedMetadata: any = {};
    private _customers: any[];
    private _customerValidationStatus: string;
    private _selectedCustListToReAssign: any[] = [];
    constructor(
        private _bsModalSvc: BsModalService,
        private _commonUtil: CommonUtilService,
        private _eventEmitter: EventEmitterService,
        private _promoSvc: PromotionService,
        private _ref: ChangeDetectorRef,
        titleSvc: TitleService,
        private translationSvc: TranslationService,
        public appConfig: AppConfig
    ) {
        super();
        titleSvc.setTitle('TARGET_MARKET');
        translationSvc
            .getTranslation('COMMON.SEL')
            .subscribe((defaultText: string) => {
                this.defaultText = defaultText;
            });
        const targetMarket: any = this.appConfig.getAppFeatureSettings('targetMarketSkip');
        this.isAutoCustomerList = this.appConfig.isModuleEnabled('enableAutoCustomer');
        this.isAllCustomerEnable = this.appConfig.isModuleEnabled('enableAllCustomer');
        if (this.appConfig.isModuleEnabled('targetMarketSkip')) {
            this.isCustomerVal = false;
            if (this._commonUtil.isObject(targetMarket.settings) &&
                this._commonUtil.isObject(targetMarket.settings.customer)) {
                this.targetMarket.customers.push(targetMarket.settings.customer);
            }
        } else {
            this.isCustomerVal = true;
            this._setMetadata();
        }
    }
    public ngOnInit() {
        this._subscribeEvents();
        /* this._bsModalSvc.show(CustomerListModalComponent, {
            class: 'pilm-popup cust-list-modal'
        }); */
    }
    public ngOnDestroy() {
        this.unsubscribe();
    }
    public hasFormField(fieldName: string) {
        return this._hasFormField(fieldName);
    }
    public hasEditableField(fieldName: string) {
        return super._hasEditableField(fieldName);
    }
    public cancelFileUpload(label: string): void {
        this.promoDetails.email.attachments = [];
    }
    public onBuyingGroupCodeSelect(data: any): void {
        const item: any = data.item;
        if (item.selected) {
            this._addItem('buyingGroup', item);
        } else {
            this._removeItem('buyingGroup', item);
        }
    }
    public onChannelCodeSelect(data: any): void {
        const item: any = data.item;
        if (item.selected) {
            this._addItem('channel', item);
        } else {
            this._removeItem('channel', item);
        }
    }
    public onLobSelect(data: any): void {
        const item: any = data.item;
        if (item.selected) {
            this._addItem('lineOfBusiness', item);
        } else {
            this._removeItem('lineOfBusiness', item);
        }
    }
    public onRtbSelect(data: any): void {
        const item: any = data.item;
        if (item.selected) {
            this._addItem('rtb', item);
        } else {
            this._removeItem('rtb', item);
        }
    }
    public onContactTypeSelect(data: any): void {
        const item: any = data.item;
        if (item.selected) {
            this._addItem('contactType', item);
        } else {
            this._removeItem('contactType', item);
        }
    }
    public onHtmlPaste($event: Event | any): void {
        if (!$event || !$event.currentTarget) {
            return;
        }
        let clipData: string;
        if ('undefined' === typeof $event.clipboardData) {
            clipData = window['clipboardData'].getData('Text');
        } else {
            clipData = $event.clipboardData.getData('Text');
        }
        this.promoDetails.email.body = clipData;
        this.errors.emailBody.invalid = false;
    }

    public keyupHandlerFunction(emailBody: any): void {
        this.promoDetails
            .email.body = emailBody;
        this.errors.emailBody.invalid = this.promoDetails.email.sendEmail ?
            !this._commonUtil.isValidString(emailBody) :
            false;
    }
    public resetSelection(): void {
        this.selectedCount = 0;
        const metadata: any = {};
        for (const prop in this.metadata) {
            if (!this.metadata.hasOwnProperty(prop)) {
                continue;
            }
            const items: any[] = this.metadata[prop];
            this.metadata[prop] = [];
            if (
                !this._commonUtil.isValidArray(items)
            ) {
                continue;
            }
            items.forEach((item: any) => {
                item.selected = false;
            });
            metadata[prop] = items;
        }
        if (this.isAutoCustomerList) {
            this._customers = [];
        }
        this.targetMarket
            .customers = [];
        this._ref.detectChanges();
        setTimeout(() => {
            this.metadata = metadata;
        }, 100);
    }
    public revertProceed(): void {
        const selectedMetadata: any =
            this._commonUtil.getClonedObject(this._selectedMetadata);
        Object.assign(this.targetMarket, selectedMetadata);
        this.metadata = {};
        setTimeout(() => {
            this._updateMetadata();
            this.metaActionModal.hide();
        }, 50);
        this._customerValidationStatus = 'success';
        this._validatePromoDetails();
    }
    public reSelectCustomers(): void {
        this.metaActionModal.hide();
        this.targetMarket
            .customers = [];
        this._setSelectedMetadata();
        this._openCustListModal();
    }
    public uploadFile(file: any): void {
        const promoDraft: any = this._promoSvc.promotionDraft;
        this._isFileUploading = true;
        this._toggleActionBar();
        this._promoSvc
            .uploadFile(file.data, promoDraft.promoId)
            .subscribe((response) => {
                if (this._commonUtil.isValidString(response.fileName)) {
                    this.errors.pdf.invalid = false;
                    this.promoDetails.email.attachments = [{
                        label: 'TARGET_MARKET.PDF_UP',
                        files: [response]
                    }];
                } else {
                    this._eventEmitter
                        .onServerError
                        .emit({
                            fileType: 'PDF',
                            msgKey: 'TARGET_MARKET.ERRORS.PDF.FAILED'
                        });
                    this.errors.pdf.invalid = true;
                }
                this._isFileUploading = false;
                this._toggleActionBar();
            }, (error) => {
                this.errors.pdf.invalid = true;
                this._isFileUploading = false;
                this._toggleActionBar();
            });
    }
    public showCustomerList(): void {
        if (this.isAllCustomerEnable) {
            this._openCustListModal();
            return;
        }
        if (this.isAutoCustomerList ? false : !this.selectedCount) {
            return;
        }
        this._eventEmitter
            .onLoadingMsg
            .emit('TARGET_MARKET.MODAL.CONNECTING');
        if (!this._isMetadataChanged() && this._commonUtil.isValidArray(this._customers)) {
            this._eventEmitter
                .onLoaderToggle
                .emit(true);
            setTimeout(() => {
                this._openCustListModal();
            }, 10);
            return;
        }
        this._promoSvc
            .getCustomerList(
            this.targetMarket.buyingGroup,
            this.targetMarket.channel,
            this.targetMarket.lineOfBusiness,
            this.targetMarket.rtb,
            this.targetMarket.contactType,
                '*'
            )
            .subscribe((response: any) => {
                if (response.errorCode || !this._commonUtil.isValidArray(response)) {
                    this._customers = [];
                    this._setSelectedMetadata();
                } else {
                    this._customers = response;
                    this._setSelectedMetadata();
                }
                this._openCustListModal();
            });
    }
    public viewSelectedCustomers(): void {
        // ViewCustListModalComponent
        const custListModal: BsModalRef = this._bsModalSvc.show(ViewCustListModalComponent, {
            class: 'pilm-popup cust-list-modal'
        });
        custListModal.content.customers = this.targetMarket.customers;
    }

    public selectAllCustomer(isSelect: boolean, defaultInit: boolean = false): void {
        try {
            this.isAllCustomer = isSelect;
            if (isSelect) {
                if (this.targetMarket.customers && this._commonUtil.isValidArray(this.targetMarket.customers) && this.targetMarket.customers[0].name !== 'ALL_CUSTOMERS') {
                    this._selectedCustListToReAssign = this._commonUtil.deepCopy(this.targetMarket.customers || []);
                }
                this.targetMarket.customers = [{
                    name: 'ALL_CUSTOMERS',
                    customerNumber: 'ALL_CUSTOMERS',
                    contacts: []
                }];
                this._customerValidationStatus = 'success';
            } else if (!defaultInit) {
                if (!this._commonUtil.isValidArray(this._selectedCustListToReAssign) &&
                    this.targetMarket.customers &&
                    this._commonUtil.isValidArray(this.targetMarket.customers) &&
                    this.targetMarket.customers[0].name !== 'ALL_CUSTOMERS'
                ) {
                    this._selectedCustListToReAssign = this.targetMarket.customers || [];
                }
                this._customerValidationStatus = '';
                this.targetMarket.customers = this._selectedCustListToReAssign || [];
                this._customers = [];
            }
        } catch (e) {
            console.log('selectAllCustomer-->', e);
        }
    }

    private _addItem(key: string, item: any): void {
        const detectChange: boolean = 0 === this.selectedCount;
        this.errors[key].invalid = false;
        this.targetMarket[key].push(item.code);
        if ('contactType' !== key) {
            ++this.selectedCount;
        }
        if (detectChange) {
            this._ref.detectChanges();
        }
    }
    private _isMetadataChanged(): boolean {
        if (this.isAllCustomerEnable || this.isAutoCustomerList) {
            return false;
        }
        for (const prop in this._selectedMetadata) {
            if (!this._selectedMetadata.hasOwnProperty(prop)) {
                continue;
            }
            const arr1: any[] = this._selectedMetadata[prop];
            const arr2: any[] = this.targetMarket[prop];
            if (!this._commonUtil.arrayEquals(arr1, arr2)) {
                return true;
            }
        }
        return false;
    }
    private _openCustListModal(): void {
        if (!this.isAllCustomerEnable && !this._commonUtil.isValidArray(this._customers)) {
            this._eventEmitter
                .onAlert
                .emit({
                    type: 'alert-warning',
                    msgKey: 'TARGET_MARKET.ERRORS.CUSTOMERS.NOT_AVL'
                });
            return;
        }
        const custListModal: BsModalRef = this._bsModalSvc.show(CustomerListModalComponent, {
            class: 'pilm-popup cust-list-modal'
        });
        custListModal.content.setContent(this.targetMarket);
        custListModal.content.targetMarket = this.targetMarket;
        if (this.isAllCustomerEnable) {
            custListModal.content.searchCustomer();
        } else {
            custListModal.content._setCustomerList(this._customers);
        }
        this._bsModalSvc
            .onShown
            .subscribe(() => {
                this._eventEmitter
                    .onLoaderToggle
                    .emit(false);
            });
    }
    private _removeItem(key: string, item: any): void {
        const idx = this.targetMarket[key].indexOf(item.code);
        if (-1 !== idx) {
            this.targetMarket[key].splice(idx, 1);
            if ('contactType' !== key) {
                --this.selectedCount;
            }
        }
        if (!this.selectedCount) {
            this._ref.detectChanges();
        }
    }
    private _subscribeEvents() {
        // Set tempalte ready flag
        this.subscriptions.push(
            this._promoSvc
                .onTemplateReady
                .subscribe((isTemplateReady: boolean) => {
                    if (!isTemplateReady) {
                        return;
                    }
                    const templateFields: any[] = this._promoSvc.getTemplateFields('targets');
                    this.setFormFields(templateFields);
                    this.isTemplateReady = isTemplateReady;
                })
        );
        this.subscriptions.push(
            this._promoSvc
                .onPromotionLoaded
                .subscribe((isPromoLoaded: boolean) => {
                    if (isPromoLoaded) {
                        this.setEditableFormFields(this._promoSvc.getEditableFields('targets'),
                            this._promoSvc.isPromoConfigEditable());
                        this._isPromoLoaded = true;
                        this._setPromoDetails();
                    }
                })
        );
        // Validate promotion form if save button is clicked
        this.subscriptions.push(
            this._eventEmitter
                .onValidatePromoDetails
                .subscribe(() => {
                    this._commonUtil
                        .delayCallback('validatingTM', () => {
                            this._validatePromoDetails();
                        }, 50);
                })
        );
    }
    private _setPromoDetails(): void {
        this._promoSvc.currentStep = 3;
        const promoDetails = this._promoSvc.getPromoDetails();
        if (!this._commonUtil.isObject(promoDetails)) {
            return;
        }

        const targetMarket: any = promoDetails.targetMarket;
        for (const prop in targetMarket) {
            if (!targetMarket.hasOwnProperty(prop)) {
                continue;
            }
            if (this._commonUtil.isValidArray(targetMarket[prop])) {
                this.targetMarket[prop] = targetMarket[prop];
            }
        }
        /*if (!this._commonUtil.isDefined(targetMarket)) {
            this.showCustomerList();
        }*/
        this._setSelectedMetadata();
        const email: any = promoDetails.email;
        if (this._commonUtil.isObject(email)) {
            this.promoDetails.email = email;
            const attachments: any[] = email.attachments;
            if (attachments && attachments.length) {
                this.pdfName = email.attachments[0].files[0].fileName;
            }
        }
        this._updateMetadata();
    }
    private _setMetadata(): void {
        this._promoSvc
            .getMetadata()
            .subscribe((response: any) => {
                const channels: any[] = response.channel;
                const lobs: any[] = response.lineOfBusiness;
                const buyGroups: any[] = response.buyingGroup;
                const rtbs: any[] = response.rtb;
                const contactTypes: any[] = response.contactType;
                this.metaRes = {
                    channel: this._commonUtil.isValidArray(channels) ? channels : [],
                    lineOfBusiness: this._commonUtil.isValidArray(lobs) ? lobs : [],
                    buyingGroup: this._commonUtil.isValidArray(buyGroups) ? buyGroups : [],
                    rtb: this._commonUtil.isValidArray(rtbs) ? rtbs : [],
                    contactType: this._commonUtil.isValidArray(contactTypes) ? contactTypes : []
                };
                if (this.isAutoCustomerList || this.isAllCustomerEnable) {
                    this.targetMarket.lineOfBusiness = this.collectTargetFilterData(lobs);
                    this.targetMarket.buyingGroup = this.collectTargetFilterData(buyGroups);
                    this.targetMarket.rtb = this.collectTargetFilterData(rtbs);
                    this.targetMarket.contactType = this.collectTargetFilterData(contactTypes);
                    this.targetMarket.channel = this.collectTargetFilterData(channels);
                }
                this._updateMetadata();
            });
    }

    private collectTargetFilterData(items: any[]): any[] {
        const codes = [];
        if (this._commonUtil.isValidArray(items)) {
            items.map((item) => codes.push(item.code));
        }
        return codes;
    }

    private _setSelectedMetadata(): void {
        this._selectedMetadata = this._commonUtil.getClonedObject({
            buyingGroup: this.targetMarket.buyingGroup,
            channel: this.targetMarket.channel,
            lineOfBusiness: this.targetMarket.lineOfBusiness,
            rtb: this.targetMarket.rtb,
            contactType: this.targetMarket.contactType
        });
    }
    private _savePromoDetails(): void {
        const targetMarket: any = Object.assign({}, this.targetMarket);
        delete targetMarket.contactType;
        this.promoDetails.targetMarket = targetMarket;
        this._promoSvc.updateLocalPromoDraft(this.promoDetails);
        this._eventEmitter
            .onPromoSave
            .emit(true);
    }
    private _toggleActionBar(): void {
        this._eventEmitter.onToggleActions.emit(this._isFileUploading);
    }
    private _updateMetadata(): void {
        if (
            !this._isPromoLoaded ||
            !this.metaRes ||
            !Object.keys(this.metaRes).length
        ) {
            return;
        }
        const promoDetails = this._promoSvc.getPromoDetails();
        const targetMarket: any = promoDetails.targetMarket;
        if (this.isAutoCustomerList) {
            if (this._commonUtil.isValidArray(targetMarket.customers)
                && targetMarket.customers.length === 0) {
                this.showCustomerList();
            }
        } else if (this.isAllCustomerEnable) {
            if (this._commonUtil.isObject(targetMarket)
                && this._commonUtil.isValidArray(targetMarket.customers)
                && targetMarket.customers.length === 1) {
                this.selectAllCustomer((targetMarket.customers[0].customerNumber === 'ALL CUSTOMERS'), true);
            } else {
                this.selectAllCustomer(false, true);
            }
        }
        const metaResponse: any = this._commonUtil
            .getClonedObject(this.metaRes);
        if (!this._commonUtil.isObject(targetMarket)) {
            this.metadata = metaResponse;
            return;
        }
        for (const prop in metaResponse) {
            if (!metaResponse.hasOwnProperty(prop)) {
                continue;
            }
            const items: any[] = metaResponse[prop];
            const values: string[] = targetMarket[prop];
            if (
                !this._commonUtil.isValidArray(items) ||
                !this._commonUtil.isValidArray(values)) {
                continue;
            }
            items.forEach((item: any) => {
                if (-1 !== values.indexOf(item.code)) {
                    item.selected = true;
                    ++this.selectedCount;
                }
            });
        }
        this.metadata = metaResponse;
    }
    private _validatePromoDetails(): void {
        let isFrmInvalid: boolean;
        if (this.promoFrm.invalid) {
            isFrmInvalid = true;
            this._commonUtil.setFormDirty(this.promoFrm.controls);
        }
        isFrmInvalid = this._validateOtherFields();
        if (isFrmInvalid) {
            return;
        } else if ('inProgress' === this._customerValidationStatus) {
            return;
        }
        this._savePromoDetails();
    }
    private _validateOtherFields(): boolean {
        if (!this.isCustomerVal) {
            return false;
        }
        let isFrmInvalid: boolean = false;
        const targetMarket: any = this.targetMarket;
        let errorCount: number = 0;
        let isInvalidMeta: boolean = true;

        for (const prop in targetMarket) {
            if (!targetMarket.hasOwnProperty(prop) || 'customers' === prop || 'contactType' === prop) {
                continue;
            }
            if (this._commonUtil.isValidArray(targetMarket[prop])) {
                errorCount = 0;
                isInvalidMeta = false;
                break;
            } else {
                ++errorCount;
            }
        }

        if (isInvalidMeta) {
            isFrmInvalid = true;
        } else if (!this._commonUtil.isValidArray(targetMarket.customers)) {
            this._eventEmitter
                .onAlert
                .emit({
                    type: 'alert-warning',
                    msgKey: 'TARGET_MARKET.ERRORS.CUSTOMERS.REQUIRED'
                });
            isFrmInvalid = true;
        } else if (this._isMetadataChanged()) {
            if ('failed' === this._customerValidationStatus) {
                this.metaActionModal.show();
                isFrmInvalid = true;
            } else {
                this._customerValidationStatus = 'inProgress';
                this._validateCustomersWithNewSelection();
            }
        }

        // Set validation property for meta
        for (const prop in targetMarket) {
            if (!targetMarket.hasOwnProperty(prop) || 'customers' === prop) {
                continue;
            }
            this.errors[prop].invalid = isInvalidMeta;
        }

        if (this.promoDetails.email.sendEmail &&
            !this._commonUtil.isValidString(this.promoDetails.email.body)) {
            this.errors.emailBody.invalid = true;
            isFrmInvalid = true;
            ++errorCount;
        }
        // Making PDF optional
        /* if (
            !isFrmInvalid &&
            !this._commonUtil.isValidArray(this.promoDetails.email.attachments)
        ) {
            this.errors.pdf.invalid = true;
            isFrmInvalid = true;
            ++errorCount;
            this._eventEmitter
                .onAlert
                .emit({
                    type: 'alert-warning',
                    msgKey: 'TARGET_MARKET.ERRORS.PDF.REQUIRED'
                });
        } */
        return isFrmInvalid;
    }

    private _validateCustomersWithNewSelection(): void {
        this._promoSvc
            .getCustomerList(
            this.targetMarket.buyingGroup,
            this.targetMarket.channel,
            this.targetMarket.lineOfBusiness,
            this.targetMarket.rtb,
            this.targetMarket.contactType,
                '*'
            )
            .subscribe((response: any) => {
                if (response.errorCode || !this._commonUtil.isValidArray(response)) {
                    this._customers = [];
                    this._customerValidationStatus = 'failed';
                    this.metaActionModal.show();
                } else {
                    const customers: any[] = response;
                    let matchingCustomerCount: number = 0;
                    for (const customer of customers) {
                        if (!this._commonUtil.isObject(customer)) {
                            continue;
                        }
                        const result = this._commonUtil.getFilteredItem(
                            this.targetMarket.customers, 'customerNumber', customer.customerNumber
                        );
                        if (
                            this._commonUtil.isObject(result)
                        ) {
                            ++matchingCustomerCount;
                        }
                    }
                    if (this.targetMarket.customers.length !== matchingCustomerCount) {
                        this._customers = customers;
                        this._customerValidationStatus = 'failed';
                        this.metaActionModal.show();
                    } else {
                        this._customerValidationStatus = 'success';
                        this._setSelectedMetadata();
                        this._validatePromoDetails();
                    }
                }
            });
    }
}
