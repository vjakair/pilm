/*****************************************************************
 * Proprietary & Confidential  |  © 2017 PhaseZero Ventures LLC  *
 * This is part of PhaseZero Ventures LLC and cannot be copied,  *
 * modified and/or distributed without the express permission of *
 * PhaseZero Ventures LLC                                        *
 *****************************************************************/
/**
 * @author: Irshadahmed
 */
import {
    Component,
    ViewEncapsulation
} from '@angular/core';
import {
    BsModalRef
} from 'ngx-bootstrap/modal/modal-options.class';
@Component({
    selector: 'part-list-modal',
    templateUrl: './partListModal.component.html',
    styleUrls: ['./partListModal.style.scss'],
    encapsulation: ViewEncapsulation.None
})
export class PartListModalComponent {
    public titleKey: string;
    public searchBy: string = 'PROMOTION_DETAILS.SEARCH_PART_BY';
    public parts: any[] = [];
    public searchTerm: string;
    constructor(
        public bsModalRef: BsModalRef
    ) {
    }

    public setContent(parts) {
        this.parts = parts;
    }
}
