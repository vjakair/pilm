/*****************************************************************
 * Proprietary & Confidential  |  © 2017 PhaseZero Ventures LLC  *
 * This is part of PhaseZero Ventures LLC and cannot be copied,  *
 * modified and/or distributed without the express permission of *
 * PhaseZero Ventures LLC                                        *
 *****************************************************************/
/**
 * @author: Irshadahmed
 */
import {
    Component,
    OnInit,
    OnDestroy
} from '@angular/core';
import {
    PromotionService
} from '../promotions.service';
import {
    CommonUtilService
} from '../../core/commonUtil.service';
import {
    SubscriptionHistory
} from '../../helper/subscriptionHistory.helper';
import {
    TranslationService
} from '../../core/translation.service';
@Component({
    selector: 'category-view',
    templateUrl: './categoryView.component.html',
    styleUrls: ['./categoryView.style.scss']
})
export class CategoryViewComponent extends SubscriptionHistory implements OnInit, OnDestroy {
    public defaultText: string;
    public disableAdd: boolean;
    public lobList: any[];
    public isTemplateReady: boolean = false;
    public isPromoLoaded: boolean = false;
    public productLobs: any[] = [];
    public catCards: any[] = [];
    public cardsLength: number;
    private _selectedLobs: any[] = [];
    private _promoDetails: any;
    private catStore: any[] = [];
    constructor(
        private _commonUtil: CommonUtilService,
        private _promoSvc: PromotionService,
        private translationSvc: TranslationService
    ) {
        super();
        this.translationSvc
            .getTranslation('COMMON.SEL')
            .subscribe((defaultText: string) => {
                this.defaultText = defaultText;
            });
        this._setLobListTree();
    }
    public hasFormField(fieldName: string) {
        return this._hasFormField(fieldName);
    }
    public hasEditableField(fieldName: string) {
        return super._hasEditableField(fieldName);
    }
    public ngOnInit() {
        this._subscribeEvents();
    }
    public ngOnDestroy() {
        this.unsubscribe();
    }
    public addNewCatCard(isAllSelected = false) {
        this._promoSvc.getCreateLobListTree(this.lobList, isAllSelected);
        const catCard: any = {
            lob: this.defaultText,
            productClass: this.defaultText,
            productLine: this.defaultText,
            lobList: this._commonUtil.getClonedArray(this.lobList),
            category: this.catStore,
            exclude: [
            ],
            allSelected: isAllSelected
        };
        this.disableAdd = true;
        this.catCards.unshift(catCard);
        this.cardsLength = this.catCards.length;
        this._promoSvc.setTargetProdList('byCategory', this.catCards);
    }
    public removeCard(cardIdx: number): void {
        this.catCards.splice(cardIdx, 1);
        this.cardsLength = this.catCards.length;
        this._promoSvc.setTargetProdList('byCategory', this.catCards);
    }
    public selectLob(lobId: any): void {
        if (-1 === this._selectedLobs.indexOf(lobId)) {
            this._selectedLobs.push(lobId);
        }
    }
    private _setLobListTree() {
        const categoryConfig = this._promoSvc.categoryConfig;
        this._promoSvc
            .getLobList(true)
            .subscribe((response) => {
                if (!this._commonUtil.isValidArray(response.cats)) {
                    return;
                }
                const lobList: any[] = [];
                response.cats.forEach((lob: any) => {
                    if (this._commonUtil.isValidArray(lob.children)) {
                        lobList.push(lob);
                    }
                });
                this.catStore = this._commonUtil.deepCopy(lobList);
                lobList.map((cat1) => {
                   if (categoryConfig.hasOwnProperty(cat1.name)) {
                       cat1.name = categoryConfig[cat1.name];
                   }
                   if (this._commonUtil.isValidArray(cat1.children)) {
                       cat1.children.map((cat2) => {
                           if (categoryConfig.hasOwnProperty(cat2.name)) {
                               cat2.name = categoryConfig[cat2.name];
                           }
                           if (this._commonUtil.isValidArray(cat2.children)) {
                               cat2.children.map((cat3) => {
                                   if (categoryConfig.hasOwnProperty(cat3.name)) {
                                       cat3.name = categoryConfig[cat3.name];
                                   }
                               });
                           }
                       });
                   }
                });
                this.lobList = lobList;
                this._setPromoDetails();
            });
    }
    private _setPromoDetails() {
        if (!this._commonUtil.isValidArray(this.lobList) ||
            !this.isPromoLoaded) {
            return;
        }
        const catCards: any[] = this._promoSvc.getTargetProdList('byCategory');
        if (catCards && this._commonUtil.isValidArray(catCards)) {
            this.cardsLength = catCards.length;
            this.catCards = catCards;
            this._toggleAddBtn();
            return;
        }
        const promoDetails: any = this._promoSvc.getPromoDetails();
        const targetProductsModel: any = promoDetails.targetProductsModel;
        if (!this._commonUtil.isObject(targetProductsModel)) {
            this.addNewCatCard();
            return;
        }
        const byCategory: any[] = targetProductsModel.byCategory;
        if (this._commonUtil.isValidArray(byCategory)) {
            this._updateCardsWithLobs(byCategory, this._commonUtil.deepCopy(this.catStore));
            this.cardsLength = this.catCards.length;
        } else {
            this.addNewCatCard();
        }
    }
    private _subscribeEvents() {
        // Set tempalte ready flag
        this.subscriptions.push(
            this._promoSvc
                .onTemplateReady
                .subscribe((isTemplateReady: boolean) => {
                    if (!isTemplateReady) {
                        return;
                    }
                    this.setFormFields([
                        'BY_CATEGORY'
                    ]);
                    this.isTemplateReady = isTemplateReady;
                })
        );
        this.subscriptions.push(
            this._promoSvc
                .onPromotionLoaded
                .subscribe((isPromoLoaded: boolean) => {
                    if (isPromoLoaded) {
                        this.setEditableFormFields(this._promoSvc.getEditableFields('rules'),
                            this._promoSvc.isPromoConfigEditable());
                        this.isPromoLoaded = isPromoLoaded;
                        this._promoSvc.currentStep = 2;
                        this._setPromoDetails();
                    }
                })
        );
    }
    private _toggleAddBtn(): void {
        const catCards: any[] = this.catCards;
        let disableAdd: boolean = false;
        for (const card of catCards) {
            if (
                !this._commonUtil.isObject(card)
            ) {
                continue;
            }
            const productLines: string[] = card.productLines;
            if (!this._commonUtil.isValidArray(productLines)) {
                disableAdd = true;
                break;
            }
        }
        this.disableAdd = disableAdd;
    }
    private _updateCardsWithLobs(cards: any[], categoryArr: any[]): void {
        if (this._lookUpAllSelected(cards)) {
            this.addNewCatCard(true);
            return;
        }
        this._promoSvc.getCreateLobListTree(this.lobList, false);
        this.lobList.map((cat1) => {
            this._lookupLob(cards, cat1);
            cat1.children.map((cat2) => {
                this._lookUpClasses(cards, cat2);
                cat2.children.map((cat3) => {
                    this._lookUpProductLines(cards, cat3, cat2.code);
                });
            });
        });
        const catCard: any = {
            lob: this.defaultText,
            productClass: this.defaultText,
            productLine: this.defaultText,
            lobList: this._commonUtil.getClonedArray(this.lobList),
            exclude: [
            ],
            category: categoryArr,
            allSelected: false
        };
        this.disableAdd = !this._commonUtil.isValidArray(cards);
        this.catCards.unshift(catCard);
        this._promoSvc.setTargetProdList('byCategory', this.catCards);
    }

    private _lookupLob(cards, obj) {
        if (this._commonUtil.isValidArray(cards)) {
            const matchLob = this._commonUtil.arrayIndexOf(cards, 'lob', obj.code);
            Object.assign(obj, {selected: this._commonUtil.isObject(matchLob)});
        }
    }

    private _lookUpClasses(cards, obj) {
        if (this._commonUtil.isValidArray(cards)) {
            for (const card of cards) {
                if (card['productClass'] === obj.code) {
                    Object.assign(obj, {selected: true});
                    const _exclude = this._commonUtil.isValidArray(card['exclude']) ?
                        card['exclude'] :
                        [];
                    Object.assign(obj, {exclude: _exclude});
                    break;
                }
            }
        }
    }

    private _lookUpProductLines(cards, obj, cat2Code) {
        if (this._commonUtil.isValidArray(cards)) {
            cards.map((card) => {
                if (this._commonUtil.isValidArray(card.productLines) &&
                    (card.productClass === cat2Code)) {
                    for (const prodLines of card.productLines) {
                        if (prodLines === obj.code) {
                            Object.assign(obj, {selected: true});
                            break;
                        }
                    }
                }
            });
        }
    }

    private _lookUpAllSelected(cards): boolean {
        let isAllSelected: boolean = false;
        if (this._commonUtil.isValidArray(cards) && cards.length === 1) {
            isAllSelected = cards[0].lob === 'ALL' &&
                cards[0].productClass === 'ALL' &&
                cards[0].productLines[0] === 'ALL';
        }
        return isAllSelected;
    }
}
