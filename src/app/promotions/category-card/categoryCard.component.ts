/*****************************************************************
 * Proprietary & Confidential  |  © 2017 PhaseZero Ventures LLC  *
 * This is part of PhaseZero Ventures LLC and cannot be copied,  *
 * modified and/or distributed without the express permission of *
 * PhaseZero Ventures LLC                                        *
 *****************************************************************/
/**
 * @author: Irshadahmed
 */
import {
    ChangeDetectorRef,
    Component,
    EventEmitter,
    Input,
    OnInit,
    Output,
    OnDestroy
} from '@angular/core';
import {
    NgForm
} from '@angular/forms';

import {
    BsModalService
} from 'ngx-bootstrap/modal';
import {
    BsModalRef
} from 'ngx-bootstrap/modal/modal-options.class';

import {
    PromotionService
} from '../promotions.service';
import {
    CommonUtilService
} from '../../core/commonUtil.service';
import {
    PartListModalComponent
} from '../part-list-modal/partListModal.component';
import {
    EventEmitterService
} from '../../core/event-emitter';
import {
    SubscriptionHistory
} from '../../helper/subscriptionHistory.helper';
import { ExcludeItemModalComponent } from '../exclude-items-modal/excludeItemModal.component';
import * as _ from 'lodash';

@Component({
    selector: 'category-card',
    templateUrl: './categoryCard.component.html',
    styleUrls: ['./categoryCard.style.scss']
})
export class CategoryCardComponent extends SubscriptionHistory implements OnDestroy, OnInit {
    public classList: any[];
    @Input()
    public cardNo: number;
    @Input()
    public defaultText: string;
    @Input()
    public catCard: any;
    @Input()
    public disableRemove: boolean;
    @Input()
    get disableAdd() {
        return this._disableAdd;
    }
    set disableAdd(disableAdd: boolean) {
        if (disableAdd === this._disableAdd) {
            return;
        }
        this._disableAdd = disableAdd;
        this.disableAddChange.emit(this._disableAdd);
    }
    public isFrmSubmitted: boolean;
    @Input()
    public lobList: any[];
    public prodLines: any[];
    public prodLineSelected: boolean;
    @Output()
    public disableAddChange = new EventEmitter();
    @Output()
    public removeCard: EventEmitter<any> = new EventEmitter();
    @Input()
    public scroll: boolean;
    @Output()
    public selectLob: EventEmitter<any> = new EventEmitter();
    @Output()
    public viewExclusionList: EventEmitter<any> = new EventEmitter();
    public defaultValue: any = 12;
    public isExcludePartsAvail: boolean = false;
    private _disableAdd: boolean;
    private _selectedLob: any;
    constructor(
        private _bsModalSvc: BsModalService,
        private _commonUtil: CommonUtilService,
        private _eventEmitter: EventEmitterService,
        private _promotionSvc: PromotionService,
        private _ref: ChangeDetectorRef
    ) {
        super();
        // Validate promotion form if save button is clicked
        this.subscriptions.push(
            _eventEmitter
                .onValidatePromoDetails
                .subscribe(() => this.isFrmSubmitted = true)
        );
    }
    public ngOnInit() {
        this.catCard.productLines = [];
        this._subscribeEvents();
        this._setDefaultCatCards(this.lobList);
    }
    public ngOnDestroy() {
        this.unsubscribe();
    }
    public hasEditableField(fieldName: string) {
        return super._hasEditableField(fieldName);
    }
    public onLobSelect($event: any): void {
        this._setDefaultCatCards($event.items);
    }
    public onLobBulkAction($event: any): void {
        for (const cat1 of $event.items) {
            Object.assign(cat1, {selected: 'select' === $event.type});
        }
        this._setDefaultCatCards($event.items);
    }
    public onClassSelect($event: any): void {
        if ('click' === $event.type) {
            this._resetProdLines();
            this.prodLines = this._commonUtil
                .getFilteredItem($event.items,
                    'selected',
                    true,
                    true);
            // this.prodLines.map((prod) => {
            //    prod.children.map((item) => {
            //       item.selected = true;
            //    });
            // });
            this._updateCatCard($event.items, 'CLASS');
            this._validateExcludePart($event.items);
            // this.defaultValue = Math.random();
            this._ref.detectChanges();
        }
    }

    public onClassBulkAction($event: any): void {
        $event.items.map((cat2) => {
            cat2.children.map((cat3) => {
                cat3.selected = cat2.selected;
            });
        });
        this._resetProdLines();
        this.prodLines = $event.items;
        if ('select' !== $event.type) {
            this.prodLines = [];
        }
        this._updateCatCard($event.items, 'CLASS');
        this._validateExcludePart($event.items);
        // this.defaultValue = Math.random();
        this._ref.detectChanges();
    }

    public onProdLineBulkAction($event: any): void {
        /*$event.items.map((cat2) => {
            cat2.children.map((cat3) => {
                cat3.selected = 'select' === $event.type;
            });
        });*/
        this._resetProdLines();
        this._updateCatCard($event.items, 'PRODUCT');
        this._validateExcludePart($event.items);
        // this.defaultValue = Math.random();
        this._ref.detectChanges();
    }

    /**
     * Product Line changes
     * @param prodLine
     */
    public onProdLineSelect($event: any): void {
        this._updateCatCard($event.items, 'PRODUCT');
        this._validateExcludePart($event.items);
    }

    public onDpToggle(isVisible: boolean): void {
        if (this.scroll) {
            this._eventEmitter.onPageScroll.emit(true);
        }
    }
    public removeCatCard(cardNo: any) {
        if (this.disableRemove) {
            return;
        }
        this.removeCard.emit(this.cardNo);
    }
    public viewExclusionModal(): void {
        const custListModal: BsModalRef = this._bsModalSvc.show(ExcludeItemModalComponent, {
            class: 'pilm-popup exc-prod-modal'
        });
        custListModal.content.setContent(this.catCard);
        this._bsModalSvc
            .onHidden
            .subscribe(() => {
                this.isExcludePartsAvail = this._commonUtil.isValidArray(
                    this._promotionSvc.getExcludeParts(
                        this._commonUtil.deepCopy(this.catCard.lobList)));
            });
    }
    public viewExcPartList(): void {
        let cls: string = 'pilm-popup part-list-modal';
        const parts: any[] = this._promotionSvc.getExcludeParts(this._commonUtil.deepCopy(this.catCard.lobList));
        if (!this._commonUtil.isValidArray(parts)) {
            return;
        }
        if (11 < parts.length) {
            cls += ' full-screen';
        }
        const partListMod: BsModalRef = this._bsModalSvc.show(PartListModalComponent, {
            class: cls
        });
        partListMod.content.setContent(parts);
    }
    private _resetCategoryCard(): void {
        const selectedLob: any = this._commonUtil.getFilteredItem(
            this.lobList,
            'selected',
            true
        );
        if (this._commonUtil.isObject(selectedLob)) {
            this.selectLob.emit(selectedLob.id);
            this.classList = selectedLob.children;
            const selectedCls: any = this._commonUtil.getFilteredItem(
                this.classList,
                'selected',
                true
            );
            if (this._commonUtil.isObject(selectedCls)) {
                this.prodLines = selectedCls.children;
            }
        }
        // this.selectText = this.defaultText;
        /* this.cardSettings = {
            lob: {
                selectText: this.defaultText
            },
            cls: {
                selectText: this.defaultText
            },
            prodLine: {
                selectText: this.defaultText
            }
        }; */
    }
    private _resetProdLines() {
        this.catCard.productLines = [];
        this.prodLineSelected = false;
        this.catCard.selectedCount = 0;
        this.disableAdd = true;
    }
    private _resetExcList(prodLineCd: any): void {
        const newExcList: any[] = [];
        const excList: any[] = this.catCard.exclude;
        if (!this._commonUtil.isValidArray(excList)) {
            return;
        }
        excList.forEach((item: any) => {
            if (item.productLine === prodLineCd) {
                return;
            }
            newExcList.push(item);
        });
        this.catCard.exclude = newExcList;
    }

    private _setDefaultCatCards(lobList): void {
        this.catCard.productLines = [];
        if (this._commonUtil.isValidArray(lobList)) {
            this.classList = [];
            for (const cat1 of lobList) {
                if (cat1.selected) {
                    for (const cat2 of cat1.children) {
                        const cat2Obj: any = {cat1ID: cat1.id, cat1Code: cat1.code, cat1Name: cat1.name};
                        Object.assign(cat2, cat2Obj);
                        if (!cat2.selected) {
                            cat2.exclude = [];
                        }
                        this.classList.push(cat2);
                    }
                }
            }
            this.prodLines = this._commonUtil
                .getFilteredItem(this.classList,
                    'selected',
                    true,
                    true);
            this.prodLineSelected = this._commonUtil.isValidArray(this.prodLines) &&
                this._commonUtil.isValidArray(this.classList);
        } else {
            this.prodLineSelected = false;
        }
        this._excludePartStatus();
    }

    private _validateExcludePart(classLineItems): void {
        this.prodLineSelected = false;
        let prodLines = [];
        const activeClassList = [];
        if (this._commonUtil.isValidArray(classLineItems)) {
            for (const classLine of classLineItems) {
                if (classLine.selected) {
                    activeClassList.push(classLine);
                } else {
                    classLine.exclude = [];
                }
            }
            if (this._commonUtil.isValidArray(activeClassList)) {
                for (const line of activeClassList) {
                    const classList = this._commonUtil
                        .getFilteredItem(line.children,
                            'selected',
                            true,
                            true);
                    prodLines = [...prodLines, ...classList];
                    if (prodLines.length > 1) {
                        break;
                    }
                }
                this.prodLineSelected = this._commonUtil.isValidArray(prodLines);
            }
        }
        this._excludePartStatus();
    }

    private _excludePartStatus() {
        this.isExcludePartsAvail = this._commonUtil.isValidArray(
            this._promotionSvc.getExcludeParts(
                this._commonUtil.deepCopy(this.catCard.lobList)));
    }
    private _updateCatCard(itemList, status) {
        const syncOnClassOrProductWithLOB = () => {
            const groupOnItems = _.groupBy(itemList, 'cat1ID');
            for (const baseClass of this.catCard.lobList) {
                if (groupOnItems.hasOwnProperty(baseClass.id)) {
                    baseClass.children = groupOnItems[baseClass.id];
                }
            }
        };
        switch (status) {
            case 'CLASS':
                syncOnClassOrProductWithLOB();
                break;
            case 'PRODUCT':
                syncOnClassOrProductWithLOB();
                break;
        }
    }
    private _subscribeEvents() {
        this.subscriptions.push(
            this._promotionSvc
                .onPromotionLoaded
                .subscribe((isPromoLoaded: boolean) => {
                    if (!isPromoLoaded) {
                        return;
                    }
                    this.setEditableFormFields(this._promotionSvc.getEditableFields('rules'),
                        this._promotionSvc.isPromoConfigEditable());
                })
        );
    }
}
