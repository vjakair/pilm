/*****************************************************************
 * Proprietary & Confidential  |  © 2017 PhaseZero Ventures LLC  *
 * This is part of PhaseZero Ventures LLC and cannot be copied,  *
 * modified and/or distributed without the express permission of *
 * PhaseZero Ventures LLC                                        *
 *****************************************************************/
/**
 * @author: Irshadahmed
 */
// Import Routes
import {
    Routes
} from '@angular/router';
import {
    ApprovalProcessComponent
} from './approval-process/approvalProcess.component';
import {
    AuthenticatedGuard
} from '../auth/authenticated.guard';
import {
    CategoryViewComponent
} from './category-view/categoryView.component';
import {
    ConfigureRulesComponent
} from './configure-rules/configureRules.component';
import {
    ChooseTemplateComponent
} from './choose-template/chooseTemplate.component';
import {
    DefinePromotionComponent
} from './define/definePromotion.component';
import {
    GoalsComponent
} from './goals/goals.component';
import {
    ListViewComponent
} from './list-view/listView.component';
/* import {
    PromoRouteResolver
} from '../app.resolver'; */
import {
    NotificationComponent
} from './notification/notification.component';
import {
    PromotionComponent
} from './promotions.component';
import {
    PromotionDetailsComponent
} from './promotion-details/promotionDetails.component';
import {
    PromotionStatusComponent
} from './promotion-status-details/promotionStatusDetails.component';
import {
    TargetMarketComponent
} from './target-market/targetMarket.component';
import {
    DownloadReportComponent
 } from './reports/download-report.component';
import { BrandViewComponent } from './brand-view/brandView.component';

export const ROUTES: Routes = [
    /* {
        path: 'promotions/status/:statusId',
        component: PromotionStatusComponent,
        canActivate: [AuthenticatedGuard]
    },
    {
        path: 'promotions/:promotionId',
        component: PromotionDetailsComponent,
        canActivate: [AuthenticatedGuard]
    } , */
    {
        path: 'notification',
        component: NotificationComponent,
        pathMatch: 'full',
        canActivate: [AuthenticatedGuard]
    },
    {
        path: 'reports',
        component: DownloadReportComponent,
        pathMatch: 'full',
        canActivate: [AuthenticatedGuard]
    },
    {
        path: 'promotions/choose-template',
        component: ChooseTemplateComponent,
        pathMatch: 'full',
        canActivate: [AuthenticatedGuard]
    },
    {
        path: 'promotions/:promoId/details',
        component: PromotionDetailsComponent,
        pathMatch: 'full',
        canActivate: [AuthenticatedGuard]
    },
    {
        path: 'promotions/:promoId/details/:notificationId',
        component: PromotionDetailsComponent,
        pathMatch: 'full',
        canActivate: [AuthenticatedGuard]
    },
    {
        path: 'promotions/:promoStatus/snapshots',
        component: PromotionStatusComponent,
        pathMatch: 'full',
        canActivate: [AuthenticatedGuard]
    },
    {
        path: 'promotions',
        component: PromotionComponent,
        canActivate: [AuthenticatedGuard],
        resolve: {
            promoRoutePatters: 'PromoRoutePatters'
        },
        children: [
            {
                path: ':promoId/define/:templateId',
                component: DefinePromotionComponent,
                pathMatch: 'full',
                canActivate: [AuthenticatedGuard]
            },
            {
                path: ':promoId/goals/:templateId',
                component: GoalsComponent,
                pathMatch: 'full',
                canActivate: [AuthenticatedGuard]
            },
            {
                path: ':promoId/rules/:templateId',
                component: ConfigureRulesComponent,
                canActivate: [AuthenticatedGuard],
                children: [
                    {
                        path: '',
                        redirectTo: 'by-list',
                        pathMatch: 'full'
                    },
                    {
                        path: 'by-list',
                        component: ListViewComponent
                    },
                    {
                        path: 'by-category',
                        component: CategoryViewComponent
                    },
                    {
                        path: 'by-brand',
                        component: BrandViewComponent
                    }
                ]
            },
            {
                path: ':promoId/target-market/:templateId',
                component: TargetMarketComponent,
                pathMatch: 'full',
                canActivate: [AuthenticatedGuard]
            },
            {
                path: ':promoId/approvals/:templateId',
                component: ApprovalProcessComponent,
                pathMatch: 'full',
                canActivate: [AuthenticatedGuard]
            }
        ]
    }
];
