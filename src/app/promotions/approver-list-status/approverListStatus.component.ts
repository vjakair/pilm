import {
    Component,
    Input
} from '@angular/core';
@Component({
    selector: 'approver-list-status',
    template: `
    <ul class="approver-list-status" [ngClass]="{'not-started': !approvalProcess}">
    <li *ngFor="let approver of promoApprovers" [ngClass]="{'active': 'APPROVED' === approver.status, 'rejected': approver.groupId === rejectedGroupId}">
      <div class="circle-wrapper"><div class="circle"></div></div>
      <div class="approver">
        <div class="group-user-info">
        <span class="group-name" translate="APPROVAL_PROCESS.{{approver.groupId}}"></span>:
        <span class="approver-name">{{approver.name}}</span>
        <span class="approver-email">{{approver.email}}</span>
        </div>
      </div>
      </li>
  </ul>
    `,
    styleUrls: [
        './approverListStatus.style.scss'
    ]
})
export class ApproverListStatusComponent {
    @Input()
    public approvalProcess:boolean;
    @Input()
    public promoApprovers:any[];
    @Input()
    public promoStatus:string;
    @Input()
    public rejectedGroupId: string;
    constructor() {

    }
}