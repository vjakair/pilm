/*****************************************************************
 * Proprietary & Confidential  |  © 2017 PhaseZero Ventures LLC  *
 * This is part of PhaseZero Ventures LLC and cannot be copied,  *
 * modified and/or distributed without the express permission of *
 * PhaseZero Ventures LLC                                        *
 *****************************************************************/
/**
 * @author: Irshadahmed
 */
import {
    ComponentRef,
    Component,
    Input,
    OnDestroy
} from '@angular/core';
import {
    CommonUtilService
} from '../../core/commonUtil.service';
import {
    EventEmitterService
} from '../../core/event-emitter';
import {
    NavigationService
} from '../../core/navigation.service';
import {
    PromotionService
} from '../promotions.service';
import {
    SubscriptionHistory
} from '../../helper/subscriptionHistory.helper';
import {
    TitleService
} from '../../core/title';
@Component({
    selector: 'promo-status-details',
    templateUrl: './promotionStatusDetails.component.html',
    styleUrls: ['./promotionStatusDetails.style.scss']
})
export class PromotionStatusComponent extends SubscriptionHistory implements OnDestroy {
    public promoStatus: string;
    public promoSnaps: any[];
    public promotions: any[];
    public selectedSnap: any;
    public pagination: any = {
        currentPage: 1,
        totalItems: 0,
        itemsPerPage: 20,
        maxSize: 10,
        show: false
    };
    private _subscriptions: any[] = [];
    constructor(
        private _commonUtil: CommonUtilService,
        private _eventEmitter: EventEmitterService,
        public navigation: NavigationService,
        private _promotionSvc: PromotionService,
        private _titleSvc: TitleService
    ) {
        super();
        navigation.toggleMainContainer(true);
        _titleSvc.setTitle('PROMOTION_STATUS_DETAILS');
        this.promoStatus = navigation.getRouteParam('promoStatus');
        this._subscribeEvents();
        this._getPromoSnaps();
        this._getPromotionsByStatus();
    }
    public ngOnDestroy() {
        this.navigation.toggleMainContainer(false);
        this.unsubscribe();
    }
    public onActionComplete(actionDetails) {
        if (this._commonUtil.isObject(actionDetails)) {
            const action:string = actionDetails.actionType;
            const response: any = actionDetails.response;
            let promoDetails: any = this._commonUtil.isValidArray(response) ?
                response[0] : response;
            if (!this._commonUtil.isObject(promoDetails)) {
                promoDetails = {};
            }
            const actionable: boolean = promoDetails.actionable;
            if ('rf' !== action && !actionable) {
                this._getPromoSnaps();
            }
        }
        this._getPromotionsByStatus();
    }
    public viewPromoSnapDetails(promotion: any): void {
        this.pagination.totalItems = promotion.count;
        this.pagination.currentPage = 1;
        this.pagination.show = false;
        this.navigation.navigate([
            'promotions',
            promotion.status,
            'snapshots'
        ]);
        this.promoStatus = promotion.status;
        this._getPromotionsByStatus();
    }
    public pageChanged(ev: any): void {
        if (this._commonUtil.isObject(ev)) {
            this.pagination.currentPage = ev.page;
            this._getPromotionsByStatus();
        }
    }
    private _getPromoSnaps() {
        this._promotionSvc
            .getPromoSnaps(false)
            .subscribe(
            (promoSnaps: any) => {
                this.promoSnaps = promoSnaps;
                if (!this._commonUtil.isObject(this.selectedSnap)) {
                    this._setSelectedSnap();
                }
            });
    }
    private _getPromotionsByStatus() {
        this._promotionSvc
            .getPromotionsByStatus(this.promoStatus, false, this.pagination.currentPage, this.pagination.itemsPerPage)
            .subscribe(
            (promoSnaps: any) => {
                if (promoSnaps && promoSnaps.errorCode) {
                    this.promotions = [];
                    return;
                }
                if ('pending' === this.promoStatus) {
                    this._setApproverGroupInfo(promoSnaps);
                } else {
                    this.promotions = promoSnaps;
                    this._setSelectedSnap();
                }
                this.pagination.show = true;
            });
    }
    private _setApproverGroupInfo(promoPendingTasks: any[]) {
        this._promotionSvc
            .setPromoApprovers(promoPendingTasks)
            .subscribe((promotions: any[]) => {
                this.promotions = promotions;
                this._setSelectedSnap();
            });
    }
    private _setSelectedSnap() {
        if (!this._commonUtil.isValidArray(this.promoSnaps)) {
            return;
        }
        const snaps: any[] = this.promoSnaps.filter((_promoSnap: any) => {
            return _promoSnap.status === this.promoStatus;
        });
        this.selectedSnap = snaps[0];
        this.pagination.totalItems = snaps[0].count;
    }
    private _subscribeEvents() {
        this.subscriptions
            .push(
            this._eventEmitter
                .onApproverGroupChange
                .subscribe(() => this._setApproverGroupInfo(this.promotions))
            );
    }
}
