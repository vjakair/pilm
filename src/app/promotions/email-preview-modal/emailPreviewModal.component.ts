/*****************************************************************
 * Proprietary & Confidential  |  © 2017 PhaseZero Ventures LLC  *
 * This is part of PhaseZero Ventures LLC and cannot be copied,  *
 * modified and/or distributed without the express permission of *
 * PhaseZero Ventures LLC                                        *
 *****************************************************************/
/**
 * @author: Irshadahmed
 */
import {
    Component,
    ViewEncapsulation
} from '@angular/core';
import {
    BsModalRef
} from 'ngx-bootstrap/modal/modal-options.class';
@Component({
    selector: 'customer-list',
    templateUrl: './emailPreviewModal.component.html',
    styleUrls: ['./emailPreviewModal.style.scss'],
    encapsulation: ViewEncapsulation.None
})
export class EmailPreviewModalComponent {
    public emailSubject: string;
    public emailBody: string;
    constructor(
        public bsModalRef: BsModalRef
    ) { }
}
