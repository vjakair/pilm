/*****************************************************************
 * Proprietary & Confidential  |  © 2017 PhaseZero Ventures LLC  *
 * This is part of PhaseZero Ventures LLC and cannot be copied,  *
 * modified and/or distributed without the express permission of *
 * PhaseZero Ventures LLC                                        *
 *****************************************************************/
/**
 * @author: Irshadahmed
 */
import {
    Directive,
    ElementRef,
    EventEmitter,
    Input,
    OnInit,
    Output
} from '@angular/core';
import {
    PromotionService
} from '../promotions.service';

@Directive({
    selector: '[displayField]'
})
export class DisplayFieldDirective implements OnInit {
    @Input()
    public fieldName: string;
    @Input()
    public stage: string;
    @Output()
    public removeFormField: EventEmitter<string> = new EventEmitter();
    constructor(
        private _promoSvc: PromotionService,
        private _el: ElementRef
    ) { }

    public ngOnInit() {
        if (this._promoSvc.hasField(this.stage, this.fieldName)) {
            return;
        }
        this.removeFormField.emit(this.fieldName);
        this._el.nativeElement.remove();
    }
}
