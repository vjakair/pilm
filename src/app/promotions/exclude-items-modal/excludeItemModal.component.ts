/*****************************************************************
 * Proprietary & Confidential  |  © 2017 PhaseZero Ventures LLC  *
 * This is part of PhaseZero Ventures LLC and cannot be copied,  *
 * modified and/or distributed without the express permission of *
 * PhaseZero Ventures LLC                                        *
 *****************************************************************/
/**
 * @author: Arvind
 */

import { ChangeDetectorRef, Component, ViewEncapsulation } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal/modal-options.class';
import { CommonUtilService } from '../../core/commonUtil.service';
import { PromotionService } from '../promotions.service';

@Component({
    selector: 'exclude-item',
    templateUrl: './excludeItemModal.component.html',
    styleUrls: ['./excludeItemModal.style.scss'],
    encapsulation: ViewEncapsulation.None
})

export class ExcludeItemModalComponent {
    public catCard: any;
    public searchTerm: string;
    public currentPage: number = 1;
    public categories: any[];
    public totalItems: number;
    public readonly itemsPerPage: number = 10;
    public readonly maxSize: number = 10;
    public pager: string;
    public parts: any[];
    public selectedProdLine: any;
    public selectedProductClass: any;
    private _lobId: number;
    constructor(
        public bsModalRef: BsModalRef,
        private _commonUtil: CommonUtilService,
        private _promoSvc: PromotionService,
        private _ref: ChangeDetectorRef
    ) { }

    public closeModal(commitChanges: boolean): void {
        // this._promoSvc.setTargetProdList('byCategory', this.categories);
        this.bsModalRef.hide();
        if (!this._commonUtil.isValidArray(this.parts)) {
            return;
        }
        this._deleteActionFlag(commitChanges);
    }

    public searchParts(): void {
        this._commonUtil
            .delayCallback('searchingParts', () => {
                this.currentPage = 1;
                this.getParts();
            }, 300);
    }

    public resetSearchTerm(): void {
        this.searchTerm = '';
        this.getParts();
    }

    public pageChanged(event: any): void {
        this.currentPage = event.page;
        this.getParts();
    }

    public togglePartSelection(part: any) {
        if (!this._commonUtil.isObject(part)) {
            return;
        }
        if (part.selected) {
            this.deSelectPart(part, true);
        } else {
            this.selectPart(part, true);
        }
    }

    public deSelectPart(part: any, toggleAction?: boolean): void {
        const partDetails: any = this._commonUtil
            .getFilteredItemWithIndex(this.selectedProductClass
                    .exclude,
                'partNumber',
                part.partNumber);
        if (!this._commonUtil.isObject(partDetails)) {
            return;
        }
        this.selectedProductClass
            .exclude
            .splice(partDetails.index, 1);
        part.selected = false;
        if (toggleAction) {
            this.toggleActionFlag(part);
        }
    }

    public selectPart(part: any, toggleAction?: boolean): void {
        if (!this._commonUtil.isValidArray(this.selectedProductClass.exclude)) {
            this.selectedProductClass.exclude = [];
        }
        this.selectedProductClass
            .exclude
            .push({
                productLine: this.selectedProdLine.code,
                partNumber: part.partNumber,
                shortDescription: part.partDesc
            });
        part.selected = true;
        if (toggleAction) {
            this.toggleActionFlag(part);
        }
    }
    public setContent(catCard): void {
        this.categories = catCard.lobList;
        let selectedProdLine: any = {};
        let productClass: any = {};
        const _classId = null;
        for (const cat1 of catCard.lobList) {
            cat1.isClosed = true;
            if (this._commonUtil.isValidArray(cat1.children)) {
                for (const cat2 of cat1.children) {
                    cat2.isClosed = true;
                    if (this._commonUtil.isValidArray(cat2.exclude)) {
                        if (this._commonUtil.isUnDefined(_classId)) {
                            this._lobId = cat1.id;
                            cat1.isClosed = false;
                            cat2.isClosed = false;
                            selectedProdLine = cat2.children[0];
                            productClass = cat2;
                        }
                    } else {
                        cat2.exclude = [];
                    }
                }
            }
        }
        if (this._commonUtil.isUnDefined(this._lobId)) {
            for (const cat1 of catCard.lobList) {
                if (cat1.selected && this._commonUtil.isValidArray(cat1.children)) {
                    for (const cat2 of cat1.children) {
                        if (cat2.selected && this._commonUtil.isValidArray(cat2.children)) {
                            this._lobId = cat1.id;
                            selectedProdLine = cat2.children[0];
                            productClass = cat2;
                            cat1.isClosed = false;
                            cat2.isClosed = false;
                            return this.setProdLineParts(selectedProdLine, productClass);
                        }
                    }
                }
            }
        }
        this.setProdLineParts(selectedProdLine, productClass);
    }

    public setProdLineParts(selectedProdLine: any, productClass: any): void {
        if (
            !this._commonUtil.isObject(selectedProdLine) ||
            !this._commonUtil.isObject(productClass)
        ) {
            return;
        }
        this.selectedProdLine = selectedProdLine;
        this.selectedProductClass = productClass;
        const prodLines: any[] = productClass.children;
        if (!this._commonUtil.isValidArray(prodLines)) {
            return;
        }
        this.currentPage = 1;
        this._ref.detectChanges();
        this.getParts();
    }

    public getParts(): void {
        this._promoSvc.getParts(
            ((this.currentPage - 1) * this.itemsPerPage),
            this.itemsPerPage,
            this._lobId,
            this.selectedProductClass.id,
            this.selectedProdLine.id,
            this.searchTerm
        )
            .subscribe((response: any): void => {
                const parts: any[] = response.parts;
                if (this._commonUtil.isValidArray(parts)) {
                    this._setPartSelection(parts);
                    this.parts = parts;
                } else {
                    this.parts = [];
                }
                this.totalItems = response.totalResults || parts.length;
                this._setPager();
            });
    }

    private _setPartSelection(parts: any[]): void {
        if (!this._commonUtil.isValidArray(parts) ||
            !this._commonUtil.isValidArray(this.selectedProductClass.exclude)) {
            return;
        }
        // console.log('product is =>(%o) and class is => (%o)', this.selectedProdLine, this.selectedProductClass);
        parts.forEach((part: any) => {
            const item: any = this._commonUtil
                .getFilteredItem(
                    this.selectedProductClass.exclude,
                    'partNumber',
                    part.partNumber
                );
            if (this._commonUtil.isObject(item)) {
                part.selected = true;
            }
        });
    }

    private _deleteActionFlag(commitChanges?: boolean): void {
        this.parts
            .forEach((part: any): void => {
                if (commitChanges) {
                    delete part.actioned;
                    return;
                }
                if (part.actioned) {
                    if (part.selected) {
                        this.deSelectPart(part);
                    } else {
                        this.selectPart(part);
                    }
                    delete part.actioned;
                }
            });
    }
    private _setPager(): void {
        let toCount: number = this.currentPage * this.itemsPerPage;
        toCount = toCount < this.totalItems ? toCount : this.totalItems;
        this.pager = ((this.currentPage - 1) * 10) + ' - ' + toCount;
    }

    private toggleActionFlag(part: any): void {
        if (part.actioned) {
            delete part.actioned;
        } else {
            part.actioned = true;
        }
    }
}
