/*****************************************************************
 * Proprietary & Confidential  |  © 2017 PhaseZero Ventures LLC  *
 * This is part of PhaseZero Ventures LLC and cannot be copied,  *
 * modified and/or distributed without the express permission of *
 * PhaseZero Ventures LLC                                        *
 *****************************************************************/
/**
 * @author: Irshadahmed
 */
import {
    Component,
    ViewEncapsulation
} from '@angular/core';
import {
    BsModalRef
} from 'ngx-bootstrap/modal/modal-options.class';
@Component({
    selector: 'view-cust-list',
    templateUrl: './viewCustListModal.component.html',
    styleUrls: ['../customer-list-modal/customerListModal.style.scss'],
    encapsulation: ViewEncapsulation.None
})
export class ViewCustListModalComponent {
    public customers: any[];
    public customer: any[];
    public searchTerm: string;
    constructor(
        public bsModalRef: BsModalRef
    ) { }
}
