/*****************************************************************
 * Proprietary & Confidential  |  © 2017 PhaseZero Ventures LLC  *
 * This is part of PhaseZero Ventures LLC and cannot be copied,  *
 * modified and/or distributed without the express permission of *
 * PhaseZero Ventures LLC                                        *
 *****************************************************************/
/**
 * @author: Arvind
 */

import {
    Component,
    ElementRef,
    EventEmitter,
    HostListener,
    Input,
    OnChanges,
    OnInit,
    Output, SimpleChange, SimpleChanges
} from '@angular/core';
import {
    CommonUtilService
} from '../../core/commonUtil.service';
import {
    FilterPipe
} from '../../core/filter.pipe';
@Component({
    selector: 'category-select-box',
    templateUrl: './categorySelect.component.html',
    styleUrls: [
        './categorySelect.style.scss'
    ]
})

export class CategorySelectComponent implements OnInit, OnChanges {
    public item: any;
    @Input()
    public items: any[];
    @Output()
    public itemSelect: EventEmitter<any> = new EventEmitter();
    @Output()
    public multiSelectAction: EventEmitter<any> = new EventEmitter();
    @Input()
    public key: string;
    @Input()
    public defaultValue: string;
    @Input()
    public btnClass: string;
    @Input()
    public disabled: boolean;
    public searchTerm: string = '';
    public isOpen: boolean;
    @Input()
    public isDropup: boolean;
    @Input()
    public isFilter: boolean = false;
    @Input()
    public isMultiSelect: boolean;
    @Input()
    public name: string;
    @Output()
    public onDpToggle: EventEmitter<boolean> = new EventEmitter();
    @Input()
    public placeholder: string;
    @Input()
    public isMultiLevel: boolean = false;
    @Input()
    public isMultiSelectionAction: boolean = true;

    @Input()
    public valueChange: any;

    public selectedCount: number;
    public selectedText: string;
    public itemList: any[] = [];
    // public tempSelectedItems:any[] = [];
    private _filterPipe: FilterPipe;
    constructor(
        private _commonUtil: CommonUtilService,
        private _eleRef: ElementRef
    ) {
        this._filterPipe = new FilterPipe();
    }
    public ngOnChanges(changes: SimpleChanges) {
        if (this._commonUtil.isValidArray(this.items)) {
            this.itemList = JSON.parse(JSON.stringify(this.items));
            this._resetItemList(this.itemList, false);
            this._setDefaultSelectedItems();
        } else {
            this.selectedText = this.defaultValue;
            this.selectedCount = 0;
        }
    }
    public ngOnInit() {
        if (!this.btnClass) {
            this.btnClass = 'btn-default';
        }
        this._setDefaultSelectedItems();
    }

    public commitSelection() {
        // this.tempSelectedItems = [];
        this.isOpen = false;
        this.searchTerm = '';
    }
    public searchItem(field: string, value: string): any[] {
        if (!value) {
            this.items = JSON.parse(JSON.stringify(this.itemList));
            return;
        }
        let searchItemList: any[] = [];
        const items: any[] = JSON.parse(JSON.stringify(this.itemList));

        const searchItem = (itemList, sourceList) => {
            const foundItemList: any[] = [];
            for (let i: number = 0; i < sourceList.length; i++) {
                const itemVal = itemList[i][field].toLowerCase();
                if ( -1 !== itemVal.indexOf(value.toLowerCase())) {
                    itemList[i].selected = false;
                    foundItemList.push(itemList[i]);
                }
            }
            return foundItemList;
        };

        if (this.isMultiLevel) {
            const sourceList: any [] = this._commonUtil.getClonedArray(this.itemList);
            // tslint:disable-next-line:prefer-for-of
            for (let i: number = 0 ; i < sourceList.length; i ++) {
                const cat2 = sourceList[i];
                if (this._commonUtil.isValidArray(cat2.children)) {
                    const found = searchItem(cat2.children, sourceList[i]['children']);
                    if (this._commonUtil.isValidArray(found)) {
                        sourceList[i]['children'] = found;
                        searchItemList.push(sourceList[i]);
                    }
                }
            }
        } else {
            searchItemList = searchItem(items, this.itemList);
        }
        this.items = searchItemList;
    }
    public resetSearchTerm($event): void {
        $event.preventDefault();
        $event.stopPropagation();
        this.searchTerm = '';
        this.items = JSON.parse(JSON.stringify(this.itemList));
        document.getElementById('filter').focus();
    }
    public selectAll(): void {
        this._resetItemList(this.items, true);
        const selectedItems: any[] = this._getSelectedItems();
        if (this.isMultiSelectionAction) {
            this.multiSelectAction.emit({
                items: selectedItems,
                type: 'select'
            });
            this._setDefaultSelectedItems();
        } else {
            selectedItems.forEach((item: any): void => {
                if (item.selected) {
                    return;
                }
                item.selected = true;
                this.selectItem(item);
            });
        }
    }

    public deSelectAll(): void {
        this._resetItemList(this.items, false);
        const selectedItems: any[] = this._getSelectedItems();
        if (this.isMultiSelectionAction) {
            this.multiSelectAction.emit({
                items: selectedItems,
                type: 'deSelect'
            });
            this._setDefaultSelectedItems();
        }
    }
    @HostListener('document:click', ['$event'])
    public onClick($event) {
        if (this._eleRef.nativeElement.contains($event.target)) {
            return;
        }
        this.isOpen = false;
    }
    public onShown(): void {
        this.onDpToggle.emit(true);
        /* setTimeout(() => {
            this._eleRef.nativeElement.scrollIntoView();
        }, 50); */
    }
    /* public onHide():void {
        this.cancelSelection();
    } */
    public selectItem(item: any): void {
        item.selected = !item.selected;
        this.selectedText = this._getSelectedText();
        this._onItemSelect(item, 'click');
    }
    private _onItemSelect(selectedItem: any, itemType?: string): void {
        const selectedItems: any[] = this._getSelectedItems();
        this.itemSelect.emit({
            item: selectedItem,
            name: this.name,
            selectedCount: this.selectedCount,
            type: itemType,
            items: selectedItems
        });
    }
    private _doGetSelectedText(selectedItems: any): string {
        this.selectedCount = selectedItems.length;
        let selectedText = '';
        selectedItems.forEach((item: any) => {
            const value = item[this.key];
            selectedText += '' === selectedText ? value
                : ', ' + value;
        });
        return selectedText;
    }
    private _getSelectedItems(): any[] {
        let selectedItems: any[];
        const buildItemsList = (items, itemList) => {
            itemList.map( (item) => {
                item.selected = false;
                items.map((selectPart) => {
                    if (item.name === selectPart.name) {
                        item.selected = selectPart.selected;
                    }
                });
            });
            return itemList;
        };

        const mapWithMultiLevel = (currentItem) => {
            let mapArr = [];
            let found = [];
            for (const item of this.items) {
                if (item.id === currentItem.id) {
                    found = item['children'];
                    break;
                }
            }
            if (this._commonUtil.isValidArray(found)) {
                mapArr = buildItemsList(found, currentItem['children']);
                found = [];
            } else {
                mapArr = currentItem['children'];
            }
            return mapArr;
        };

        if (this._commonUtil.isValidString(this.searchTerm)) {
            if (this.isMultiLevel) {
                // tslint:disable-next-line:prefer-for-of
                for (let i: number = 0 ; i < this.itemList.length; i ++) {
                    if (this._commonUtil.isValidArray(this.itemList[i]['children'])) {
                        const build = mapWithMultiLevel(this.itemList[i]);
                        if (this._commonUtil.isValidArray(build)) {
                            this.itemList[i]['children'] = build;
                        }
                    }
                }
                selectedItems = this.itemList;
            } else {
                if (this._commonUtil.isValidArray(this.items) &&
                    this._commonUtil.isValidArray(this.itemList)) {
                    selectedItems = buildItemsList(this.items, this.itemList);
                }
            }
        } else {
            selectedItems = this.items;
        }
        return selectedItems;
    }
    private _getSelectedText(): string {
        const selectedItems: any[] = this._commonUtil.getSelectedItems(this.items);
        this.selectedCount = selectedItems.length;
        if (this.selectedCount) {
            if (this.isMultiLevel) {
                const _selectedItems: any[] = [];
                this.items.map((item) => {
                    _selectedItems.push(...this._commonUtil.getSelectedItems(item.children));
                });
                return this._doGetSelectedText(_selectedItems);
            } else {
                return this._doGetSelectedText(selectedItems);
            }
        } else {
            return this.defaultValue;
        }
    }
    private _setDefaultSelectedItems(): void {
        if (
            !this._commonUtil.isValidArray(this.items) ||
            !this._commonUtil.isObject(this.items[0])
        ) {
            this.selectedText = this.defaultValue;
            return;
        }
        const selectedItems: any[] = this._commonUtil.getSelectedItems(this.items);
        this.selectedCount = selectedItems.length;
        this.selectedText = this._getSelectedText();
    }

    private _resetItemList(itemList: any[], action: boolean) {
        const isChildrenAvail = (item) => {
            return this._commonUtil.isDefined(item.children) &&
                this._commonUtil.isArray(item.children);
        };
        itemList.map((item) => {
            if (!this.isMultiLevel) {
                item.selected = action;
            }
            if (isChildrenAvail(item)) {
                for (const cat2 of item.children) {
                    cat2.selected = action;
                    if (isChildrenAvail(cat2)) {
                        for (const cat3 of cat2.children) {
                            cat3.selected = action;
                        }
                    }
                }
            }
        });
    }
}
