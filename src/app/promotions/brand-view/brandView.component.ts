/*****************************************************************
 * Proprietary & Confidential  |  © 2017 PhaseZero Ventures LLC  *
 * This is part of PhaseZero Ventures LLC and cannot be copied,  *
 * modified and/or distributed without the express permission of *
 * PhaseZero Ventures LLC                                        *
 *****************************************************************/
/**
 * @author: Arvind
 */

import { Component, OnDestroy, OnInit } from '@angular/core';
import { SubscriptionHistory } from '../../helper/subscriptionHistory.helper';
import { CommonUtilService } from '../../core/commonUtil.service';
import { PromotionService } from '../promotions.service';
import { TranslationService } from '../../core/translation.service';
import { BsModalRef } from 'ngx-bootstrap/modal/modal-options.class';
import { PartListModalComponent } from '../part-list-modal/partListModal.component';
import { BsModalService } from 'ngx-bootstrap/modal';
import { ExcBrandProdModalComponent } from '../exc-brand-prod-modal/excBrandProdModal.component';

@Component({
    selector: 'brand-view',
    templateUrl: './brandView.component.html',
    styleUrls: ['./brandView.style.scss']
})

export class BrandViewComponent extends SubscriptionHistory implements OnInit, OnDestroy {
    public defaultText: string;
    public isTemplateReady: boolean = false;
    public isPromoLoaded: boolean = false;
    public isExcludePartsAvail: boolean = false;
    public brandSelected: boolean = false;
    public brands: any[];
    constructor(
        private _commonUtil: CommonUtilService,
        private _promoSvc: PromotionService,
        private translationSvc: TranslationService,
        private _bsModalSvc: BsModalService
    ) {
        super();
        this.translationSvc
            .getTranslation('COMMON.SEL')
            .subscribe((defaultText: string) => {
                this.defaultText = defaultText;
            });
    }

    public ngOnInit() {
        this._subscribeEvents();
    }

    public ngOnDestroy() {
        this.unsubscribe();
    }
    public hasEditableField(fieldName: string) {
        return super._hasEditableField(fieldName);
    }

    public onBrandSelect(data = null): void {
        if (data &&
            data.item &&
            data.item.name
        ) {
            const prevSelectedItem: any = this._commonUtil.getFilteredItemWithIndex(
                this.brands,
                'name',
                data.item.name
            );
            if (prevSelectedItem) {
                this.brands[prevSelectedItem.index]['exclude'] = [];
            }
        }
        this._excludePratOnBrand();
    }
    public viewExclusionModal(): void {
        const custListModal: BsModalRef = this._bsModalSvc.show(ExcBrandProdModalComponent, {
            class: 'pilm-popup exc-prod-modal'
        });
        custListModal.content.setContent(this.brands);
        this._bsModalSvc
            .onHidden
            .subscribe(() => {
                custListModal.content._deleteActionFlag();
                this._excludePratOnBrand();
            });
    }
    public _excludePratOnBrand() {
        const selectedBrand = [];
        this.isExcludePartsAvail = false;
        for (const brand of this.brands) {
            if (brand.selected) {
                selectedBrand.push({
                    brand: brand.name,
                    exclude: brand.exclude
                });
                if (!this.isExcludePartsAvail) {
                    this.isExcludePartsAvail = this._commonUtil.isValidArray(brand.exclude);
                }
            }
        }
        this.brandSelected = this._commonUtil.isValidArray(selectedBrand);
        this._promoSvc.setTargetProdList('byBrand', selectedBrand);
    }
    public viewExcPartList(): void {
        if (
            !this._commonUtil.isValidArray(this.brands)
        ) {
            return;
        }
        let parts: any[] = [];
        this.brands.map((brand) => {
            if (this._commonUtil.isValidArray(brand.exclude)) {
                parts = [...parts, ...brand.exclude];
            }
        });
        if (
            !this._commonUtil.isValidArray(parts)
        ) {
            return;
        }
        let cls: string = 'pilm-popup part-list-modal';
        if (11 < parts.length) {
            cls += ' full-screen';
        }
        const partListMod: BsModalRef = this._bsModalSvc.show(PartListModalComponent, {
            class: cls
        });
        partListMod.content.setContent(parts);
    }
    private brandReset() {
        this.brandSelected = false;
    }
    private _subscribeEvents() {
        // Set tempalte ready flag
        this.subscriptions.push(
            this._promoSvc
                .onTemplateReady
                .subscribe((isTemplateReady: boolean) => {
                    if (!isTemplateReady) {
                        return;
                    }
                    this.setFormFields([
                        'BY_BRANDS'
                    ]);
                    this.isTemplateReady = isTemplateReady;
                })
        );
        this.subscriptions.push(
            this._promoSvc
                .onPromotionLoaded
                .subscribe((isPromoLoaded: boolean) => {
                    if (isPromoLoaded) {
                        this.setEditableFormFields(this._promoSvc.getEditableFields('rules'),
                            this._promoSvc.isPromoConfigEditable());
                        this.isPromoLoaded = isPromoLoaded;
                        this._promoSvc.currentStep = 2;
                        this.getBrands(); // Get the brands
                    }
                })
        );
    }
    private getBrands() {
        this._promoSvc
            .getBrandList()
            .subscribe((data: any) => {
                if (data &&
                    data.brands &&
                    this._commonUtil.isArray(data.brands)
                ) {
                    const brands = [];
                    for (const brand of data.brands) {
                        brands.push({
                            name: brand,
                            selected: false,
                            exclude: []
                        });
                    }
                    this.brands = brands;
                    this._setSelectedBrands();
                }
            });
    }
    private _doSetSelectedBrands(): boolean {
        const selectedBrands: any[] = this._promoSvc.getTargetProdList('byBrand');
        if (!this._commonUtil.isValidArray(selectedBrands)) {
            return false;
        }
        this.brandSelected = true;
        this.brands.forEach((brand: any) => {
            for (const selectBrand of selectedBrands) {
                if (brand.name === selectBrand.brand) {
                    brand.selected = true;
                    brand.exclude = selectBrand.exclude;
                    if (!this.isExcludePartsAvail) {
                        this.isExcludePartsAvail = this._commonUtil.isValidArray(brand.exclude);
                    }
                }
            }
        });
        return true;
    }
    private _setSelectedBrands() {
        if (
            !this.isPromoLoaded ||
            !this._commonUtil.isValidArray(this.brands)
        ) {
            return;
        }

        if (this._doSetSelectedBrands()) {
            return;
        }
        const promoDetails = this._promoSvc.getPromoDetails();
        if (
            !this._commonUtil.isObject(promoDetails)
        ) {
            return;
        }
        const targetProductsModel: any = promoDetails.targetProductsModel;
        if (!this._commonUtil.isObject(targetProductsModel)) {
            return;
        }
        this._promoSvc
            .setTargetProdList('byBrand', targetProductsModel.byBrand);
        this._doSetSelectedBrands();
    }
}
