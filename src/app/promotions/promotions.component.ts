/*****************************************************************
 * Proprietary & Confidential  |  © 2017 PhaseZero Ventures LLC  *
 * This is part of PhaseZero Ventures LLC and cannot be copied,  *
 * modified and/or distributed without the express permission of *
 * PhaseZero Ventures LLC                                        *
 *****************************************************************/
/**
 * @author: Irshadahmed
 */
import {
    Component,
    OnDestroy,
    OnInit,
    ViewChild
} from '@angular/core';
import {
    ActivatedRoute
} from '@angular/router';
import {
    CommonUtilService
} from '../core/commonUtil.service';
import {
    EventEmitterService
} from '../core/event-emitter';
import {
    NavigationService
} from '../core/navigation.service';
import {
    PromotionService
} from './promotions.service';
import {
    SubscriptionHistory
} from '../helper/subscriptionHistory.helper';
@Component({
    selector: 'promotion',
    templateUrl: './promotions.component.html',
    styleUrls: ['./promotions.component.scss']
})
export class PromotionComponent extends SubscriptionHistory implements OnDestroy {
    public completedStage: number;
    public currentStep: number;
    public isCreationFlow: boolean;
    public promoDraft: any;
    public selectedStepNo: number;
    public stepSettings: any;
    private _promoId: string;
    private _promoStatus: string;
    private _routerLink: string;
    private _templateId: string;
    constructor(
        private _activatedRoute: ActivatedRoute,
        private _commonUtil: CommonUtilService,
        private _eventEmitter: EventEmitterService,
        private _navigation: NavigationService,
        private _promoSvc: PromotionService
    ) {
        super();
        this._initView();
    }
    public ngOnDestroy() {
        this.unsubscribe();
    }
    public _initView() {
        this._templateId = this._navigation.getRouteParam('templateId');
        this._promoId = this._navigation.getRouteParam('promoId');
        const patterns: RegExp[] = this._activatedRoute.snapshot.data['promoRoutePatters'];
        this._promoSvc.setInitialPage(patterns);
        // Set template definition
        this._promoSvc
            .getTemplateDefinition(this._templateId)
            .subscribe((response) => {
                this._promoSvc
                    .getUpdateWorkFlow(
                        (data) => {
                            if (data) {
                                response['templateEditFields'] = data;
                            }
                            this._promoSvc.template = response;
                            this._promoSvc.disableGoalDetails =
                                'PROMOTION.NEW_PROD_LAUNCH' === response.templateName;
                            this._promoSvc.onTemplateReady.next(true);
                        });
                /*response['templateEditFields'] = {
                    headers: [
                        'END_DATE'
                    ],
                    rules: [
                        'BY_LIST',
                        'BY_CATEGORY'
                    ],
                    targets: [
                        'CUSTOMER'
                    ],
                    goals: [],
                    common: [],
                    approvalData: []
                };*/

            });
        this._setPromotionDraft();
        this._subscribeEvents();
    }
    public onStepChange(selectedStepNo: number): void {
        if (selectedStepNo === this.selectedStepNo) {
            return;
        }
        this.selectedStepNo = selectedStepNo;
        this._promoSvc.currentStep = selectedStepNo;
        this._setupBottomNav();
    }
    public setCurrentStep(button: any): void {
        this._routerLink = button.routerLink;
        this._promoStatus = button.action;
        this._eventEmitter
            .onValidatePromoDetails
            .emit(true);
    }
    private _navigateToNextStep(): void {
        const routerLink: string = this.stepSettings.currentRouterLink;
        const navUrl: string = this._commonUtil.replaceUrlParams(
            routerLink,
            {
                promotionId: this._promoId,
                templateId: this._templateId
            }
        );
        this._navigation.navigate(navUrl);
        this._fireEvents();
    }
    private _doSetCurrentStep(): void {
        let selectedStepNo: number = this.selectedStepNo;
        const currentStep = this._promoSvc.currentStep;
        if (4 > selectedStepNo) {
            selectedStepNo += 1;
        }
        if (currentStep < selectedStepNo) {
            this._promoSvc.currentStep = selectedStepNo;
            this.currentStep = selectedStepNo;
        }

        const navUrl: string = this._commonUtil.replaceUrlParams(
            this._routerLink,
            {
                promotionId: this._promoId,
                templateId: this._templateId
            }
        );
        this.onStepChange(selectedStepNo);
        this._navigation.navigate(navUrl);
        this._fireEvents();
    }
    private _fireEvents(): void {
        setTimeout(() => {
            this._promoSvc.onPromotionLoaded.next(true);
            this._promoSvc.onTemplateReady.next(true);
        }, 250);
    }
    private _setupBottomNav(): void {
        this.stepSettings = this._promoSvc.getStepSettingsByStepNo(this.selectedStepNo);
        if ('SAVED_AS_DRAFT' !== this.promoDraft.status) {
            // { text: "BUTTONS.SAVE_DRAFT", action: "SAVED_AS_DRAFT", disabled: false, isPrimary: false, inverse: true, routerLink: "/" }
            this.stepSettings.BUTTONS.map( (button) => {
                if (button.text === 'BUTTONS.SAVE_DRAFT') {
                    button.disabled = true;
                } else {
                    button.action = this.promoDraft.status;
                }
            });
        }

    }
    private _setPromotionDraft(): void {
        /* const promoDraft = this._promoSvc.promotionDraft;
        if (promoDraft && promoDraft.promoId === this._promoId) {
            this.promoDraft = promoDraft;
            this._setupBottomNav();
            this._eventEmitter.onPromotionLoaded.emit(promoDraft);
        } else {
            this._promoSvc
            .getPromotionById(this._promoId)
            .subscribe((response) => {
                this.promoDraft = promoDraft;
                this._promoSvc.promotionDraft = response;
                this._setupBottomNav();
                this._eventEmitter.onPromotionLoaded.emit(response);
            });
        } */
        this._promoSvc
            .getPromotionById(this._promoId)
            .subscribe((response) => {
                // Setup Navigation settings based on current page
                const stage: string = response.stage;
                this._promoSvc.promoStageObj = {_promoStatus: response.status, _promoStage: stage};
                let currentStep: number;
                this.completedStage = this._promoSvc.getCurrentStepByStage(stage);
                const initialPageNo: number = this._promoSvc.initialPageNo;
                let navigate: boolean = false;
                if (initialPageNo > this.completedStage) {
                    currentStep = this.completedStage + 1;
                    if (initialPageNo !== currentStep) {
                        navigate = true;
                    }
                    this.selectedStepNo = currentStep;
                    this._promoSvc.currentStep = currentStep;
                } else if (initialPageNo < this.completedStage) {
                    currentStep = initialPageNo;
                    this._promoSvc.currentStep = currentStep;
                    this.selectedStepNo = initialPageNo;
                } else {
                    currentStep = this.completedStage;
                    this._promoSvc.currentStep = currentStep;
                    this.selectedStepNo = currentStep;
                }
                this.currentStep = this._promoSvc.currentStep;

                if (navigate) {
                    this._navigateToNextStep();
                }
                this.promoDraft = response;
                this._promoSvc.promotionDraft = response;
                this._promoSvc.onPromotionLoaded.next(true);
                this._setupBottomNav();
            });
    }
    private _subscribeEvents(): void {
        this.subscriptions.push(
            this._eventEmitter
                .onPromoSave
                .subscribe((response) => this._updatePromotion())
        );
        this.subscriptions.push(
            this._eventEmitter
                .onToggleActions
                .subscribe((isDisabled) => this._toggleActionButtons(isDisabled))
        );
    }
    private _toggleActionButtons(disabled: boolean): void {
        this.stepSettings
            .BUTTONS
            .forEach((button: any) => {
                button.disabled = disabled;
            });
    }
    private _updatePromotion(): void {
        if (this.completedStage < this.selectedStepNo) {
            this.completedStage = this.selectedStepNo;
        }
        this._promoSvc.completedStage = this.completedStage;
        this._promoSvc
            .updatePromoDetails(this._promoStatus)
            .subscribe((response: any) => {
                if (response.errorCode) {
                    this._eventEmitter
                        .onAlert
                        .emit({
                            type: 'alert-warning',
                            msgKey: 4 === this.selectedStepNo ?
                                'PROMOTION.ERROR.FAILED' :
                                'PROMOTION.ERROR.UPDATING'
                        });
                } else {
                    this._promoSvc.promotionDraft = response;
                    this._doSetCurrentStep();
                }
            });
    }
}
