/*****************************************************************
 * Proprietary & Confidential  |  © 2017 PhaseZero Ventures LLC  *
 * This is part of PhaseZero Ventures LLC and cannot be copied,  *
 * modified and/or distributed without the express permission of *
 * PhaseZero Ventures LLC                                        *
 *****************************************************************/
/**
 * @author: Irshadahmed
 */

import { ChangeDetectorRef, Component, ViewEncapsulation } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal/modal-options.class';
import { PromotionService } from '../promotions.service';
import { CommonUtilService } from '../../core/commonUtil.service';
import { getResponseURL } from '@angular/http/src/http_utils';

@Component({
    selector: 'exclude-product-modal',
    templateUrl: './excBrandProdModal.component.html',
    styleUrls: ['./excBrandProdModal.style.scss'],
    encapsulation: ViewEncapsulation.None
})
export class ExcBrandProdModalComponent {
    public searchTerm: string;
    public totalItems: number;
    public currentPage: number = 1;
    public readonly itemsPerPage: number = 10;
    public readonly maxSize: number = 10;
    public pager: string;
    public parts: any[];
    public categories: any[];
    public selectedBrand: any;
    constructor(
        public bsModalRef: BsModalRef,
        private _commonUtil: CommonUtilService,
        private _promoSvc: PromotionService,
        private _ref: ChangeDetectorRef
    ) {
    }
    public closeModal(commitChanges): void {
        this.bsModalRef.hide();
        if (!this._commonUtil.isValidArray(this.parts)) {
            return;
        }
        this._deleteActionFlag(commitChanges);
    }

    public setContent(brands): void {
        const selectedBrands = this._commonUtil.getSelectedItems(brands);
        this.categories = [];
        if (!this._commonUtil.isValidArray(selectedBrands)) {
            return;
        }
        this.categories = selectedBrands;
        this.setBrandParts(selectedBrands[0]);
    }
    public setBrandParts(selectedBrand) {
        if (
            !this._commonUtil.isObject(selectedBrand)
        ) {
            return;
        }
        this.selectedBrand = selectedBrand;
        this.currentPage = 1;
        this._ref.detectChanges();
        this.getParts();
    }
    public getParts(): void {
        this._promoSvc.getBrandProductList(
            ((this.currentPage - 1) * this.itemsPerPage),
            this.itemsPerPage,
            this.selectedBrand,
            this.searchTerm
        )
            .subscribe((response: any): void => {
                const parts: any[] = response.parts;
                if (this._commonUtil.isValidArray(parts)) {
                    this._setPartSelection(parts);
                    this.parts = parts;
                } else {
                    this.parts = [];
                }
                this.totalItems = response.totalResults || parts.length;
                this._setPager();
            });
    }
    public setPage(pageNo: number): void {
        this.currentPage = pageNo;
    }
    public pageChanged(event: any): void {
        this.currentPage = event.page;
        this.getParts();
    }
    public resetSearchTerm(): void {
        this.searchTerm = '';
        this.getParts();
    }
    public searchParts(): void {
        this._commonUtil
            .delayCallback('searchingParts', () => {
                this.currentPage = 1;
                this.getParts();
            }, 300);
    }
    public togglePartSelection(part: any) {
        if (!this._commonUtil.isObject(part)) {
            return;
        }
        if (part.selected) {
            this.deSelectPart(part, true);
        } else {
            this.selectPart(part, true);
        }
    }
    private selectPart(part: any, toggleAction?: boolean): void {
        const cat = this._getSelectedCategory();
        if (!this._commonUtil.isValidArray(cat['exclude'])) {
            cat['exclude'] = [];
        }
        cat['exclude'].push({
            productLine: '',
            partNumber: part.partNumber,
            shortDescription: part.partDesc
        });
        part.selected = true;
        if (toggleAction) {
            this.toggleActionFlag(part);
        }
    }
    private deSelectPart(part: any, toggleAction?: boolean): void {
        const cat = this._getSelectedCategory();
        const partDetails: any = this._commonUtil
            .getFilteredItemWithIndex(cat
                    .exclude,
                'partNumber',
                part.partNumber);
        if (!this._commonUtil.isObject(partDetails)) {
            return;
        }
        cat
            .exclude
            .splice(partDetails.index, 1);
        part.selected = false;
        if (toggleAction) {
            this.toggleActionFlag(part);
        }
    }
    private _setPager(): void {
        let toCount: number = this.currentPage * this.itemsPerPage;
        toCount = toCount < this.totalItems ? toCount : this.totalItems;
        this.pager = ((this.currentPage - 1) * 10) + ' - ' + toCount;
    }
    private _setPartSelection(parts: any[]): void {
        if (!this._commonUtil.isValidArray(parts)) {
            return;
        }
        const cat = this._getSelectedCategory();
        parts.forEach((part: any) => {
            const item: any = this._commonUtil
                .getFilteredItem(
                    cat['exclude'],
                    'partNumber',
                    part.partNumber
                );
            if (this._commonUtil.isObject(item)) {
                part.selected = true;
            }
        });
    }
    private _getSelectedCategory() {
        for (const cat of this.categories) {
            if (cat.name === this.selectedBrand.name) {
                return cat;
            }
        }
    }
    private _deleteActionFlag(commitChanges?: boolean): void {
        this.parts
            .forEach((part: any): void => {
                if (commitChanges) {
                    delete part.actioned;
                    return;
                }
                if (part.actioned) {
                    if (part.selected) {
                        this.deSelectPart(part);
                    } else {
                        this.selectPart(part);
                    }
                    delete part.actioned;
                }
            });
    }
    private toggleActionFlag(part: any): void {
        if (part.actioned) {
            delete part.actioned;
        } else {
            part.actioned = true;
        }
    }
}
