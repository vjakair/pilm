/*****************************************************************
 * Proprietary & Confidential  |  © 2017 PhaseZero Ventures LLC  *
 * This is part of PhaseZero Ventures LLC and cannot be copied,  *
 * modified and/or distributed without the express permission of *
 * PhaseZero Ventures LLC                                        *
 *****************************************************************/
/**
 * @author: Irshadahmed
 */
import {
    Component,
    ChangeDetectorRef,
    OnInit,
    OnDestroy,
    ViewChild
} from '@angular/core';
import {
    NgForm
} from '@angular/forms';

import {
    CommonUtilService
} from '../../core/commonUtil.service';
import {
    EventEmitterService
} from '../../core/event-emitter';
import {
    PromotionService
} from '../promotions.service';
import {
    SubscriptionHistory
} from '../../helper/subscriptionHistory.helper';
import {
    TitleService
} from '../../core/title';
@Component({
    selector: 'goals',
    templateUrl: './goals.component.html',
    styleUrls: ['./goals.style.scss']
})
export class GoalsComponent extends SubscriptionHistory implements OnInit, OnDestroy {
    public disableGoalDetails: boolean;
    public isTemplateReady: boolean = false;
    public awarenessOnly: boolean = false;
    public promoDetails: any = {

    };
    @ViewChild('promoFrm')
    public promoFrm: NgForm;
    constructor(
        private _commonUtil: CommonUtilService,
        private _eventEmitter: EventEmitterService,
        private _promoSvc: PromotionService,
        private _ref: ChangeDetectorRef,
        titleSvc: TitleService
    ) {
        super();
        titleSvc.setTitle('GOALS');
    }
    public ngOnInit() {
        this._subscribeEvents();
    }
    public ngOnDestroy() {
        this.unsubscribe();
    }
    public hasFormField(fieldName: string) {
        return super._hasFormField(fieldName);
    }
    public hasEditableField(fieldName: string) {
        return super._hasEditableField(fieldName);
    }
    public toggleGoals(isAwarenessOnly: boolean): void {
        this.awarenessOnly = this.disableGoalDetails ? true : isAwarenessOnly;
        this._ref.detectChanges();
    }
    private _subscribeEvents() {
        // Set tempalte ready flag
        this.subscriptions.push(
            this._promoSvc
                .onTemplateReady
                .subscribe((isTemplateReady: boolean) => {
                    if (!isTemplateReady) {
                        return;
                    }
                    const templateFields: any[] = this._promoSvc.getTemplateFields('goals');
                    this.setFormFields(templateFields);
                    this.isTemplateReady = isTemplateReady;
                    const isDisabled = this._promoSvc.disableGoalDetails;
                    if (isDisabled) {
                        this.disableGoalDetails = isDisabled;
                        this.awarenessOnly = true;
                    }
                    this._toggleAwarenessDetails();
                })
        );
        this.subscriptions.push(
            this._promoSvc
                .onPromotionLoaded
                .subscribe((isPromoLoaded: boolean) => {
                    if (isPromoLoaded) {
                        this.setEditableFormFields(this._promoSvc.getEditableFields('rules'),
                            this._promoSvc.isPromoConfigEditable());
                        this._setPromoDetails();
                    }
                })
        );
        // Validate promotion form if save button is clicked
        this.subscriptions.push(
            this._eventEmitter
                .onValidatePromoDetails
                .subscribe(() => this._validatePromoDetails())
        );
    }
    private _validatePromoDetails() {
        const valid: boolean = this.promoFrm.valid;
        if (!this.awarenessOnly && !valid) {
            this._commonUtil.setFormDirty(this.promoFrm.controls);
            return;
        }
        if (this.awarenessOnly) {
            this.promoDetails = {
                goalDetails: false,
                awarenessOnly: true
            };
        } else {
            this.promoDetails.goalDetails = true;
            this.promoDetails.awarenessOnly = false;
        }
        this._promoSvc.updateLocalPromoDraft(this.promoDetails);
        this._eventEmitter
            .onPromoSave
            .emit(true);
    }
    private _toggleAwarenessDetails():void {
        this.awarenessOnly = this.disableGoalDetails ? true : this.promoDetails.awarenessOnly; 
    }
    private _setPromoDetails(): void {
        this._promoSvc.currentStep = 1;
        const promoDetails = this._promoSvc.getPromoDetails();
        if (!promoDetails) {
            return;
        }
        for (const key in promoDetails) {
            if (!promoDetails.hasOwnProperty(key)) {
                continue;
            }
            this.promoDetails[key] = promoDetails[key];
        }
        this._toggleAwarenessDetails();
    }
}
