/*****************************************************************
 * Proprietary & Confidential  |  © 2017 PhaseZero Ventures LLC  *
 * This is part of PhaseZero Ventures LLC and cannot be copied,  *
 * modified and/or distributed without the express permission of *
 * PhaseZero Ventures LLC                                        *
 *****************************************************************/
/**
 * @author: Irshadahmed
 */
// Import Standard Services
import {
    Injectable
} from '@angular/core';
import {
    Http
} from '@angular/http';
import {
    Observable
} from 'rxjs/Rx';
import {
    Params
} from '@angular/router';
import {
    BehaviorSubject
} from 'rxjs/BehaviorSubject';
import * as $ from 'jquery';
// Import Custom Services
import {
    ApiDispatcherService
} from '../core/apiDispatcher.service';
import {
    AppConfig
} from '../config/app.config';
import {
    AuthService
} from '../auth/auth.service';
import {
    CommonUtilService
} from '../core/commonUtil.service';
import {
    LocalStorageService
} from '../helper/localStorage.helper';
import {
    NavigationService
} from '../core/navigation.service';
import {
    PromotionSettings
} from '../core/promotionSettings.provider';
@Injectable()
export class PromotionService extends LocalStorageService {
    public onPromotionLoaded: BehaviorSubject<boolean> = new BehaviorSubject(false);
    public onTemplateReady: BehaviorSubject<boolean> = new BehaviorSubject(false);
    protected _template: any;
    protected _initialPageNo: number;
    private _approverGroups: any[];
    private readonly _promoImagesPath: any = {
        wait: 'waiting.png',
        launch: 'launched.png',
        complete: 'completed.png',
        draft: 'draft.png',
        pending: 'pending.png',
        reject: 'rejected.png',
        archive: 'archived.png'
    };
    private _currentStep: number;
    private _completedStage: number;
    private _disableGoalDetails: boolean;
    private _promotionDraft: any;
    private _targetProdList: any = {};
    private _orderList: any;
    private _promoStageObj: {_promoStatus?: string, _promoStage?: string} = {};
    set currentStep(step: number) {
        this._currentStep = step;
    }
    get currentStep(): number {
        return this._currentStep;
    }
    set completedStage(step: number) {
        this._completedStage = step;
    }
    get completedStage(): number {
        return this._completedStage;
    }
    get disableGoalDetails(): boolean {
        return this._disableGoalDetails;
    }
    set disableGoalDetails(disable: boolean) {
        this._disableGoalDetails = disable;
    }
    get promotionStages(): any[] {
        return this._promotionSettings.PROMOTION_STAGES;
    }
    get promotionType(): string {
        return this.commonUtil.isObject(this._promotionDraft) ?
            this._promotionDraft.promoType : undefined;
    }
    get promotionDraft(): any {
        return this._promotionDraft;
    }
    set promotionDraft(promoDraft: any) {
        this._promotionDraft = promoDraft;
    }
    get initialPageNo(): number {
        return this._initialPageNo;
    }
    get categoryConfig(): any {
        return this._promotionSettings.CategoryConfig;
    }
    get targetListOption(): string {
        return this._targetProdList.option;
    }
    set template(template: any) {
        this._template = template;
    }
    set promoStageObj(promoStageObj: any) {
        this._promoStageObj = promoStageObj;
    }
    get promoStageObj(): any {
        return this._promoStageObj;
    }
    constructor(
        private _apiDispatcher: ApiDispatcherService,
        private _appConfig: AppConfig,
        private _authService: AuthService,
        private _commonUtil: CommonUtilService,
        private _promotionSettings: PromotionSettings,
        private _navigationSvc: NavigationService,
        private _http: Http,
    ) {
        super(_commonUtil);
    }
    public isPromoEditable(): boolean {
        const promoStage = this._commonUtil.getClonedObject(this.promoStageObj);
        let isEditable = true;
        if (this._commonUtil.isObject(promoStage) &&
            this._commonUtil.isValidString(promoStage._promoStatus)
        ) {
            isEditable = !(promoStage._promoStatus === 'COMPLETED' ||
                promoStage._promoStatus === 'SAVED_AS_DRAFT' ||
                promoStage._promoStatus === 'REJECTED'
            );
        }
        return isEditable;
    }
    public isPromoConfigEditable(): boolean {
        const promoStage = this._commonUtil.getClonedObject(this.promoStageObj);
        let isEditable = false;
        if (this._commonUtil.isObject(promoStage) &&
            this._commonUtil.isValidString(promoStage._promoStatus)
        ) {
            isEditable = (promoStage._promoStatus === 'LAUNCHED');
        }
        return isEditable;
    }
    public getUpdateWorkFlow(callback) {
        const staticUrl = `${this._appConfig.getProtocol()}//${this._appConfig.getCdnBaseUrl()}/menu/promotion-template-update.json`;
        $.ajax({
            type: 'GET',
            url: staticUrl,
            cache: true,

            success: (res) => {
                return callback(res);
            },
            error: () => {
                return callback();
            }
        });
    }
    public createPromotionDraft(promotionType: string, tplId: string): Observable<any> {
        return this._apiDispatcher
            .doPostApiCall('promoDraft', {
                promoType: promotionType,
                templateId: tplId
            });
    }
    public disableStepButtons() {
        this._promotionSettings
            .STEPS.forEach((step) => {
            if (!this._commonUtil.isObject(step)) {
                return;
            }
            step.BUTTONS.forEach((button: any) => {
                if (!this._commonUtil.isObject(button)) {
                    return;
                }
                button.disabled = true;
            });
        });
    }
    public getCustomerList(
        buyingGroupCodes: string[],
        channelCodes: string[],
        lobCodes: string[],
        rtbCodes: string[],
        contactTypes: string[],
        searchPattern: string
    ): Observable<any> {
        return this._apiDispatcher
            .doPostApiCall(
                'customerList',
                {
                    buyingGroup: buyingGroupCodes,
                    channel: channelCodes,
                    lineofBusiness: lobCodes,
                    rtbCode: rtbCodes,
                    customer: '',
                    contactType: contactTypes
                },
                {
                    search: searchPattern
                }, null, null, null,
                {
                    pageSize: 20,
                    currentPage: 1
                }
            );
    }
    public getLobList(cache?: boolean): Observable<any> {
        return this._apiDispatcher
            .doGetApiCall('lobListTree', undefined, undefined, cache);
    }
    public getMetadata(): Observable<any> {
        return this._apiDispatcher
            .doGetApiCall('metadata');
    }
    public getParts(
        frm: number,
        itemsPerPage: number,
        lobId: number,
        classId: number,
        prodLineId: number,
        query?: string
    ): Observable<any> {
        return this._apiDispatcher
            .doPostApiCall('parts', {
                    q: this.commonUtil.isValidString(query) ? query : null,
                    from: frm, // starts from 0
                    size: itemsPerPage,
                    cats: [
                        lobId,
                        classId,
                        prodLineId
                    ]
                }
            );
    }
    public getPartsByListId(id: string): Observable<any> {
        return this._apiDispatcher
            .doGetApiCall('listParts', {
                listId: id
            });
    }
    public getPromoDetails(): any {
        const promoStage: any = this._promotionSettings.STEPS[this.currentStep];
        const promotionDraft: any = this.promotionDraft;
        return this.commonUtil.isObject(promoStage) && this.commonUtil.isObject(promotionDraft) ?
            this.promotionDraft[promoStage.id] : undefined;
    }
    public getPromoNotes(promotionId: string): Observable<any> {
        if (!this.commonUtil.isValidString(promotionId)) {
            return;
        }
        return this._apiDispatcher
            .doGetApiCall(
                'promoNotes',
                {
                    promoId: promotionId
                }
            );
    }
    public getPromotionById(
        promotionId: string,
        notificationId?: string,
        cached?: boolean
    ): Observable<any> {
        let apiKey: string;
        const queryParam: any = {
            promoId: promotionId
        };
        if (this.commonUtil.isValidString(notificationId)) {
            queryParam.id = notificationId;
            apiKey = 'promoNotification';
        }
        return this._doGetPromotions(
            queryParam,
            cached,
            apiKey
        );
    }
    public getPromotionsByStatus(promoStatus: string, cached?: boolean, page: number = 1, size: number = 20): Observable<any> {
        return this._doGetPromotions(
            {
                status: promoStatus,
                page: page - 1,
                size: size,
            },
            cached
        );
    }
    public getPendingPromotions(): Observable<any> {
        return this._apiDispatcher
            .doGetApiCall('pendingTasks');
    }
    public getPromoSnaps(cached?: boolean): Observable<any> {
        return this._apiDispatcher.doGetApiCall(
            'snapshot',
            null,
            null,
            cached
        );
    }
    public getPromoPendingTasks(cached?: boolean): Observable<any> {
        return this._apiDispatcher.doGetApiCall(
            'dummy',
            null,
            null,
            cached
        );
    }
    public getPromoImagePath(key: string): string {
        return this._promoImagesPath[key];
    }
    public getPromoTemplates(templateId?: string, cached?: boolean): Observable<any> {
        return this._apiDispatcher.doGetApiCall(
            this._commonUtil.isValidString(templateId) ? 'template' : 'templates',
            null,
            null,
            cached
        );
    }
    public getRecentActivities(cached?: boolean): Observable<any> {
        return this._apiDispatcher.doGetApiCall(
            'recentActivities',
            null,
            null,
            cached
        );
    }
    public getStepSettingsByStepId(stepId: string): any {
        if (!this._commonUtil.isValidString(stepId)) {
            return;
        }
        return this._commonUtil.getFilteredItem(
            this._promotionSettings.STEPS,
            'title',
            stepId
        );
    }
    public getStepSettingsByStepNo(stepNo: number): any {
        let stepSettings: any = this._promotionSettings.STEPS[stepNo];
        if (!this.commonUtil.isObject(stepSettings)) {
            stepSettings = {};
        }
        return this.commonUtil.getClonedObject(stepSettings);
    }
    public getTargetProdList(option: string): any[] {
        if (!this.commonUtil.isValidString(option)) {
            return undefined;
        }
        return this._targetProdList[option];
    }
    public getTemplateDefinition(tplId: string, cached: boolean = false): Observable<any> {
        return this._apiDispatcher.doGetApiCall('template', {
            templateId: tplId
        }, cached);
    }
    public getTemplateFields(stageId: string): any[] {
        try {
            return this._template.templateFields[stageId];
        } catch (e) {
            return [];
        }
    }
    public getEditableFields(stageId: string): any[] {
        try {
            return this._template.templateEditFields[stageId];
        } catch (e) {
            return [];
        }
    }
    public hasField(stageId: string, fieldName: string): boolean {
        if (!this.commonUtil.isValidString(stageId) || !this.commonUtil.isValidString(fieldName)) {
            Observable.throw('Invalid stage/field id!');
        }
        try {
            const fields: string[] = this._template.templateFields[stageId];
            return -1 !== fields.indexOf(fieldName);
        } catch (e) {
            console.log('Template definition not found!');
            Observable.throw(e);
        }
    }
    public setPromoApprovers(promotions: any[]): Observable<any> {
        return new Observable((subscriber): any => {
            if (!this.commonUtil.isValidArray(promotions)) {
                subscriber.next(promotions);
                subscriber.complete();
                return;
            }
            this.getApproverGroupData()
                .subscribe((response: any): void => {
                    if (response.errorCode) {
                        subscriber.next(promotions);
                        subscriber.complete();
                        return;
                    }
                    const selectedApprovers: any[] = this.getSelectedApprovers();
                    promotions.forEach((promotion: any): void => {
                        if (
                            !this.commonUtil.isValidString(promotion.groupName) ||
                            !this.commonUtil.isObject(promotion)
                        ) {
                            promotion.approver = {};
                            return;
                        }
                        const selectedApprover: any = this.commonUtil
                            .getFilteredItem(selectedApprovers, 'groupId', promotion.groupName);
                        promotion.approver = selectedApprover;
                    });
                    subscriber.next(promotions);
                    subscriber.complete();
                });
        });
    }
    public setInitialPage(patterns: any[]): void {
        const targetPageUrl: string = this._navigationSvc.targetPageUrl;
        if (!this._commonUtil.isValidArray(patterns)) {
            return;
        }
        for (const i in patterns) {
            if (this._commonUtil.isUnDefined(patterns[i])) {
                continue;
            }
            const pattern: RegExp = patterns[i];
            if (pattern.test(targetPageUrl)) {
                this._initialPageNo = parseInt(i, 0);
                return;
            }
        }
        this._initialPageNo = 0;
    }
    public getCurrentStepByStage(stage: string): number {
        const currentStep: number = this._promotionSettings
            .PROMOTION_STAGES.indexOf(stage);
        return -1 === currentStep ? 0 : currentStep;
    }
    public performSelectedAction(
        actionType: string,
        promoTaskId: string,
        promotionId: string,
        reason: string
    ) {
        const apiKey: string = 'archived' === actionType ? 'archivedPromo' : 'action';
        const reqPayload: any[] = [
            {
                action: 'archived' === actionType ? 'ARCHIVED' : actionType,
                note: reason,
                promoId: promotionId,
                taskId: promoTaskId
            }
        ];
        return this._apiDispatcher
            .doPostApiCall(apiKey, reqPayload);
    }

    public getSelectedApprovers(): any[] {
        if (!this.commonUtil.isValidArray(this._approverGroups)) {
            return;
        }
        const selectedApprovers: any[] = [];
        this._approverGroups
            .forEach((approverGroup: any): void => {
                if (!this.commonUtil.isObject(approverGroup)) {
                    return;
                }
                const selectedApprover: any = this.commonUtil
                    .getFilteredItem(approverGroup.groupUsers, 'selected', true);
                if (this.commonUtil.isObject(selectedApprover)) {
                    selectedApprovers.push({
                        name: selectedApprover.name,
                        groupId: approverGroup.groupId,
                        email: selectedApprover.emailId
                    });
                }
            });
        return selectedApprovers;
    }

    public getOrderList(cache?: boolean): Observable<any> {
        return new Observable((observer): any => {
            if (cache && this.commonUtil.isValidArray(this._orderList)) {
                observer.next(this._orderList);
                observer.complete();
            } else {
                this._apiDispatcher
                    .doPostApiCall('orderList', {
                        cartType: 'ORDER_LIST',
                        countReq: 1,
                        latestOnTop: 1
                    })
                    .subscribe((response: any) => {
                        const orderList: any[] = response.orderList;
                        if (this.commonUtil.isValidArray(orderList)) {
                            this._orderList = orderList;
                        }
                        observer.next(orderList);
                        observer.complete();
                    });
            }
        });
    }

    public savePromoNote(promotionId: string, promoNote: string): Observable<any> {
        return this._apiDispatcher
            .doPostApiCall(
                'saveNote',
                {
                    action: 'NOTES_ADDED',
                    note: promoNote,
                    promoId: promotionId
                }
            );
    }

    public getApproverGroupData(): Observable<any> {
        return new Observable((subscriber: any) => {
            if (this.commonUtil.isObject(this._approverGroups)) {
                subscriber.next(this._approverGroups);
                subscriber.complete();
                return;
            }
            this.setApproverGroupData()
                .subscribe(() => {
                    subscriber.next(this._approverGroups);
                    subscriber.complete();
                }, (error) => {
                    subscriber.error(error);
                    subscriber.complete();
                });
        });
    }

    public setApproverGroupData(): Observable<void> {
        return new Observable((subscriber: any) => {
            const promoApprover: any = this._appConfig.getAppFeatureSettings('PromotionApprovers');
            if (!this._commonUtil.isObject(promoApprover)) {
                return;
            }
            const promoApproverGroups: any = promoApprover.settings;
            const userRoles: string[] = Object.keys(promoApproverGroups);
            this._authService
                .getUsersListByRoles(userRoles)
                .subscribe((response: any) => {
                    this._doSetApproverGroups(promoApproverGroups, response);
                    subscriber.next();
                    subscriber.complete();
                }, (error: any) => {
                    subscriber.error(error);
                });
        });
    }

    public setPromoTemplatesType(templates: any[]): void {
        const templateSettings = this._promotionSettings.templateSettings;
        templates.forEach((template: any) => {
            const tplSettings = this._commonUtil
                .getFilteredItem(
                    templateSettings,
                    'templateName',
                    template.templateName
                );
            template.type = tplSettings.type;
        });
    }
    public setTargetProdList(option: string, data: any[]): void {
        if (
            -1 === ['byCategory', 'byList', 'byBrand'].indexOf(option)
        ) {
            return;
        }
        this._targetProdList.option = option;
        this._targetProdList[option] = data;
    }
    public resetTargetProdList(): void {
        this._targetProdList = {};
    }
    public unSetOrderList(): void {
        this._orderList = undefined;
    }
    public updateLocalPromoDraft(promoDetails: any, stgComplete?: boolean): void {
        const promoStage: any = this._promotionSettings.STEPS[this.currentStep];
        this._promotionDraft[promoStage.id] = promoDetails;
        this._promotionDraft.stgComplete = stgComplete;
    }
    public getUpdateLobListTree(card: any, lobList: any[]): void {
        const lobItem: any = this.commonUtil.getFilteredItem(
            lobList,
            'code',
            card.lob
        );
        if (
            !this.commonUtil.isObject(lobItem) ||
            !this.commonUtil.isValidArray(lobItem.children)
        ) {
            return;
        }
        lobItem.selected = true;
        for (const item of lobItem.children) {
            if (!this.commonUtil.isObject(item)) {
                continue;
            }
            if (card.productClass === item['code']) {
                item.selected = true;
                const prodLines: any[] = item.children;
                if (!this.commonUtil.isValidArray(prodLines)) {
                    return;
                }
                prodLines.forEach((prodLine: any): void => {
                    if (-1 === card.productLines.indexOf(prodLine['code'])) {
                        return;
                    }
                    prodLine.selected = true;
                });
            }
        }
    }

    /**
     * Building category based data
     * @param lobList
     * @param defaultData
     */
    public getCreateLobListTree(lobList: any[], defaultData = true): void {
        lobList.map((lob) => {
            lob.selected = defaultData;
            lob.children.map((cat2) => {
                cat2.selected = defaultData;
                cat2.children.map((cat3) => {
                    cat3.selected = defaultData;
                });
            });
        });
    }

    public updatePromoDetails(promoStatus: string) {
        const stage: string = this._promotionSettings.PROMOTION_STAGES[this.completedStage];
        this._promotionDraft.stage = stage;
        this.promotionDraft.status = promoStatus;
        this.promotionDraft.stgComplete = true;
        return this._apiDispatcher
            .doPutApiCall('promoDraft', this._promotionDraft);
    }
    public uploadFile(file: File, promotionId: string): Observable<any> {
        return this._apiDispatcher
            .uploadFile('uploadFile', file, { promoId: promotionId });
    }
    public uploadCustomerFile(file: File, promotionId: string): Observable<any> {
        return this._apiDispatcher
            .uploadFile('uploadCustomerFile', file, { promoId: promotionId });
    }
    public downloadCustomerFile(
        buyingGroupCodes: string[],
        channelCodes: string[],
        lobCodes: string[],
        rtbCodes: string[],
        contactTypes: string[],
    ): Observable<any> {
        return this._apiDispatcher
            .doPostApiCall(
                'downloadCustomerFile',
                {
                    buyingGroup: buyingGroupCodes,
                    channel: channelCodes,
                    lineofBusiness: lobCodes,
                    rtbCode: rtbCodes,
                    customer: '',
                    contactType: contactTypes
                },
                '',
                '',
                '',
                'arraybuffer'
            );
    }
    public validatePromoCode(promotionCode: string): Observable<any> {
        return this._apiDispatcher.doPostApiCall(
            'validatePromoCd',
            {
                promoCode: promotionCode
            },
            null
        );
    }

    public getNotificationList(
        searchTerm: string,
        queryParam: Params,
        cache?: boolean
    ): Observable<any> {
        return this._apiDispatcher
            .doGetApiCall(this._commonUtil.isValidString(searchTerm)
                ? 'filteredNotificationList' : 'allNotificationList',
                this._commonUtil.isValidString(searchTerm) ?
                    { searchByText: searchTerm } : undefined,
                queryParam,
                cache
            );
    }

    public getExcludeParts(catCard): any[] {
        let parts: any[] = [];
        if (this._commonUtil.isValidArray(catCard)) {
            for (const cat1 of catCard) {
                if (cat1.selected && this._commonUtil.isValidArray(cat1.children)) {
                    for (const cat2 of cat1.children) {
                        if (cat2.selected && this._commonUtil.isValidArray(cat2.exclude)) {
                            parts = [...parts, ...cat2.exclude];
                        }
                    }
                }
            }
        }
        return parts;
    }

    public getBrandList(): Observable<any> {
        return this._apiDispatcher
            .doGetApiCall('brandList',
                null,
                null,
                false
            );
    }

    public getBrandProductList(
        frm: number,
        itemsPerPage: number,
        brand: any,
        query?: string
    ): Observable<any> {
        return this._apiDispatcher
            .doPostApiCall('brandPartList', {
                    q: this.commonUtil.isValidString(query) ? query : null,
                    from: frm,
                    size: itemsPerPage,
                    cats: [0, 0, 0],
                    filter: [
                        {
                            name: 'Brand',
                            type: 'STRING',
                            values: [brand.name]
                        }
                    ]
                }
            );
    }

    private _doGetPromotions(
        queryParam: Params,
        cached: boolean,
        apiKey?: string
    ): Observable<any> {
        return this._apiDispatcher.doGetApiCall(
            apiKey || 'promotions',
            null,
            queryParam,
            cached
        );
    }

    private _doSetApproverGroups(promoApproverGroups, promoApproverGroupUsers: any): void {
        if (!this._commonUtil.isObject(promoApproverGroups)) {
            return;
        }
        // this._promoApproverGroups = promoApproverGroups;
        const approverGroups: any[] = [];
        for (const prop in promoApproverGroups) {
            if (!promoApproverGroups.hasOwnProperty(prop)) {
                continue;
            }
            const grpUsers: any[] = promoApproverGroupUsers[prop];
            const promoApproverGroup: any = promoApproverGroups[prop];
            const userId: string = this.commonUtil.isValidArray(promoApproverGroup) ?
                promoApproverGroup[0] : undefined;
            if (this.commonUtil.isValidString(userId)) {
                this._setSelectedApprover(grpUsers, userId);
            }
            approverGroups.push({
                groupId: prop,
                groupUsers: grpUsers
            });
        }
        this._approverGroups = approverGroups;
    }

    private _setSelectedApprover(groupUsers: any[], userId: string): any {
        let selectedApprover: any;
        for (const groupUser of groupUsers) {
            if (!this._commonUtil.isObject(groupUser)) {
                continue;
            }
            groupUser.name = groupUser.firstName + ' ' + groupUser.lastName;
            if (userId === groupUser.userId) {
                selectedApprover = groupUser;
                selectedApprover.selected = true;
            }
        }
        return selectedApprover;
    }

}
