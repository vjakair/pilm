/*****************************************************************
 * Proprietary & Confidential  |  © 2017 PhaseZero Ventures LLC  *
 * This is part of PhaseZero Ventures LLC and cannot be copied,  *
 * modified and/or distributed without the express permission of *
 * PhaseZero Ventures LLC                                        *
 *****************************************************************/
/**
 * @author: Irshadahmed
 */
import {
    Component,
    EventEmitter,
    Input,
    Output
} from '@angular/core';
import {
    NavigationService
} from '../../core/navigation.service';
@Component({
    selector: 'top-navbar',
    template: `
    <ul class="nav nav-pills top-nav-bar">
        <li routerLinkActive="active" [routerLinkActiveOptions]="{exact: true}"
        [ngClass]="{'completed': 0 <= completedStage}">
            <a [routerLink]=" ['/promotions', promoId, 'define', templateId] "
            (click)="stepChange.emit(0)"
            translate="BUTTONS.DEF_PROMO"></a>
            <a translate="BUTTONS.DEF_PROMO"></a>
        </li>
        <li routerLinkActive="active" [routerLinkActiveOptions]="{exact: true}"
        [ngClass]="{'completed': 1 <= completedStage,
        'disabled': (1 > currentStepNo && 1 > completedStage)}">
            <a [routerLink]=" ['/promotions', promoId, 'goals', templateId] "
            (click)="stepChange.emit(1)"
            translate="BUTTONS.GOALS"></a>
            <a translate="BUTTONS.GOALS"></a>
        </li>
        <li routerLinkActive="active" [routerLinkActiveOptions]="{exact: false}"
        [ngClass]="{'completed': 2 <= completedStage,
        'disabled': (2 > currentStepNo && 2 > completedStage)}">
            <a [routerLink]=" ['/promotions', promoId, 'rules', templateId] "
            (click)="stepChange.emit(2)"
            translate="BUTTONS.CONFIG_RULES"></a>
            <a translate="BUTTONS.CONFIG_RULES"></a>
        </li>
        <li routerLinkActive="active" [routerLinkActiveOptions]="{exact: true}"
        [ngClass]="{'completed': 3 <= completedStage,
        'disabled': (3 > currentStepNo && 3 > completedStage)}">
            <a [routerLink]=" ['/promotions', promoId, 'target-market', templateId] "
            (click)="stepChange.emit(3)"
            translate="BUTTONS.TAR_MARKT"></a>
            <a translate="BUTTONS.TAR_MARKT"></a>
        </li>
        <li routerLinkActive="active" [routerLinkActiveOptions]="{exact: true}"
        [ngClass]="{'completed': 4 <= completedStage,
        'disabled': (4 > currentStepNo && 4 > completedStage)}">
            <a [routerLink]=" ['/promotions', promoId, 'approvals', templateId] "
            (click)="stepChange.emit(4)"
            translate="BUTTONS.APPROVAL_PROC"></a>
            <a translate="BUTTONS.APPROVAL_PROC"></a>
        </li>
    </ul>
    `,
    styleUrls: ['./topNavbar.style.scss']
})
export class TopNavbarComponent {
    public promoId: string;
    public templateId: string;
    @Input()
    public completedStage: number;
    @Input()
    public currentStepNo: number;
    @Output()
    public stepChange: EventEmitter<number> = new EventEmitter();
    constructor(
        _navigation: NavigationService
    ) {
        this.promoId = _navigation.getRouteParam('promoId');
        this.templateId = _navigation.getRouteParam('templateId');
    }
}
