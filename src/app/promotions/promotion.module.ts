/*****************************************************************
 * Proprietary & Confidential  |  © 2017 PhaseZero Ventures LLC  *
 * This is part of PhaseZero Ventures LLC and cannot be copied,  *
 * modified and/or distributed without the express permission of *
 * PhaseZero Ventures LLC                                        *
 *****************************************************************/
/**
 * @author: Irshadahmed
 */
import {
    CommonModule
} from '@angular/common';
import {
    FormsModule
} from '@angular/forms';
import {
    NgModule
} from '@angular/core';
import {
    RouterModule,
    PreloadAllModules
} from '@angular/router';
import {
    AccordionModule
} from 'ngx-bootstrap/accordion';
import {
    BsDatepickerModule
} from 'ngx-bootstrap/datepicker';
import {
    ModalModule
} from 'ngx-bootstrap/modal';
import {
    PaginationModule
} from 'ngx-bootstrap/pagination';
import {
    TooltipModule
} from 'ngx-bootstrap/tooltip';
import {
    TranslateModule
} from '@ngx-translate/core';
// Import Custom Components
import {
    ActionModalComponent
} from './action-modal/actionModal.component';
import {
    AddNoteModalComponent
} from './add-note-modal/addNoteModal.component';
import {
    AdminConsoleModalComponent
} from './admin-console-modal/adminConsoleModal.component';
import {
    ApproverListStatusComponent
} from './approver-list-status/approverListStatus.component';
import {
    ApprovalProcessComponent
} from './approval-process/approvalProcess.component';
import {
    AsidebarComponent
} from './asidebar/asidebar.component';
import {
    CategoryCardComponent
} from './category-card/categoryCard.component';
import {
    CategoryViewComponent
} from './category-view/categoryView.component';
import {
    ConfigureRulesComponent
} from './configure-rules/configureRules.component';
import {
    ChooseTemplateComponent
} from './choose-template/chooseTemplate.component';
import {
    ComponentsModule
} from '../components/components.module';
import {
    CoreModule
} from '../core/core.module';
import {
    CustomerFilterPipe
} from './customer-list-modal/customer.filter';
import {
    CustomerListModalComponent
} from './customer-list-modal/customerListModal.component';
import {
    DefinePromotionComponent
} from './define/definePromotion.component';
import {
    DisplayFieldDirective
} from './display-field/displayField.directive';
import {
    EmailPreviewModalComponent
} from './email-preview-modal/emailPreviewModal.component';
import {
    GoalsComponent
} from './goals/goals.component';
import {
    ListViewComponent
} from './list-view/listView.component';
import {
    NotificationComponent
} from './notification/notification.component';
import {
    PartListModalComponent
} from './part-list-modal/partListModal.component';
import {
    PendingTaskCardComponent
} from './promotion-task-card/promotionTaskCard.component';
import {
    PromotionComponent
} from './promotions.component';
import {
    PromotionDetailsComponent
} from './promotion-details/promotionDetails.component';
import {
    PromotionService
} from './promotions.service';
import {
    PromotionSnapCardComponent
} from './promotion-snap-card/promotionSnapCard.component';
import {
    PromotionStatusComponent
} from './promotion-status-details/promotionStatusDetails.component';
import {
    PromotionRecentActivityCardComponent
} from './promotion-recent-activity-card/promotionRecentActivityCard.component';
import {
    ROUTES
} from './promotions.routes';
import {
    TargetMarketComponent
} from './target-market/targetMarket.component';
import {
    TemplateCardComponent
} from './template-card/templateCard.component';
import {
    TopNavbarComponent
} from './top-navbar/topNavbar.component';
import {
    ViewCustListModalComponent
} from './view-cust-list-modal/viewCustListModal.component';
import {
    DownloadReportComponent
} from './reports/download-report.component';
import {
    CategorySelectComponent
} from './category-select/categorySelect.component';
import {
    BsDropdownModule
} from 'ngx-bootstrap/dropdown';
import {
    ExcludeItemModalComponent
} from './exclude-items-modal/excludeItemModal.component';
import {
    BrandViewComponent
} from './brand-view/brandView.component';
import {
    ExcBrandProdModalComponent
} from './exc-brand-prod-modal/excBrandProdModal.component';

@NgModule({
    declarations: [
        /**
         * Components / Directives/ Pipes
         */
        ActionModalComponent,
        AddNoteModalComponent,
        AdminConsoleModalComponent,
        ApproverListStatusComponent,
        ApprovalProcessComponent,
        AsidebarComponent,
        CategoryCardComponent,
        CategoryViewComponent,
        ConfigureRulesComponent,
        ChooseTemplateComponent,
        CustomerFilterPipe,
        CustomerListModalComponent,
        DefinePromotionComponent,
        DisplayFieldDirective,
        EmailPreviewModalComponent,
        ExcludeItemModalComponent,
        GoalsComponent,
        ListViewComponent,
        NotificationComponent,
        PartListModalComponent,
        PendingTaskCardComponent,
        PromotionComponent,
        PromotionDetailsComponent,
        PromotionSnapCardComponent,
        PromotionStatusComponent,
        PromotionRecentActivityCardComponent,
        TargetMarketComponent,
        TemplateCardComponent,
        TopNavbarComponent,
        ViewCustListModalComponent,
        DownloadReportComponent,
        CategorySelectComponent,
        BrandViewComponent,
        ExcBrandProdModalComponent
    ],
    entryComponents: [
        ActionModalComponent,
        AddNoteModalComponent,
        AdminConsoleModalComponent,
        CustomerListModalComponent,
        EmailPreviewModalComponent,
        ExcludeItemModalComponent,
        PartListModalComponent,
        ViewCustListModalComponent,
        ExcBrandProdModalComponent
    ],
    imports: [
        AccordionModule.forRoot(),
        BsDatepickerModule.forRoot(),
        BsDropdownModule.forRoot(),
        CommonModule,
        ComponentsModule,
        CoreModule,
        FormsModule,
        ModalModule.forRoot(),
        PaginationModule.forRoot(),
        RouterModule.forRoot(ROUTES, { useHash: false, preloadingStrategy: PreloadAllModules, enableTracing: true }),
        TooltipModule.forRoot(),
        TranslateModule
    ],
    providers: [
        PromotionService
    ],
    exports: [
        PendingTaskCardComponent,
        PromotionSnapCardComponent,
        PromotionRecentActivityCardComponent
    ]
})
export class PromotionModule {
}
