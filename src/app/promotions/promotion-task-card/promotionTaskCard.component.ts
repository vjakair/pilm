/*****************************************************************
 * Proprietary & Confidential  |  © 2017 PhaseZero Ventures LLC  *
 * This is part of PhaseZero Ventures LLC and cannot be copied,  *
 * modified and/or distributed without the express permission of *
 * PhaseZero Ventures LLC                                        *
 *****************************************************************/
/**
 * @author: Irshadahmed
 */
import {
    Component,
    EventEmitter,
    Input,
    Output
} from '@angular/core';
import {
    BsModalService
} from 'ngx-bootstrap/modal';
import {
    BsModalRef
} from 'ngx-bootstrap/modal/modal-options.class';
import {
    ActionModalComponent
} from '../action-modal/actionModal.component';
import {
    AuthService
} from '../../auth/auth.service';
import {
    PromotionService
} from '../promotions.service';
import {
    NavigationService
} from '../../core/navigation.service';
import {
    CommonUtilService
} from '../../core/commonUtil.service';
@Component({
    selector: 'promotion-task-card',
    templateUrl: './promotionTaskCard.component.html',
    styleUrls: ['./promotionTaskCard.style.scss']
})
export class PendingTaskCardComponent {
    public actionKey: string;
    public actionType: string; // approved/reject/rf
    @Input()
    public promoStatus: string;
    public reasonReqd: boolean;
    public reason: string;
    @Input()
    public cssClass: string;
    @Input()
    public applyBorder: boolean;
    @Input()
    public titleKey: string;
    public key: string = null;
    public actionModal: BsModalRef;
    @Input()
    public promotion: any;
    @Output()
    public actionComplete: EventEmitter<any> = new EventEmitter();
    constructor(
        private _authSvc: AuthService,
        private _promotionSvc: PromotionService,
        private _modalService: BsModalService,
        private _navigation: NavigationService,
        private _commonUtil: CommonUtilService,
    ) {
    }
    public approve() {
        this._showModal();
        this.actionModal.content.reasonReqd = false;
        this.actionModal.content.actionType = 'approved';
        this.actionModal.content.actionKey = 'COMMON.CONF_APPR';
        this.actionModal.content.promotion = this.promotion;
        this.focusTextarea();
    }
    public disableButton(permission: string): boolean {
        const actionable: boolean = this.promotion.actionable;
        return !actionable || !this._authSvc.userHasPermissions(permission);
    }
    public focusTextarea() {
        setTimeout(() => {
            document.getElementById('comments').focus();
        }, 50);
    }
    public getPromoImagePath(key: string): string {
        return this._promotionSvc.getPromoImagePath(key);
    }
    public reject() {
        this._showModal();
        this.actionModal.content.reasonReqd = true;
        this.actionModal.content.actionType = 'rejected';
        this.actionModal.content.actionKey = 'COMMON.REJ_APPR';
        this.actionModal.content.promotion = this.promotion;
        this.focusTextarea();
    }
    public rfi() {
        this._showModal();
        this.actionModal.content.reasonReqd = true;
        this.actionModal.content.actionType = 'rf';
        this.actionModal.content.actionKey = 'COMMON.REQ_INFO';
        this.actionModal.content.promotion = this.promotion;
        this.focusTextarea();
    }
    public edit() {
        let stage: string;
        const state = this.promoStatus;
        if (state === 'pending') {
            stage = 'APPROVAL_PROCESS';
        }
        const stgComplete: boolean = this.promotion.stgComplete;
        let stepNo: number = this._promotionSvc.getCurrentStepByStage(stage);
        if (4 > stepNo && stgComplete) {
            stepNo = stepNo + 1;
        }
        const stepSettings: any = this._promotionSvc.getStepSettingsByStepNo(stepNo);
        if (!this._commonUtil.isObject(stepSettings)) {
            return;
        }
        const promotionId =  this.promotion.promoId;
        const tplId: number = this.promotion.templateId;
        const routerLink: string = stepSettings.currentRouterLink;
        const navUrl: string = this._commonUtil.replaceUrlParams(routerLink, {
            promotionId,
            templateId: tplId
        });
        this._navigation.navigate(navUrl);
    }
    private _showModal(): void {
        this.actionModal = this._modalService.show(ActionModalComponent, {
            class: 'pilm-popup action-modal'
        });
        this.actionModal.content.actionComplete = this.actionComplete;
    }
}
