/*****************************************************************
 * Proprietary & Confidential  |  © 2017 PhaseZero Ventures LLC  *
 * This is part of PhaseZero Ventures LLC and cannot be copied,  *
 * modified and/or distributed without the express permission of *
 * PhaseZero Ventures LLC                                        *
 *****************************************************************/
/**
 * @author: Irshadahmed
 */
import {
    ChangeDetectorRef,
    Component,
    OnDestroy,
    OnInit,
    ViewChild
} from '@angular/core';
import {
    BsDatepickerComponent
} from 'ngx-bootstrap/datepicker';
import {
    NgForm
} from '@angular/forms';
import {
    CommonUtilService
} from '../../core/commonUtil.service';
import {
    DateUtilService
} from '../../core/dateUtil.service';
import {
    EventEmitterService
} from '../../core/event-emitter';
import {
    PromotionService
} from '../promotions.service';
import {
    SubscriptionHistory
} from '../../helper/subscriptionHistory.helper';
import {
    TitleService
} from '../../core/title';
import { retry } from 'rxjs/operator/retry';
@Component({
    selector: 'define-promotion',
    templateUrl: './definePromotion.component.html',
    styleUrls: ['./definePromotion.style.scss']
})
export class DefinePromotionComponent extends SubscriptionHistory implements OnDestroy, OnInit {
    public dpPlacement: string = 'top';
    public formValidated: boolean = false;
    public isPromoLoaded: boolean = false;
    public isTemplateReady: boolean = false;
    public promoDetails: any = {
        notifyTime: {
            hours: '12',
            min: '00',
            ampm: 'AM'
        },
        tempMeta: {
            defaultPromo: true,
            disableOtherDt: true
        }
    };
    public promoCdErrKey: string = 'EXIST';
    @ViewChild('promoCode')
    public promoCodeInp: any;
    @ViewChild('promoFrm')
    public promoFrm: NgForm;
    public promoValStatus: string;
    @ViewChild('promoCdtooltip')
    public promoCdTooltip: any;
    public startDtConfig: any = {
        minDate: this._dateUtilSvc.getToday(),
        isDisabled: false,
        name: 'startDate',
        id: 'sDp',
        dateInvalid: true,
        bsConfig: {
            containerClass: 'theme-default',
            dateInputFormat: 'M/D/YYYY',
            placement: 'top',
            showWeekNumbers: false
        }
    };
    public endDtConfig: any = {
        minDate: this._dateUtilSvc.getToday(),
        manualChange: false,
        dateInvalid: true,
        bsConfig: {
            containerClass: 'theme-default',
            dateInputFormat: 'M/D/YYYY',
            placement: 'top',
            showWeekNumbers: false
        }
    };
    public notificationDtConfig: any = {
        minDate: this._dateUtilSvc.getToday(),
        manualChange: false,
        dateInvalid: true,
        bsConfig: {
            containerClass: 'theme-default',
            dateInputFormat: 'M/D/YYYY',
            placement: 'top',
            showWeekNumbers: false
        }
    };
    public uploadConfig: any = {
        headerImgOpt: {
            type: 'header',
            label: 'DEFINE_PROMO.HEADER_IMG',
            s: 1, // Image Size in MB
            w: '1440px', // Image width
            h: '160px' // Image height
        },
        bannerImgOpt: {
            type: 'banner',
            label: 'DEFINE_PROMO.BANNER_IMG',
            s: 1,
            w: '210px',
            h: '409px'
        },
        adImgOpt: {
            type: 'ad',
            label: 'DEFINE_PROMO.AD_BANNER_IMG',
            s: 1,
            w: '728px',
            h: '90px'
        },
        headerImgOptMob: {
            type: 'header_mob',
            label: 'DEFINE_PROMO.HEADER_IMG_MOB',
            s: 1, // Image Size in MB
            w: '400px', // Image width
            h: '60px' // Image height
        },
        bannerImgOptMob: {
            type: 'banner_mob',
            label: 'DEFINE_PROMO.BANNER_IMG_MOB',
            s: 1, // Image Size in MB
            w: '360px', // Image width
            h: '80px' // Image height
        },
        adImgOptMob: {
            type: 'ad_mob',
            label: 'DEFINE_PROMO.AD_BANNER_IMG_MOB',
            s: 1, // Image Size in MB
            w: '360px', // Image width
            h: '80px' // Image height
        },
    };
    private _promoCode: string;
    private _uploadedFiles: any = {};
    private _isFileUploading: boolean = false;
    constructor(
        private _ref: ChangeDetectorRef,
        private _commonUtil: CommonUtilService,
        private _dateUtilSvc: DateUtilService,
        private _eventEmitter: EventEmitterService,
        titleSvc: TitleService,
        private _promoSvc: PromotionService
    ) {
        super();
        titleSvc.setTitle('DEFINE_PROMO');
    }
    public ngOnInit() {
        this._subscribeEvents();
    }
    public ngOnDestroy() {
        this.unsubscribe();
    }
    public hasFormField(fieldName: string) {
        return super._hasFormField(fieldName);
    }
    public hasEditableField(fieldName: string) {
        return super._hasEditableField(fieldName);
    }
    public cancelFileUpload(label: string): void {
        delete this._uploadedFiles[label];
    }
    public invalidateOtherDates() {
        this.promoDetails.endDate = undefined;
        this.startDtConfig.dateInvalid = true;
        this.endDtConfig.dateInvalid = true;
        this.notificationDtConfig.dateInvalid = true;
        this.promoDetails.tempMeta.disableOtherDt = true;
    }
    public isPromoCodeValid() {
        if (!this.formValidated) {
            return true;
        }
        return this._isValidPromoCode();
    }
    public onScroll($event: any): void {
        if (false === $event instanceof Event) {
            return;
        }
        const scrollTop: number = $event.target && $event.target.scrollTop;
        let dpPlacement: string;
        if (162 >= scrollTop) {
            if ('bottom' === this.dpPlacement) {
                dpPlacement = 'top';
            }
        } else if ('top' === this.dpPlacement) {
            dpPlacement = 'bottom';
        }
        if (dpPlacement && dpPlacement !== this.dpPlacement) {
            this.dpPlacement = dpPlacement;
            this._eventEmitter.onDatePickerOrientationChange.emit(dpPlacement);
        }
    }
    public onStartDateChange(date: string | boolean): void {
        if (!date) {
            this.invalidateOtherDates();
            return;
        }

        // Set minimum date for end date
        const nextDayDate: Date = this._dateUtilSvc.getNextDayDate(date);
        this.endDtConfig.minDate = nextDayDate;
        if (
            !this.promoDetails.endDate ||
            !this.endDtConfig.manualChange
        ) {
            this.endDtConfig.autoSelected = true;
            this.endDtConfig.bsConfig.containerClass = 'theme-default auto-selected';
            setTimeout(() => {
                this.endDtConfig.autoSelected = false;
            }, 50);
            this.promoDetails.endDate = nextDayDate;
        }
        if (this.promoDetails.endDate && this.promoDetails.endDate < nextDayDate) {
            this.endDtConfig.dateInvalid = true;
            this.formValidated = true;
        } else {
            this.endDtConfig.dateInvalid = false;
        }

        if (!this.notificationDtConfig.manualChange) {
            this.notificationDtConfig.bsConfig.containerClass = 'theme-default auto-selected';
            this.notificationDtConfig.autoSelected = true;
            setTimeout(() => {
                this.notificationDtConfig.autoSelected = false;
            }, 50);
        }

        if (this.promoDetails.notifyDt && this.promoDetails.notifyDt > date) {
            this.notificationDtConfig.dateInvalid = true;
            this.formValidated = true;
        } else {
            this.notificationDtConfig.dateInvalid = false;
        }
        this.startDtConfig.manualChange = true;
        this.startDtConfig.dateInvalid = false;
        this.promoDetails.startDate = date;
        this.promoDetails.tempMeta.disableOtherDt = false;
        this._ref.detectChanges();
    }
    public onEndDateChange(date: string | boolean): void {
        if (!date || this.endDtConfig.autoSelected) {
            this.endDtConfig.dateInvalid = true;
            return;
        }
        this.endDtConfig.dateInvalid = false;
        this.promoDetails.endDate = date;
        this.endDtConfig.manualChange = true;
        this._ref.detectChanges();
    }
    public onNotifyDateChange(date: string): void {
        if (!date || this.notificationDtConfig.autoSelected) {
            this.notificationDtConfig.dateInvalid = true;
            return;
        }
        this.notificationDtConfig.dateInvalid = false;
        this.promoDetails.notifyDt = date;
        this.notificationDtConfig.manualChange = true;
        this._ref.detectChanges();
    }
    public togglePromoCodeValSpinner(): boolean {
        return this._commonUtil.isValidString(this.promoDetails.promoCode.customCode);
    }
    public uploadFile(file: any): void {
        const promoDraft: any = this._promoSvc.promotionDraft;
        this._isFileUploading = true;
        this._toggleActionBar();
        this._promoSvc
            .uploadFile(file.data, promoDraft.promoId)
            .subscribe((response) => {
                if (this._commonUtil.isValidString(response.fileName)) {
                    this._uploadedFiles[file.label] = response;
                } else {
                    this._eventEmitter
                        .onServerError
                        .emit({
                            fileType: file.type
                        });
                }
                this._isFileUploading = false;
                this._toggleActionBar();
            }, (error) => {
                this._isFileUploading = false;
                this._toggleActionBar();
                console.log('Could not upload file', error);
            });
    }
    public validatePromoCode(): void {
        const customCode: string = this.promoDetails.promoCode.customCode;
        if (this._promoCode === customCode) {
            return;
        }
        if (!this._isValidPromoCode()) {
            this.promoValStatus = 'invalid';
            return;
        }
        const promoCode: string = this.promoDetails.promoCode.customCode;
        this.promoValStatus = 'inProgress';
        this._eventEmitter.onLoaderDisabled.emit(true);
        this._toggleActionBar();
        this._commonUtil.delayCallback('promoCdVal', () => {
            this._promoSvc.validatePromoCode(
                promoCode
            ).subscribe((response: any): void => {
                if (!this._isValidPromoCode() || 'error' === response.type) {
                    this.promoValStatus = 'invalid';
                    this._toggleActionBar();
                    return;
                }
                const statusCode: number = response.status;
                if (409 === statusCode) {
                    this.promoCdErrKey = 'EXIST';
                    this.promoValStatus = 'invalid';
                    this._commonUtil.showTooltip(this.promoCdTooltip);

                } else {
                    this.promoValStatus = 'complete';
                }
                this._toggleActionBar();
            });
        }, 3000);
    }
    public removeWhiteSpace(event: Event): void {
        let promo = this.promoDetails.promoCode.customCode;
        promo = (!promo) ? '' : promo.replace(/ /g, ''); // promo.replace(/[^a-zA-Z0-9]/g, '');
        this.promoDetails.promoCode.customCode = promo;
    }
    private _toggleActionBar(): void {
        this._eventEmitter.onToggleActions.emit(
            'inProgress' === this.promoValStatus || this._isFileUploading
        );
    }
    private _getUploadedFiles(): any[] {
        const media: any[] = [];
        const uploadedFiles: any = this._uploadedFiles;
        for (const key in uploadedFiles) {
            if (!uploadedFiles.hasOwnProperty(key)) {
                continue;
            }
            media.push({
                label: key,
                files: [
                    uploadedFiles[key]
                ]
            });
        }
        return media;
    }
    private _isValidPromoCode(): boolean {
        if (this.promoDetails.promoCode.isDefault) {
            return true;
        }
        const promoCode: string = this.promoDetails.promoCode.customCode;
        return this._commonUtil.isValidString(promoCode) && 6 <= promoCode.length;
    }
    private _setPromoDetails(): void {
        this._promoSvc.currentStep = 0;
        const promoDetails = this._promoSvc.getPromoDetails();
        if (!promoDetails) {
            this.promoDetails.promoCode = {
                isDefault: true
            };
            return;
        }
        for (const key in promoDetails) {
            if (!promoDetails.hasOwnProperty(key)) {
                continue;
            }
            const value: any = promoDetails[key];
            if (this._commonUtil.isUnDefined(value)) {
                continue;
            }
            if (-1 === ['endDate', 'startDate', 'notificationTimestamp'].indexOf(key)) {
                if ('promoCode' === key) {
                    const promoCode: any = promoDetails[key];
                    this._promoCode = promoCode.customCode;
                    this.promoDetails[key] = promoCode;
                } else if ('media' === key) {
                    this._setUploadedImages(value);
                } else {
                    this.promoDetails[key] = promoDetails[key];
                }
            } else {
                const d = new Date(value);
                if ('notificationTimestamp' === key) {
                    this.promoDetails['notifyTime'] =
                        this._dateUtilSvc.getMeridianTime(value);
                    this.promoDetails['notifyDt'] = d;
                    this.endDtConfig.manualChange = true;
                    this.notificationDtConfig.manualChange = true;
                    this.startDtConfig.manualChange = true;
                } else {
                    if (key === 'endDate') {
                        const endDate = this._dateUtilSvc.convertUTCDateToLocalDate(promoDetails[key]);
                        this.promoDetails[key] = new Date(endDate);
                    } else {
                        if (this._promoSvc.isPromoConfigEditable()) {
                            this.startDtConfig.minDate = d;
                        }
                        this.promoDetails[key] = d;
                    }
                }
            }
        }
        this._ref.detectChanges();
    }
    private _setUploadedImages(mediaItems: any[]): void {
        if (!this._commonUtil.isValidArray(mediaItems)) {
            return;
        }
        try {
            mediaItems.forEach((mediaItem: any) => {
                if (!this._commonUtil.isObject(mediaItem)) {
                    return;
                }
                const file: any = mediaItem.files[0];
                const label: string = mediaItem.label;
                if (!this._commonUtil.isObject(file)
                    || !this._commonUtil.isValidString(label)) {
                    return;
                }
                this._uploadedFiles[label] = file;
            });
        } catch (e) {
            console.log('Unable to fetch media items!', e);
        }
    }
    private _subscribeEvents() {
        // Set tempalte ready flag
        const _self = this;
        this.subscriptions.push(
            this._promoSvc
                .onTemplateReady
                .subscribe((isTemplateReady: boolean) => {
                    if (!isTemplateReady) {
                        return;
                    }
                    const templateFields: any[] = this._promoSvc.getTemplateFields('headers');
                    this.setFormFields(templateFields);
                    _self.isTemplateReady = isTemplateReady;
                })
        );
        this.subscriptions.push(
            this._promoSvc
                .onPromotionLoaded
                .subscribe((isPromoLoaded: boolean) => {
                    if (!isPromoLoaded) {
                        return;
                    }
                    this.setEditableFormFields(this._promoSvc.getEditableFields('headers'),
                        this._promoSvc.isPromoConfigEditable());
                    _self._setPromoDetails();
                    this.isPromoLoaded = true;
                })
        );
        // Validate promotion form if save button is clicked
        this.subscriptions.push(
            this._eventEmitter
                .onValidatePromoDetails
                .subscribe(() => _self._validatePromoDetails())
        );
    }
    private _validatePromoDetails(): void {
        let valid: boolean = this.promoFrm.valid;
        if (valid) { // Validate other fields which doesn't come under the scope of promo form
            valid = this._validateOtherFields();
            if (
                this.promoDetails.promoCode.isDefault
            ) {
                this.promoDetails.promoCode.customCode = undefined;
            } else if (
                'invalid' === this.promoValStatus
            ) {
                this._commonUtil.showTooltip(this.promoCdTooltip);
            }
        }
        if (!valid) {
            this.formValidated = true;
            this._commonUtil.setFormDirty(this.promoFrm.controls);
            return;
        }
        const headers: any = {};
        const promoDetails = this.promoDetails;
        for (const key in promoDetails) {
            if (!promoDetails.hasOwnProperty(key) || 'notifyTime' === key
                || 'tempMeta' === key) {
                continue;
            }
            const value: any = promoDetails[key];
            if (-1 === ['endDate', 'startDate', 'notifyDt'].indexOf(key)) {
                headers[key] = value;
            } else {
                const d: Date = value;
                if (!this._dateUtilSvc.isDateValid(d)) {
                    continue;
                }
                if ('notifyDt' === key) {
                    const selectedTime = this.promoDetails.notifyTime;
                    let hours: number = parseInt(selectedTime.hours, 0);
                    const min: number = parseInt(selectedTime.min, 0);
                    if ('PM' === selectedTime.ampm && 12 > hours) {
                        hours += 12;
                    } else if ('AM' === selectedTime.ampm && 12 === hours) {
                        hours = 0;
                    }
                    d.setHours(hours, min, 0, 0);
                    headers.notificationTimestamp = d.getTime();
                } else {
                    if (key === 'endDate') {
                        // console.log('Before process =>>>(%o) and decode => (%o)', this._commonUtil.deepCopy(d), new Date(this._commonUtil.deepCopy(d)));
                        headers[key] = this._dateUtilSvc.convertWithTimeFormat(this._commonUtil.deepCopy(d), 'to');

                        // console.log('Timestamp =>>>(%o) and decode => (%o)', this._commonUtil.deepCopy(headers[key]), new Date(this._commonUtil.deepCopy(headers[key])));
                        const endDate = this._dateUtilSvc.convertUTCDateToLocalDate(this._commonUtil.deepCopy(headers[key]));
                        // console.log('Process after  =>>>(%o) and after => (%o)', this._commonUtil.deepCopy(endDate), new Date(this._commonUtil.deepCopy(endDate)));

                    } else {
                        headers[key] = d.getTime();
                    }
                }
            }
        }
        headers['media'] = this._getUploadedFiles();
        this._promoSvc.updateLocalPromoDraft(headers);
        this._eventEmitter
            .onPromoSave
            .emit(true);
    }
    private _isDateValid(dtConfig: any) {
        if (!this._commonUtil.isObject(dtConfig)) {
            dtConfig = {};
        }
        if (!dtConfig.manualChange) {
            dtConfig.dateInvalid = true;
        }
        return !dtConfig.dateInvalid;
    }
    private _validateOtherFields(): boolean {
        const stratDtValid: boolean = this._isDateValid(this.startDtConfig);
        const endDtValid: boolean = this._isDateValid(this.endDtConfig);
        const notifyDtValid: boolean = this._isDateValid(this.notificationDtConfig);
        return (
            this.promoDetails.promoCode.isDefault ||
            ('invalid' !== this.promoValStatus && this._isValidPromoCode())
        )
            && !this._isFileUploading
            && stratDtValid
            && endDtValid
            && notifyDtValid;
    }
}
