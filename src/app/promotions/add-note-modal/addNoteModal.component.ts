/*****************************************************************
 * Proprietary & Confidential  |  © 2017 PhaseZero Ventures LLC  *
 * This is part of PhaseZero Ventures LLC and cannot be copied,  *
 * modified and/or distributed without the express permission of *
 * PhaseZero Ventures LLC                                        *
 *****************************************************************/
/**
 * @author: Irshadahmed
 */
import {
    Component,
    EventEmitter,
    Input,
    OnDestroy,
    ViewChild
} from '@angular/core';
import {
    NgForm
} from '@angular/forms';
import {
    BsModalRef
} from 'ngx-bootstrap/modal/modal-options.class';
import {
    CommonUtilService
} from '../../core/commonUtil.service';
import {
    EventEmitterService
} from '../../core/event-emitter';
import {
    SubscriptionHistory
} from '../../helper/subscriptionHistory.helper';
import {
    PromotionService
} from '../promotions.service';
@Component({
    selector: 'add-note',
    template: `
    <div class="modal-dialog">
    <form class="pilm-form" #actionFrm="ngForm" role="form">
    <div class="modal-header" style="border-bottom: none;">
    <div class="action-img add-note"></div>
    <h4 class="modal-title" translate="PROMOTION_DETAILS.ADD_NOTE"></h4>
</div>
<div class="modal-body">
    <div class="reason" style="margin-top: 0;"
    [ngClass]="{'invalid': comments.invalid && (comments.dirty || comments.touched)}">
        <textarea class="form-control pilm-form-control"
        name="comments" id="comments" maxlength="250"
        autocomplete="off"
        autofocus="true"
        [(ngModel)]="promoNote" #comments="ngModel" required></textarea>
        <div class="char-count"><span>{{promoNote ? promoNote.length : 0}}/250</span></div>
    </div>
    <div class="actions">
        <button type="button" (click)="closePopover()"
        class="btn btn-default" translate="BUTTONS.CANC"></button>
        <button type="submit" (click)="saveNote()"
        class="btn btn-primary" translate="BUTTONS.ADD"></button>
    </div>
</div>
</form>
</div>
    `
})
export class AddNoteModalComponent extends SubscriptionHistory implements OnDestroy {
    public promoId: string;
    public promoNote: string;
    @ViewChild('actionFrm')
    public actionFrm: NgForm;
    public onNoteAdded: EventEmitter<any> = new EventEmitter();
    constructor(
        public bsModalRef: BsModalRef,
        private _commonUtil: CommonUtilService,
        private _eventEmitter: EventEmitterService,
        private _promotionSvc: PromotionService
    ) {
        super();
        this.subscriptions.push(
            _eventEmitter.onNavigationSuccess.subscribe(
                () => this.closePopover()
            )
        );
    }
    public ngOnDestroy() {
        this.unsubscribe();
    }
    public closePopover() {
        this.bsModalRef.hide();
    }
    public saveNote() {
        if (this.actionFrm.invalid) {
            this._commonUtil.setFormDirty(this.actionFrm.controls);
            return;
        }
        this._promotionSvc
            .savePromoNote(
            this.promoId,
            this.promoNote
            ).subscribe((response: any) => {
                if (response.errorCode) {
                    this._eventEmitter
                        .onAlert
                        .emit({
                            type: 'alert-warning',
                            msgKey: 'PROMOTION.ERROR.SAVE_NOTE'
                        });
                    return;
                }
                this.onNoteAdded.emit(response);
                this.closePopover();
                this._eventEmitter
                    .onAlert
                    .emit({
                        type: 'alert-success',
                        msgKey: 'PROMOTION.SUCCESS.NOTE_ADDED'
                    });
            });
    }
}
