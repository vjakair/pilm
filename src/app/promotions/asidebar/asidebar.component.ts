/*****************************************************************
 * Proprietary & Confidential  |  © 2017 PhaseZero Ventures LLC  *
 * This is part of PhaseZero Ventures LLC and cannot be copied,  *
 * modified and/or distributed without the express permission of *
 * PhaseZero Ventures LLC                                        *
 *****************************************************************/
/**
 * @author: Irshadahmed
 */
import {
    Component,
    Input
} from '@angular/core';
@Component({
    selector: 'asidebar',
    template: `
        <h5 class="label" translate="COMMON.PROMO_TYPE"></h5>
        <p>{{promoDraft.promoType}}</p>
        <h5 class="label" translate="COMMON.PROMO_DOC_NO"></h5>
        <p>{{promoDraft.promoDocNumber}}</p>
    `,
    styles: [
        `
        h5 {
            color: #6e6e6e;
            font-size: 12px;
            margin-bottom: 5px;
            text-align: left;
            padding: 10px 1px;
            display: block;
            margin: 0;
            text-transform: capitalize;
        }
        p {
            background-color: #e8e8e8;
            border: 1px solid #dbdbdb;
            border-radius: 3px;
            font-size: 13px;
            font-weight: 600;
            line-height: 32px;
            text-align: left;
            padding-left: 10px;
            color: black;
            margin-bottom: 15px;
            height: 34px;
        }
        `
    ]
})
export class AsidebarComponent {
    @Input()
    public promoDraft: any;
}
