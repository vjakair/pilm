/*****************************************************************
 * Proprietary & Confidential  |  © 2017 PhaseZero Ventures LLC  *
 * This is part of PhaseZero Ventures LLC and cannot be copied,  *
 * modified and/or distributed without the express permission of *
 * PhaseZero Ventures LLC                                        *
 *****************************************************************/
/**
 * @author: Irshadahmed
 */
import {
    Component,
    ChangeDetectorRef,
    OnDestroy,
    OnInit,
    ViewChild
} from '@angular/core';
import {
    NgForm
} from '@angular/forms';
import {
    CommonUtilService
} from '../../core/commonUtil.service';
import {
    EventEmitterService
} from '../../core/event-emitter';
import {
    PromotionService
} from '../promotions.service';
import {
    SubscriptionHistory
} from '../../helper/subscriptionHistory.helper';
import {
    TitleService
} from '../../core/title';
import { AppConfig } from '../../config/app.config';
@Component({
    selector: 'goals',
    templateUrl: './approvalProcess.component.html',
    styleUrls: ['./approvalProcess.style.scss']
})
export class ApprovalProcessComponent extends SubscriptionHistory implements OnInit, OnDestroy {
    public isTemplateReady: boolean;
    public imageBaseUrl: string;
    public promoApprovers: any[];
    public promoDetails: any = {
        approvalRequired: true,
        fyiUsers: []
    };
    @ViewChild('promoFrm')
    public promoFrm: NgForm;
    constructor(
        private _commonUtil: CommonUtilService,
        private _eventEmitter: EventEmitterService,
        private _promoSvc: PromotionService,
        titleSvc: TitleService,
        private _ref: ChangeDetectorRef,
        private _appConfig: AppConfig
    ) {
        super();
        titleSvc.setTitle('APPROVAL_PROCESS');
        this._serApproverGroups();
    }
    public ngOnInit() {
        this.imageBaseUrl = `${this._appConfig.getProtocol()}//${this._appConfig.getCdnBaseUrl()}/assets/promotion/workflow-sequence.png`;
        this._subscribeEvents();
    }
    public ngOnDestroy() {
        this.unsubscribe();
    }
    public hasFormField(fieldName: string) {
        return this._hasFormField(fieldName);
    }
    public addEmail() {
        this.promoDetails.fyiUsers.push(
            this.promoDetails.email
        );
        this.promoFrm.resetForm();
        this._eventEmitter.onPageScroll.emit(true);
    }
    public removeEmail(index: any) {
        if (this._commonUtil.isUnDefined(index) && index !==0) {
            return;
        }
        this.promoDetails.fyiUsers.splice(index, 1);
    }
    public resetForm() {
        if (!this._commonUtil.isValidString(this.promoDetails.email)) {
            this.promoFrm.resetForm();
        }
    }
    private _serApproverGroups(): void {
        this._promoSvc
            .getApproverGroupData()
            .subscribe((approverGroups: any[]): void => {
                const promoApprovers: any[] = this._promoSvc
                    .getSelectedApprovers();
                if (this._commonUtil.isValidArray(promoApprovers)) {
                    this.promoApprovers = promoApprovers;
                } else {
                    this.promoApprovers = undefined;
                }
            }, (error: any) => {
                console.log(error);
                this._eventEmitter
                    .onAlert
                    .emit({
                        type: 'alert-warning',
                        msgKey: 'PROMOTION.ERROR.SERVER'
                    });
            });
    }
    private _setPromoDetails(): void {
        this._promoSvc.currentStep = 4;
        const promoDetails = this._promoSvc.getPromoDetails();
        if (!this._commonUtil.isObject(promoDetails)) {
            return;
        }
        const approverGroups: any[] = promoDetails.promoApproverGroups;
        const fyiUsers: any[] = promoDetails.fyiUsers;
        if (this._commonUtil.isValidArray(approverGroups)) {
            this.promoDetails.promoApproverGroups = approverGroups;
        }
        if (this._commonUtil.isValidArray(fyiUsers)) {
            this.promoDetails.fyiUsers = fyiUsers;
        }
        this._ref.detectChanges();
    }
    private _subscribeEvents(): void {
        // Set tempalte ready flag
        this.subscriptions.push(
            this._promoSvc
                .onTemplateReady
                .subscribe((isTemplateReady: boolean) => {
                    if (!isTemplateReady) {
                        return;
                    }
                    const templateFields: any[] = this._promoSvc.getTemplateFields('approvalData');
                    this.setFormFields(templateFields);
                    this.isTemplateReady = isTemplateReady;
                })
        );
        this.subscriptions.push(
            this._promoSvc
                .onPromotionLoaded
                .subscribe((isPromoLoaded: boolean) => {
                    if (isPromoLoaded) {
                        this._setPromoDetails();
                    }
                })
        );
        // Validate promotion form if save button is clicked
        this.subscriptions.push(
            this._eventEmitter
                .onValidatePromoDetails
                .subscribe(() => this._validatePromoDetails())
        );

        this.subscriptions.push(
            this._eventEmitter
                .onApproverGroupChange
                .subscribe(() => this._serApproverGroups())
        );
    }

    private _validatePromoDetails(): void {
        this._promoSvc.updateLocalPromoDraft(this.promoDetails);
        this._eventEmitter
            .onPromoSave
            .emit(true);
    }
}
