/*****************************************************************
 * Proprietary & Confidential  |  © 2017 PhaseZero Ventures LLC  *
 * This is part of PhaseZero Ventures LLC and cannot be copied,  *
 * modified and/or distributed without the express permission of *
 * PhaseZero Ventures LLC                                        *
 *****************************************************************/
/**
 * @author: Irshadahmed
 */
import {
    Component,
    Input,
    OnInit
} from '@angular/core';
import {
    CommonUtilService
} from '../../core/commonUtil.service';
import {
    PromotionSettings
} from '../../core/promotionSettings.provider';
@Component({
    selector: 'promotion-recent-activity-card',
    templateUrl: './promotionRecentActivityCard.component.html',
    styleUrls: ['./promotionRecentActivityCard.style.scss']
})
export class PromotionRecentActivityCardComponent implements OnInit {
    @Input()
    public recentActivity: any;
    @Input()
    public statusType: string;
    public promoStage: number;
    constructor(
        private _commonUtil: CommonUtilService,
        public promotionSettings: PromotionSettings
    ) { }
    public ngOnInit() {
        if (
            !this._commonUtil.isObject(this.recentActivity) ||
            !this._commonUtil.isObject(this.recentActivity.data)) {
            return;
        }
        this.promoStage = this.promotionSettings.PROMOTION_STAGES.indexOf(
            this.recentActivity.data.promoStage
        );
    }
}
