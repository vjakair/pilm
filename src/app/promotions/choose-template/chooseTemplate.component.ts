/*****************************************************************
 * Proprietary & Confidential  |  © 2017 PhaseZero Ventures LLC  *
 * This is part of PhaseZero Ventures LLC and cannot be copied,  *
 * modified and/or distributed without the express permission of *
 * PhaseZero Ventures LLC                                        *
 *****************************************************************/
/**
 * @author: Irshadahmed
 */
import {
    Component,
    Output,
    OnDestroy
} from '@angular/core';
import {
    CommonUtilService
} from '../../core/commonUtil.service';
import {
    NavigationService
} from '../../core/navigation.service';
import {
    PromotionService
} from '../promotions.service';
import {
    TitleService
} from '../../core/title';
import {
    TranslationService
} from '../../core/translation.service';
@Component({
    selector: 'choose-template',
    templateUrl: './chooseTemplate.component.html',
    styleUrls: ['./chooseTemplate.style.scss']
})
export class ChooseTemplateComponent implements OnDestroy {
    public disabled: boolean = true;
    public stepSettings: any;
    public templates: any[];
    private _selectedTemplate: any;
    private _subscriptions: any[] = [];
    constructor(
        private _commonUtilSvc: CommonUtilService,
        private _navigation: NavigationService,
        private _promotionSvc: PromotionService,
        _titleSvc: TitleService,
        private _translationSvc: TranslationService
    ) {
        _titleSvc.setTitle('CHOOSE_TEMPLATE');
        this.stepSettings = _promotionSvc.getStepSettingsByStepId('CHOOSE_TEMPLATE');
        this._setPromoTemplates();
    }
    public ngOnDestroy() {
        this.selectTemplate(this._selectedTemplate);
    }
    public createPromotion(button: any): void {
        this._translationSvc
            .getTranslation(this._selectedTemplate.templateName)
            .subscribe((promotionType: string) => this._doCreatePromotion(promotionType));
    }
    public selectTemplate(selectedTemplate) {
        if (!this._commonUtilSvc.isObject(selectedTemplate)) {
            return;
        }
        this._toggleTemplateSelection(selectedTemplate);
        this._commonUtilSvc.updateSaveNextBtn(
            this.stepSettings.BUTTONS,
            !selectedTemplate.selected
        );
        this._selectedTemplate = selectedTemplate;
    }
    private _doCreatePromotion(promotionType: string) {
        const templateId: string = this._selectedTemplate.templateId;
        // Create promotion draft
        this._promotionSvc
            .createPromotionDraft(promotionType, templateId)
            .subscribe((response: any) => {
                this._promotionSvc.promotionDraft = response;
                this._navigation
                    .navigate(
                    ['promotions', response.promoId, 'define', templateId]
                    );
            }/* , (error) => {
                const promotionId: string = this._commonUtilSvc.getUniqueId();
                this._promotionSvc.promotionDraft = {
                    promoId: promotionId,
                    promoType: promotionType,
                    promoDocNo: 'PILMSEP192017',
                    promoCode: 'PDN SDS 062217'
                };
                this._navigation
                    .navigate(
                    ['promotions', promotionId, 'define', this._selectedTemplate.templateId]
                    );
            } */);
    }
    private _setPromoTemplates() {
        this._promotionSvc
            .getPromoTemplates(null, true)
            .subscribe((templates: any[]) => {
                this._promotionSvc.setPromoTemplatesType(templates);
                this.templates = templates;
            });
    }
    private _toggleTemplateSelection(selectedTemplate: any) {
        this.templates.forEach((template: any) => {
            if (selectedTemplate.templateName === template.templateName) {
                selectedTemplate.selected = !selectedTemplate.selected;
            } else {
                template.selected = false;
            }
        });
    }
}
