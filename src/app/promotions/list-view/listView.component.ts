/*****************************************************************
 * Proprietary & Confidential  |  © 2017 PhaseZero Ventures LLC  *
 * This is part of PhaseZero Ventures LLC and cannot be copied,  *
 * modified and/or distributed without the express permission of *
 * PhaseZero Ventures LLC                                        *
 *****************************************************************/
/**
 * @author: Irshadahmed
 */
import {
    Component,
    OnInit,
    OnDestroy
} from '@angular/core';
import {
    CommonUtilService
} from '../../core/commonUtil.service';
import {
    PromotionService
} from '../promotions.service';
import {
    SubscriptionHistory
} from '../../helper/subscriptionHistory.helper';
import {
    EventEmitterService
} from '../../core/event-emitter';

@Component({
    selector: 'list-view',
    templateUrl: './listView.component.html',
    styleUrls: ['./listView.style.scss']
})
export class ListViewComponent extends SubscriptionHistory implements OnInit, OnDestroy {
    public isTemplateReady: boolean = false;
    public prodList: any[] = [];
    public promoDetails: any;
    private _isPromoLoaded: boolean;
    constructor(
        private _commonUtil: CommonUtilService,
        private _promoSvc: PromotionService,
        private _eventEmitter: EventEmitterService
    ) {
        super();
        this._promoSvc.currentStep = 2;
        this.prodList = [];
        this.setProdList(true);
    }
    public hasFormField(fieldName: string) {
        return this._hasFormField(fieldName);
    }
    public hasEditableField(fieldName: string) {
        return super._hasEditableField(fieldName);
    }
    public ngOnInit() {
        this._subscribeEvents();
    }
    public ngOnDestroy() {
        this.unsubscribe();
    }
    public selectList(selectedList: any): void {
        if (selectedList.cartLineCount === 0) {
            this._eventEmitter
                .onAlert
                .emit({
                    type: 'alert-warning',
                    msgKey: 'CONFIGURE_RULES.ALERTS.LIST_BLANK'
                });
            return ;
        }
        this.prodList
            .forEach((list: any) => {
                if (list.id === selectedList.id) {
                    selectedList.selected = true;
                } else {
                    list.selected = false;
                }
            });
        this._promoSvc
            .setTargetProdList('byList', selectedList.selected ? [selectedList.id] : []);
    }
    public setProdList(cache: boolean): void {
        this._promoSvc
            .getOrderList(cache)
            .subscribe((prodList: any[]) => {
                if (!this._commonUtil.isValidArray(prodList)) {
                    return;
                }
                this.prodList = prodList;
                this._setSelectedList();
            });
    }
    public updateList() {
        this.setProdList(false);
    }
    private _doSetSelectedList(): boolean {
        const selectedListIds: any[] = this._promoSvc.getTargetProdList('byList');
        if (!this._commonUtil.isValidArray(selectedListIds)) {
            return false;
        }
        this.prodList.forEach((list: any) => {
            if (-1 !== selectedListIds.indexOf(list.id)) {
                list.selected = true;
            }
        });
        return true;
    }
    private _setSelectedList() {
        if (
            !this._isPromoLoaded ||
            !this._commonUtil.isValidArray(this.prodList)
        ) {
            return;
        }
        if (this._doSetSelectedList()) {
            return;
        }

        const promoDetails = this._promoSvc.getPromoDetails();
        if (
            !this._commonUtil.isObject(promoDetails)
        ) {
            return;
        }

        const targetProductsModel: any = promoDetails.targetProductsModel;
        if (!this._commonUtil.isObject(targetProductsModel)) {
            return;
        }
        const selectedListIds: number[] =
            this._commonUtil.isObject(targetProductsModel.byList) ?
                [targetProductsModel.byList.listId] : [];
        this._promoSvc
            .setTargetProdList('byList', selectedListIds);
        this._doSetSelectedList();
    }
    private _subscribeEvents() {
        // Set tempalte ready flag
        this.subscriptions.push(
            this._promoSvc
                .onTemplateReady
                .subscribe((isTemplateReady: boolean) => {
                    if (!isTemplateReady) {
                        return;
                    }
                    this.setFormFields([
                        'BY_LIST'
                    ]);
                    this.isTemplateReady = isTemplateReady;
                })
        );
        this.subscriptions.push(
            this._promoSvc
                .onPromotionLoaded
                .subscribe((isPromoLoaded: boolean) => {
                    if (isPromoLoaded) {
                        this.setEditableFormFields(this._promoSvc.getEditableFields('rules'),
                            this._promoSvc.isPromoConfigEditable());
                        this._isPromoLoaded = true;
                        this._setSelectedList();
                    }
                })
        );
    }
}
