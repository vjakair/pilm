/*****************************************************************
 * Proprietary & Confidential  |  © 2017 PhaseZero Ventures LLC  *
 * This is part of PhaseZero Ventures LLC and cannot be copied,  *
 * modified and/or distributed without the express permission of *
 * PhaseZero Ventures LLC                                        *
 *****************************************************************/
/**
 * @author: Arvind
 */
import {
    Component,
    EventEmitter
} from '@angular/core';
import {
    CommonUtilService
} from '../../../core/commonUtil.service';
import {
    EventEmitterService
} from '../../../core/event-emitter';

@Component({
    selector: 'report-modal',
    templateUrl: './report-modal.component.html',
    styleUrls: ['./report-modal.style.scss']
})

export class ReportModalComponent {
    public reportComplete: EventEmitter<any>;
    constructor(
        private _commonUtil: CommonUtilService,
        eventEmitter: EventEmitterService
    ) {
        eventEmitter.onDownloadReport.subscribe(
            (alert: any) => this._downloadAlert(alert)
        );
    }
    private _downloadAlert(alert: any) {
        if (!this._commonUtil.isObject(alert)) {
            return;
        }
        const alertType: string = alert.type;
    }
}
