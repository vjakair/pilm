/*****************************************************************
 * Proprietary & Confidential  |  © 2017 PhaseZero Ventures LLC  *
 * This is part of PhaseZero Ventures LLC and cannot be copied,  *
 * modified and/or distributed without the express permission of *
 * PhaseZero Ventures LLC                                        *
 *****************************************************************/
/**
 * @author: Arvind
 */

import {
    Component, ChangeDetectorRef, ViewChild, OnInit
} from '@angular/core';

import {
    CommonUtilService
} from '../../core/commonUtil.service';
import {
    NavigationService
} from '../../core/navigation.service';
import {
    BsModalRef
} from 'ngx-bootstrap/modal/modal-options.class';
import {
    TitleService
} from '../../core/title';
import {
    NgForm
} from '@angular/forms';
import {
    BsModalService
} from 'ngx-bootstrap/modal';

import {
    ReportModalComponent
} from './report-modal/reportModal.component';
import { DateUtilService } from '../../core/dateUtil.service';
import { EventEmitterService } from '../../core/event-emitter';
import { ApiDispatcherService } from '../../core/apiDispatcher.service';
import * as FileSaver from 'file-saver';

@Component({
    selector: 'downloadReport',
    templateUrl: './download-report.component.html',
    styleUrls: ['./download-report.style.scss']
})

export class DownloadReportComponent implements OnInit{
    public formValidated: boolean = false;
    public dpPlacement: string = 'top';
    @ViewChild('promoFrm')
    public promoFrm: NgForm;
    public report: any = {
        notifyTime: {
            hours: '12',
            min: '00',
            ampm: 'AM'
        },
        tempMeta: {
            disableOtherDt: true
        },
        startDate: this._dateUtilSvc.getOtherDate(this._dateUtilSvc.getToday(), -30),
        endDate: this._dateUtilSvc.getToday()
    };
    public downloadReportFileNaame: string = 'Promotion-Reports.xlsx';
    // this._dateUtilSvc.getOtherDate(new Date(), -15)
    public startDtConfig: any = {
        minDate: this._dateUtilSvc.getOtherDate(this._dateUtilSvc.getToday(), -1200),
        maxDate: this._dateUtilSvc.getToday(),
        isDisabled: false,
        name: 'startDate',
        id: 'sDp',
        dateInvalid: true,
        bsConfig: {
            containerClass: 'theme-default',
            dateInputFormat: 'M/D/YYYY',
            placement: 'bottom',
            showWeekNumbers: false
        }
    };
    public endDtConfig: any = {
        minDate: this._dateUtilSvc.getOtherDate(this._dateUtilSvc.getToday(), -30),
        maxDate: this._dateUtilSvc.getToday(),
        manualChange: false,
        dateInvalid: true,
        myDateValue: Date,
        bsConfig: {
            containerClass: 'theme-default',
            dateInputFormat: 'M/D/YYYY',
            placement: 'bottom',
            showWeekNumbers: false
        }
    };

    constructor(
        private _navigationSvc: NavigationService,
        public titleSvc: TitleService,
        private _modalService: BsModalService,
        private _dateUtilSvc: DateUtilService,
        private _ref: ChangeDetectorRef,
        private _eventEmitter: EventEmitterService,
        private _apiDispatcherService: ApiDispatcherService
    ) {
        titleSvc.setTitle('REPORT');
    }
    public ngOnInit() {
        this.report.startDate = this._dateUtilSvc.getOtherDate(this._dateUtilSvc.getToday(), -30);
        this.report.endDate = this._dateUtilSvc.getToday();
        this._ref.detectChanges();
    }
    public onStartDateChange(date: string | boolean): void {
        if (!date) {
            this.invalidateOtherDates();
            return;
        }
        // const nextDayDate: Date = this._dateUtilSvc.getDateFormat(date);
        this.startDtConfig.manualChange = true;
        this.startDtConfig.dateInvalid = false;
        this.report.startDate = date;
        this.report.tempMeta.disableOtherDt = false;
        this.endDtConfig.minDate = this.report.startDate;
        this._ref.detectChanges();
        // Set minimum date for end date
        /*const nextDayDate: Date = this._dateUtilSvc.getDateFormat(date);
        this.endDtConfig.minDate = nextDayDate;
        if (
            !this.report.endDate ||
            !this.endDtConfig.manualChange
        ) {
            this.endDtConfig.autoSelected = true;
            this.endDtConfig.bsConfig.containerClass = 'theme-default auto-selected';
            setTimeout(() => {
                this.endDtConfig.autoSelected = false;
            }, 50);
            this.report.endDate = nextDayDate;
        }
        if (this.report.endDate && this.report.endDate < nextDayDate) {
            this.endDtConfig.dateInvalid = true;
            this.formValidated = true;
        } else {
            this.endDtConfig.dateInvalid = false;
        }
        this.startDtConfig.manualChange = true;
        this.startDtConfig.dateInvalid = false;
        this.report.startDate = date;
        this.report.tempMeta.disableOtherDt = false;
        this._ref.detectChanges();*/
    }
    public onEndDateChange(date: string | boolean): void {
        if (!date || this.endDtConfig.autoSelected) {
            this.endDtConfig.dateInvalid = true;
            return;
        }
        this.endDtConfig.dateInvalid = false;
        this.report.endDate = date;
        this.endDtConfig.manualChange = true;
        this.startDtConfig.maxDate = date;
        this._ref.detectChanges();
    }
    public invalidateOtherDates() {
        this.report.endDate = undefined;
        this.startDtConfig.dateInvalid = true;
        this.endDtConfig.dateInvalid = true;
        this.report.tempMeta.disableOtherDt = true;
    }

    public downloadReport(): void {
        this.formValidated = true;
        if (!this.startDtConfig.dateInvalid && !this.endDtConfig.dateInvalid && this.endDtConfig.manualChange) {
            const fromDate: any = this._dateUtilSvc.getFormattedDate(this.report.startDate);
            const toDate: any = this._dateUtilSvc.getFormattedDate(this.report.endDate);
            const loadData = {
                fromDate: fromDate,
                toDate: toDate
            };
            this._apiDispatcherService.doPostApiCall('reportDownloadPromoUsage', loadData, '', '', '', 'arraybuffer')
            .subscribe((response: any) => {
                var b = new Blob([response], { type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" });
                FileSaver.saveAs(b,  this.downloadReportFileNaame);

            });
        } else {
            this.endDtConfig.dateInvalid = true;
        }
    }

    public cancel(): void {
        this._navigationSvc
            .navigate(['/']);
    }

    public onScroll($event: any): void {
        if (false === $event instanceof Event) {
            return;
        }
        const scrollTop: number = $event.target && $event.target.scrollTop;
        let dpPlacement: string;
        if (162 >= scrollTop) {
            if ('bottom' === this.dpPlacement) {
                dpPlacement = 'top';
            }
        } else if ('top' === this.dpPlacement) {
            dpPlacement = 'bottom';
        }
        if (dpPlacement && dpPlacement !== this.dpPlacement) {
            this.dpPlacement = dpPlacement;
            this._eventEmitter.onDatePickerOrientationChange.emit(dpPlacement);
        }
    }
}
