/*****************************************************************
 * Proprietary & Confidential  |  © 2017 PhaseZero Ventures LLC  *
 * This is part of PhaseZero Ventures LLC and cannot be copied,  *
 * modified and/or distributed without the express permission of *
 * PhaseZero Ventures LLC                                        *
 *****************************************************************/
/**
 * @author: Irshadahmed
 */
import {
    Pipe,
    PipeTransform
} from '@angular/core';
@Pipe({
    name: 'myCustmerFilter'
})
export class CustomerFilterPipe implements PipeTransform {
    public transform(customers: any[], searchTerm: string): any[] {
        if (!customers || '' === searchTerm || !searchTerm) {
            return customers;
        }
        return customers.filter((customer: any) => {
            const custName: string = customer['name'].toLowerCase();
            const custNo: string = customer['customerNumber'];
            const searchKey: string = 'number' === typeof searchTerm ?
                searchTerm : searchTerm.toLowerCase();
            return -1 !== custName.indexOf(searchKey) ||
                -1 !== custNo.indexOf(this._formatSearchKey(searchKey)) ||
                -1 !== this._searchContacts(customer.contacts, searchKey);
        });
    }

    private _formatSearchKey(searchKey: number | string): string {
        if ('number' === typeof searchKey) {
            searchKey = searchKey.toString();
        }
        searchKey = Number(searchKey).toString();
        return searchKey;
    }

    private _searchContacts(contacts: any[], searchKey: string): number {
        if (contacts && contacts instanceof Array && contacts.length) {
            for (const contact of contacts) {
                if ('object' !== typeof contact) {
                    continue;
                }
                const contactType: string = contact.danaContactType ?
                    contact.danaContactType.toLowerCase() : undefined;
                if (contactType && -1 !== contactType.indexOf(searchKey)) {
                    return 1;
                }
            }
        }
        return -1;
    }
}
