/*****************************************************************
 * Proprietary & Confidential  |  © 2017 PhaseZero Ventures LLC  *
 * This is part of PhaseZero Ventures LLC and cannot be copied,  *
 * modified and/or distributed without the express permission of *
 * PhaseZero Ventures LLC                                        *
 *****************************************************************/
/**
 * @author: Irshadahmed
 */
import {
    Component,
    EventEmitter,
    ViewEncapsulation
} from '@angular/core';
import {
    NgForm
} from '@angular/forms';
import {
    BsModalRef
} from 'ngx-bootstrap/modal/modal-options.class';
import {
    CommonUtilService
} from '../../core/commonUtil.service';
import {
    PromotionService
} from '../promotions.service';
import * as FileSaver from 'file-saver';
import { EventEmitterService } from '../../core/event-emitter';
@Component({
    selector: 'customer-list',
    templateUrl: './customerListModal.component.html',
    styleUrls: ['./customerListModal.style.scss'],
    encapsulation: ViewEncapsulation.None
})
export class CustomerListModalComponent {
    public customers: any[];
    public backupCustomers: any[];
    public customer: any[];
    public targetMarket: any;
    public allCustomerSelection: string;
    public sortBy: string;
    public searchTerm: string;
    private _customers: any[];
    public uploadConfig: any = {
        csvOpt: {
            type: 'CSV'
        }
    };
    private _isFileUploading: boolean;
    public customerUpload: boolean = false;
    public invalidCustomerUpload: boolean = false;

    constructor(
        public bsModalRef: BsModalRef,
        private _commonUtil: CommonUtilService,
        private _promotionSvc: PromotionService,
        private _promoSvc: PromotionService,
        private _eventEmitter: EventEmitterService,
    ) { }

    ngOnInit() {
        const targetsData = this._promoSvc.getTemplateFields('targets');
        this.customerUpload = this._commonUtil.isValidArray(targetsData) && targetsData.indexOf('CUSTOMER_UPLOAD') > -1;
    }

    public setContent(_targetMarget): void {
        this.targetMarket = _targetMarget;
        this.backupCustomers = this._commonUtil.getClonedArray(this.targetMarket.customers);
    }
    public cancelSelection(): void {
        this.bsModalRef.hide();
        if (!this._commonUtil.isValidArray(this.customers)) {
            return;
        }
        this.targetMarket.customers = this.backupCustomers;
        this.customers.forEach((customer: any): void => {
            if (
                !this._commonUtil.isObject(customer) ||
                this._commonUtil.isUnDefined(customer.selected)
            ) {
                return;
            }

            if (this._commonUtil.isObject(customer.tempMeta)) {
                const originalSelection: string = customer.tempMeta.selected;
                customer.selected = originalSelection;
            } else {
                delete customer.selected;
            }
            if (!this._commonUtil.isValidArray(customer.contacts)) {
                return;
            }
            customer.contacts.forEach((contact: any): void => {
                if (
                    !this._commonUtil.isObject(contact) ||
                    this._commonUtil.isUnDefined(contact.selected)
                ) {
                    return;
                }
                contact.selected = !contact.selected;
            });
        });
    }
    public saveSelection(isModalClose: boolean = true): void {
        if (!this._commonUtil.isValidArray(this.customers)) {
            return;
        }
        this.customers.forEach((customer: any): void => {
            if (!this._commonUtil.isObject(customer)) {
                return;
            }
            const result: any = this._getExistingCustomer(customer.customerNumber);
            if (this._commonUtil.isObject(result)) {
                this.targetMarket.customers.splice(result.index, 1);
            }
            if ('NONE' === customer.selected || !customer.selected) {
                return;
            }
            const existingCustomer: any = {
                name: customer.name,
                customerNumber: customer.customerNumber,
                contacts: []
            };
            if ('ALL' === customer.selected) {
                existingCustomer.contacts = customer.contacts;
            } else {
                this._updateCustomerContacts(existingCustomer, customer.contacts);
            }
            if (this._commonUtil.isObject(result)) {
                this.targetMarket.customers.splice(result.index, 0, existingCustomer);
            } else {
                this.targetMarket.customers.push(existingCustomer);
            }
        });
        if (isModalClose) {
            this.bsModalRef.hide();
        }
    }
    public sortBySelection(): void {
        const customers: any[] = this.customers;
        if (!this._commonUtil.isValidArray(customers)) {
            return;
        }
        this.sortBy = 'ALL' === this.sortBy ?
            'NONE' : 'ALL';
        for (const i in customers) {
            if (!this._commonUtil.isObject(customers[i])) {
                continue;
            }
            const iSelection: string = customers[i].selected;
            let condition1: boolean;

            if ('ALL' === this.sortBy) {
                condition1 = 'ALL' === iSelection;
            } else {
                condition1 = 'NONE' === iSelection || !iSelection;
            }
            if (condition1) {
                continue;
            }
            let j: number = parseInt(i, 0);

            while (j < customers.length) {
                let temp: any;
                const jSelection: string = customers[j].selected;
                let condition2: boolean;

                if ('ALL' === this.sortBy) {
                    condition2 = 'ALL' === jSelection ||
                        ('SOME' === jSelection && (!iSelection || 'NONE' === iSelection));
                } else {
                    condition2 = (!jSelection || 'NONE' === jSelection) ||
                        ('SOME' === jSelection && 'ALL' === iSelection);
                }

                if (
                    condition2
                ) {
                    temp = customers[i];
                    customers[i] = customers[j];
                    customers[j] = temp;
                }

                ++j;
            }
        }
    }
    public toggleAllCustomerSelection(): void {
        let customerSelection: string;
        if ('SOME' === this.allCustomerSelection || 'NONE' === this.allCustomerSelection) {
            this.allCustomerSelection = 'ALL';
            customerSelection = 'NONE';
        } else {
            this.allCustomerSelection = 'NONE';
            customerSelection = 'ALL';
        }
        const customers: any[] = this.customers;
        if (!this._commonUtil.isValidArray(customers)) {
            return;
        }
        customers.forEach((customer: any) => {
            customer.selected = customerSelection;
            this.toggleCustomerSelection(customer);
        });
    }
    public toggleCustomerSelection(customer: any): void {
        let contactSelected: boolean;
        if (!customer.selected || 'SOME' === customer.selected || 'NONE' === customer.selected) {
            customer.selected = 'ALL';
            contactSelected = true;
        } else {
            customer.selected = 'NONE';
            contactSelected = false;
        }
        this._setContactSelection(customer.contacts, contactSelected);
        this._setAllCustomerSelection();
    }
    public toggleContactSelection(contact: any, customer: any) {
        contact.selected = !contact.selected;
        this._setCustomerSelection(customer);
    }

    public searchCustomer(searchTerm) {
        let search = '*';
        if (searchTerm) {
            search = searchTerm;
        }
        this.saveSelection(false);
        this._promoSvc
            .getCustomerList(
                this.targetMarket.buyingGroup,
                this.targetMarket.channel,
                this.targetMarket.lineOfBusiness,
                this.targetMarket.rtb,
                this.targetMarket.contactType,
                search
            )
            .subscribe((response: any) => {
                if (response.errorCode || !this._commonUtil.isValidArray(response.customerInfo)) {
                    this._customers = [];
                } else {
                    this._customers = response.customerInfo;
                }
                this._setCustomerList(this._customers);
            });
    }

    public cancelCustomerFileUpload(ev: any): void {
    }

    public uploadCustomerFile(file: any): void {
        const promoDraft: any = this._promoSvc.promotionDraft;
        this._isFileUploading = true;
        this.invalidCustomerUpload = false;
        this._toggleActionBar();

        this._promoSvc.uploadCustomerFile(file.data, promoDraft.promoId).subscribe((response: any) => {
            if (this._commonUtil.isObject(response) && response.targets && response.targets.targetMarket && response.targets.targetMarket.customers) {
                this.targetMarket.customers = response.targets.targetMarket.customers;
                this.bsModalRef.hide();
            } else if (response && response.status === 400) {
                this.invalidCustomerUpload = true;
            }

            this._isFileUploading = false;
            this._toggleActionBar();

        }, (e) => {
            console.log('uploadCustomerFile--->', e);
            this._isFileUploading = false;
            this._toggleActionBar();
        });
    }

    public downloadCsv(): void {
        this._promoSvc.downloadCustomerFile(
            this.targetMarket.buyingGroup,
            this.targetMarket.channel,
            this.targetMarket.lineOfBusiness,
            this.targetMarket.rtb,
            this.targetMarket.contactType
        ).subscribe((response: any) => {
            let b = new Blob([response], { type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" });
            FileSaver.saveAs(b, 'CustomerDetails.csv');
        });
    }

    private _toggleActionBar(): void {
        this._eventEmitter.onToggleActions.emit(this._isFileUploading);
    }

    private _getExistingCustomer(customerNumber: number): void {
        const targetCustomers: any[] = this.targetMarket.customers;
        if (!customerNumber || !this._commonUtil.isValidArray(targetCustomers)) {
            return;
        }
        return this._commonUtil
            .getFilteredItemWithIndex(targetCustomers, 'customerNumber', customerNumber);
    }
    private _setContactSelection(contacts: any[], selected: boolean): void {
        if (!this._commonUtil.isValidArray(contacts)) {
            return;
        }
        contacts.forEach((contact) => {
            contact.selected = selected;
        });
    }
    private _setCustomerSelection(customer: any): void {
        const contacts: any[] = customer.contacts;
        const selectedContacts: any[] = this._commonUtil
            .getFilteredItem(contacts, 'selected', true, true);
        const selectedCount: number = selectedContacts.length;
        if (contacts.length === selectedCount) {
            customer.selected = 'ALL';
            this._setAllCustomerSelection();
        } else if (0 < selectedCount) {
            customer.selected = 'SOME';
            this.allCustomerSelection = 'SOME';
        } else {
            customer.selected = 'NONE';
            this._setAllCustomerSelection();
        }
    }
    private _setAllCustomerSelection(): void {
        const selectedCustomers: any[] = this._commonUtil
            .getFilteredItem(this.customers, 'selected', 'ALL', true);
        const selectCustCount: number = selectedCustomers.length;
        if (selectCustCount === this.customers.length) {
            this.allCustomerSelection = 'ALL';
        } else if (0 < selectCustCount) {
            this.allCustomerSelection = 'SOME';
        } else {
            this.allCustomerSelection = 'NONE';
        }
    }
    private _setCustomerList(customers: any[]): void {
        if (!this._commonUtil.isValidArray(customers)) {
            return;
        }
        if (!this._commonUtil.isValidArray(this.targetMarket.customers)) {
            this.customers = customers;
            return;
        }
        let allCustSelectedCount: number = 0;
        let oneContactSelected: boolean = false;
        customers.forEach((customer: any): void => {
            const result = this._commonUtil.getFilteredItem(
                this.targetMarket.customers, 'customerNumber', customer.customerNumber
            );
            if (
                !this._commonUtil.isObject(result) ||
                !this._commonUtil.isValidArray(result.contacts)
            ) {
                return;
            }
            const contacts: any[] = customer.contacts;
            let selectedCount: number = 0;
            contacts.forEach((contact: any): void => {
                const selectedContact: any = this._commonUtil.getFilteredItem(
                    result.contacts,
                    'name',
                    contact.name
                );
                if (!this._commonUtil.isObject(selectedContact)) {
                    return;
                }
                contact.selected = true;
                oneContactSelected = true;
                ++selectedCount;
            });
            let origSel: string;
            if (selectedCount === contacts.length) {
                ++allCustSelectedCount;
                origSel = 'ALL';
            } else if (0 < selectedCount) {
                origSel = 'SOME';
            } else {
                origSel = 'NONE';
            }
            customer.selected = origSel;
            customer.tempMeta = {
                originalSelection: origSel
            };
        });
        if (allCustSelectedCount === customers.length) {
            this.allCustomerSelection = 'ALL';
        } else if (oneContactSelected) {
            this.allCustomerSelection = 'SOME';
        } else {
            this.allCustomerSelection = 'NONE';
        }
        this.customers = customers;
    }
    private _updateWithCustContacts(customers: any[]): void {
        customers.forEach((customer: any): void => {
            if (!customer.contacts && this._commonUtil.isValidArray(customer.Contacts)) {
                customer.contacts = customer.Contacts;
            }
        });
    }
    private _updateCustomerContacts(customer: any, contacts: any[]) {
        if (!this._commonUtil.isValidArray(contacts)) {
            return;
        }
        contacts.forEach((contact: any): void => {
            if (contact.selected) {
                customer.contacts.push({
                    name: contact.name,
                    email: contact.email,
                    danaContactType: contact.danaContactType
                });
            }
        });
    }
}
