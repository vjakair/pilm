/*****************************************************************
 * Proprietary & Confidential  |  © 2017 PhaseZero Ventures LLC  *
 * This is part of PhaseZero Ventures LLC and cannot be copied,  *
 * modified and/or distributed without the express permission of *
 * PhaseZero Ventures LLC                                        *
 *****************************************************************/
/**
 * @author: Irshadahmed
 */
import {
    Component,
    OnDestroy,
    ViewEncapsulation
} from '@angular/core';
import {
    AppConfig
} from '../../config/app.config';
import {
    BsModalRef
} from 'ngx-bootstrap/modal/modal-options.class';

// Import Custom services
import {
    AuthService
} from '../../auth/auth.service';
import {
    CommonUtilService
} from '../../core/commonUtil.service';
import {
    EventEmitterService
} from '../../core/event-emitter';
import {
    SubscriptionHistory
} from '../../helper/subscriptionHistory.helper';
import {
    TranslationService
} from '../../core/translation.service';

@Component({
    selector: 'admin-console-modal',
    templateUrl: './adminConsoleModal.component.html',
    styleUrls: ['./adminConsoleModal.style.scss'],
    encapsulation: ViewEncapsulation.None
})
export class AdminConsoleModalComponent extends SubscriptionHistory implements OnDestroy {
    public approverGroups: any[] = [];
    public defaultText: string;
    public errors: any = {};
    private _formInvalid: boolean = false;
    private _isDataChanged = false;
    private _promoApproverGroups: any;
    private _selectedUserIds: any = [];
    constructor(
        public appConfig: AppConfig,
        public bsModalRef: BsModalRef,
        private _authService: AuthService,
        private _commonUtil: CommonUtilService,
        private _eventEmitter: EventEmitterService,
        translationSvc: TranslationService
    ) {
        super();
        translationSvc
            .getTranslation('COMMON.SEL')
            .subscribe((defaultText: string) => {
                this.defaultText = defaultText;
            });
        this.subscriptions.push(
            _eventEmitter.onNavigationSuccess.subscribe(
                () => this.closePopover()
            )
        );
    }
    public ngOnDestroy() {
        this.unsubscribe();
    }
    public closePopover() {
        this.bsModalRef.hide();
    }
    public saveChanges() {
        const payload: any = {
            tenantId: this.appConfig.getTenantId(),
            domain: window.location.hostname,
            feature: {
                name: 'PromotionApprovers',
                settings: {},
                enable: 'true'
            }
        };
        const promoApprover: any = this.appConfig.getAppFeatureSettings('PromotionApprovers');
        if (!this._commonUtil.isObject(promoApprover)) {
            return;
        }
        const promoApproverGroups: any = promoApprover.settings || {};

        for (const approverGroup of this.approverGroups) {
            if (!this._commonUtil.isObject(approverGroup)) {
                return;
            }
            const groupId = approverGroup.groupId;
            const selectedUser = this._commonUtil.getFilteredItem(
                approverGroup.groupUsers, 'selected', true
            );
            if (!this._commonUtil.isObject(selectedUser)) {
                this.errors[groupId] = {};
                this.errors[groupId].invalid = true;
                this._formInvalid = true;
            }
            const userId: string = promoApproverGroups[groupId] ?
                promoApproverGroups[groupId][0] : undefined;
            payload.feature.settings[groupId] = [selectedUser.userId];
            if (selectedUser.userId !== userId) {
                this._isDataChanged = true;
            }
        }
        if (this._formInvalid) {
            return;
        }
        if (!this._isDataChanged) {
            this.closePopover();
            return;
        }
        const application = this.appConfig.getApplicationName();
        this._authService
            .saveAdminConsoleChanges(
                payload.feature, // PZVAF-14010 we can see on comment
                application
            ).subscribe((response: any) => {
                if (response.status === 'SUCCESS') {
                    this.appConfig.setAppFeatureSettings(
                        payload.feature.name,
                        payload.feature.settings
                    );
                    this._eventEmitter
                        .onApproverGroupChange
                        .emit();
                    this.closePopover();
                } else {
                    this._eventEmitter
                        .onAlert
                        .emit({
                            type: 'alert-warning',
                            msgKey: 'PROMOTION.ERROR.SERVER'
                        });
                }
            });
    }
}
