/*****************************************************************
 * Proprietary & Confidential  |  © 2017 PhaseZero Ventures LLC  *
 * This is part of PhaseZero Ventures LLC and cannot be copied,  *
 * modified and/or distributed without the express permission of *
 * PhaseZero Ventures LLC                                        *
 *****************************************************************/
/**
 * @author: Manjesh
 */
import {
    Component
} from '@angular/core';

import {
    CommonUtilService
} from '../../core/commonUtil.service';
import {
    NavigationService
} from '../../core/navigation.service';
import {
    PromotionService
} from '../promotions.service';
import {
    TitleService
} from '../../core/title';
@Component({
    selector: 'notification',
    templateUrl: './notification.component.html',
    styleUrls: ['./notification.style.scss']
})
export class NotificationComponent {
    public notificationDetails: any;
    public totalItems: number;
    public currentPage: number = 1;
    public itemsPerPage: number = 10;
    public searchTxt: string;
    public readonly maxSize: number = 10;
    constructor(
        private _commonUtil: CommonUtilService,
        private _navigationSvc: NavigationService,
        private _promoSvc: PromotionService,
        titleSvc: TitleService
    ) {
        titleSvc.setTitle('APPROVAL_PROCESS');
        this.fetchNotificationList();
    }

    public setPage(pageNo: number): void {
        this.currentPage = pageNo;
    }

    public pageChanged(event: any): void {
        this.currentPage = event.page;
        this.fetchNotificationList();
    }

    public resetSearchTerm($event): void {
        $event.preventDefault();
        $event.stopPropagation();
        this.searchTxt = '';
        document.getElementById('searchBy').focus();
        this.currentPage = 1;
        this.fetchNotificationList();
    }

    public searchNotification(): void {
        this._commonUtil
            .delayCallback('searchingParts', () => {
                this.currentPage = 1;
                this.fetchNotificationList();
            }, 500);
    }

    public fetchNotificationList(): void {
        const params = {
            page: this.currentPage - 1,
            size: this.itemsPerPage
        };
        this._promoSvc
            .getNotificationList(this.searchTxt, params, false)
            .subscribe((response) => {
                if (response && response.content && response.content.length > 0) {
                    this.notificationDetails = response;
                    this.totalItems = this.notificationDetails.totalElements;
                } else {
                    this.notificationDetails = {};
                    this.totalItems = 0;
                }
            });
    }

    public showPromoDetails(promotion: any): void {
        const notificationId: number = promotion.id;
        this._navigationSvc
            .navigate(['/promotions', promotion.promoId, 'details', notificationId]);
    }
}
