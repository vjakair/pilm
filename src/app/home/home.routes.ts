/*****************************************************************
 * Proprietary & Confidential  |  © 2017 PhaseZero Ventures LLC  *
 * This is part of PhaseZero Ventures LLC and cannot be copied,  *
 * modified and/or distributed without the express permission of *
 * PhaseZero Ventures LLC                                        *
 *****************************************************************/
/**
 * @author: Irshadahmed
 */
// Import Routes
import {
    Routes
} from '@angular/router';
// Import Custom components and services
import {
    HomeComponent
} from './';
import {
    AuthenticatedGuard
} from '../auth/authenticated.guard';
export const ROUTES: Routes = [
    {
        path: '',
        component: HomeComponent,
        canActivate: [AuthenticatedGuard],
        /* children: [
            { path: 'promo-details', component: PromoDetailsComponent },
            { path: 'promo', component: PromoSnapDetailsComponent }
        ]  */
    }
];
