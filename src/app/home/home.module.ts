/*****************************************************************
 * Proprietary & Confidential  |  © 2017 PhaseZero Ventures LLC  *
 * This is part of PhaseZero Ventures LLC and cannot be copied,  *
 * modified and/or distributed without the express permission of *
 * PhaseZero Ventures LLC                                        *
 *****************************************************************/
/**
 * @author: Irshadahmed
 */
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import {
    NgModule
} from '@angular/core';
import {
    RouterModule,
    PreloadAllModules
} from '@angular/router';
import {
    TranslateModule
} from '@ngx-translate/core';
import {
    ComponentsModule
} from '../components/components.module';
import {
    PromotionModule
} from '../promotions/promotion.module';
import {
    ROUTES
} from './home.routes';
import {
    HomeComponent
} from './home.component';
@NgModule({
    declarations: [
        /**
         * Components / Directives/ Pipes
         */
        HomeComponent
    ],
    imports: [
        CommonModule,
        ComponentsModule,
        FormsModule,
        PromotionModule,
        RouterModule.forRoot(ROUTES, { useHash: false, preloadingStrategy: PreloadAllModules }),
        TranslateModule
    ]
})
export class HomeModule {
}
