/*****************************************************************
 * Proprietary & Confidential  |  © 2017 PhaseZero Ventures LLC  *
 * This is part of PhaseZero Ventures LLC and cannot be copied,  *
 * modified and/or distributed without the express permission of *
 * PhaseZero Ventures LLC                                        *
 *****************************************************************/
/**
 * @author: Irshadahmed
 */
import {
    Component,
    OnDestroy,
    OnInit,
    Inject,
    Input
} from '@angular/core';

import {
    AppState
} from '../app.service';
import {
    CommonUtilService
} from '../core/commonUtil.service';
import {
    EventEmitterService
} from '../core/event-emitter';
import {
    NavigationService
} from '../core/navigation.service';
import {
    PromotionService
} from '../promotions/promotions.service';
import {
    SubscriptionHistory
} from '../helper/subscriptionHistory.helper';
import {
    TitleService
} from '../core/title';
import {
    XLargeDirective
} from './x-large';
import {
    trigger,
    state,
    style,
    animate,
    keyframes,
    transition
} from '@angular/animations';
@Component({
    /**
     * The selector is what angular internally uses
     * for `document.querySelectorAll(selector)` in our index.html
     * where, in this case, selector is the string 'home'.
     */
    selector: 'home',  // <home></home>
    /**
     * We need to tell Angular's Dependency Injection which providers are in our app.
     */
    providers: [

    ],
    /**
     * Our list of styles in our component. We may add more to compose many styles together.
     */
    styleUrls: ['./home.style.scss'],
    /**
     * Every Angular template is first compiled by the browser before Angular runs it's compiler.
     */
    templateUrl: './home.component.html'
})
export class HomeComponent extends SubscriptionHistory implements OnDestroy, OnInit {
    @Input()
    public doShowPromoDetails: () => void;
    /**
     * Set our default values
     */
    public localState = { value: '' };
    /**
     * TypeScript public modifiers
     */
    public pendingTaskCount: number;
    public promoSnaps: any[];
    public promoPendingTasks: any[];
    public recentActivities: any[];
    private _subscriptions: any[] = [];
    constructor(
        public appState: AppState,
        private _commonUtil: CommonUtilService,
        private _eventEmitter: EventEmitterService,
        private _promotionSvc: PromotionService,
        private _navigationSvc: NavigationService,
        titleService: TitleService
    ) {
        super();
        titleService.setTitle('HOME');
        this._subscribeEvents();
        this._getPromoSnaps();
        this._getPromoPendingTasks();
        this._getRecentActivities();
    }

    public goToPromotionDetails(promotion: any): void {
        this._navigationSvc.navigate(['promotions', promotion.status, 'snapshots']);
    }

    public ngOnDestroy() {
        this.unsubscribe();
    }

    public ngOnInit() {
        console.log('hello `Home` component');
        /**
         * this.title.getData().subscribe(data => this.data = data);
         */
    }

    public submitState(value: string) {
        console.log('submitState', value);
        this.appState.set('value', value);
        this.localState.value = '';
    }
    public onActionComplete(): void {
        this._getPromoSnaps();
        this._getPromoPendingTasks();
        this._getRecentActivities();
    }
    private _getPromoPendingTasks() {
        this._promotionSvc
            .getPendingPromotions()
            .subscribe(
            (response: any) => {
                if (
                    response.errorCode ||
                    !this._commonUtil.isValidArray(response)
                ) {
                    this.promoPendingTasks = [];
                    this.pendingTaskCount = 0;
                    return;
                }
                const promoPendingTasks: any[] = response;
                this._setApproverGroupInfo(promoPendingTasks);
            });
    }
    private _getPromoSnaps() {
        this._promotionSvc
            .getPromoSnaps(false)
            .subscribe(
            (response: any) => {
                if (
                    response.errorCode ||
                    !this._commonUtil.isValidArray(response)
                ) {
                    this.promoSnaps = [];
                    return;
                }
                this.promoSnaps = response;
            });
    }
    private _getRecentActivities() {
        this._promotionSvc
            .getRecentActivities(false)
            .subscribe((response: any) => {
                if (
                    response.errorCode ||
                    !this._commonUtil.isValidArray(response)
                ) {
                    this.recentActivities = [];
                    return;
                }
                this.recentActivities = response;
            });
    }
    private _setApproverGroupInfo(promoPendingTasks: any[]) {
        this._promotionSvc
            .setPromoApprovers(promoPendingTasks)
            .subscribe((promotions: any[]) => {
                this.promoPendingTasks = promotions;
                this.pendingTaskCount = promotions.length;
            });
    }
    private _subscribeEvents() {
        this.subscriptions
            .push(
            this._eventEmitter
                .onApproverGroupChange
                .subscribe(() => this._setApproverGroupInfo(this.promoPendingTasks))
            );
    }
}
