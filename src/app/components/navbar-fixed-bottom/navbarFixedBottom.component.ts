import {
    Component,
    EventEmitter,
    Input,
    OnInit,
    Output,
    ViewEncapsulation
} from '@angular/core';
@Component({
    selector: 'navbar-fixed-bottom',
    template: `
    <nav class="navbar navbar-default navbar-fixed-bottom pilm-fixed-navbar">
    <div class="container-fluid">
        <div class="btn-wrapper pull-right">
            <button class="btn {{getBtnClass(button)}}" *ngFor="let button of buttons"
            (click)="buttonClicked.emit(button)"
            [disabled]="button.disabled"
            translate="{{button.text}}"></button>
        </div>
    </div>
</nav>
    `,
    styleUrls: ['./navbarFixedBottom.style.scss'],
    encapsulation: ViewEncapsulation.None
})
export class NavbarFixedBottomComponent {
    @Output()
    public buttonClicked: EventEmitter<any> = new EventEmitter();
    @Input()
    public buttons: any[];
    public getBtnClass(button: any) {
        if (button.disabled) {
            return '';
        }
        let btnClass: string;
        if (button.hide) {
            return 'hide';
        }
        if (button.isPrimary) {
            btnClass = 'btn-primary';
        } else {
            btnClass = 'btn-default';
        }
        btnClass += button.inverse ? ' inverse' : '';
        btnClass += button.showBorder ? ' bordered' : '';
        return btnClass;
    }
}
