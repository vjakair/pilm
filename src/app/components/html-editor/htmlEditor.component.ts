
/*****************************************************************
 * Proprietary & Confidential  |  © 2017 PhaseZero Ventures LLC  *
 * This is part of PhaseZero Ventures LLC and cannot be copied,  *
 * modified and/or distributed without the express permission of *
 * PhaseZero Ventures LLC                                        *
 *****************************************************************/
/**
 * @author: Irshadahmed
 */

import {
    Component,
    ChangeDetectorRef,
    OnDestroy,
    AfterViewInit,
    ViewEncapsulation,
    EventEmitter,
    Input,
    Output,
    ViewChild
} from '@angular/core';

import {
    BsModalService
} from 'ngx-bootstrap/modal';
import {
    BsModalRef
} from 'ngx-bootstrap/modal/modal-options.class';

import {
    EmailPreviewModalComponent
} from '../../promotions/email-preview-modal/emailPreviewModal.component';

@Component({
    selector: 'pilm-html-editor',
    template: `<textarea
    style="width: 100%;"
    class="form-control pilm-form-control"
    name="htmlContent"
    [(ngModel)]="content"
    #htmlContent="ngModel"
    id="{{elementId}}"
    (change)="updateContent()"
    required></textarea>
    <input type="file" #imageUploader accept="image/*" class="hide"
    (change)="onFileSelect($event)">
    <div class="actions">
    <button class="btn btn-primary" *ngIf="showCustomPreview" style="margin-right: 10px;"
    (click)="preivewEmailBody()" translate="BUTTONS.PREVIEW"
    [disabled]="!content || '' === content"></button>
    <button class="btn btn-default" (click)="clearHtml()" translate="BUTTONS.CLEAR"></button>
</div>
    `,
    styles: [
        `
        .actions {
            margin-top: 10px;
        }
        .mce-menubtn.mce-fixed-width span {
            width: auto !important;
            min-width: 30px !important;
        }
        div.mce-menubtn.mce-opened {
            z-index: 65535 !important;
        }
        .mce-menu.mce-animate {
            max-height: 342px !important;
        }
        `
    ],
    encapsulation: ViewEncapsulation.None
})
export class HtmlEditorComponent implements AfterViewInit, OnDestroy {
    @Input()
    public content: any;
    @Input()
    public elementId: string;
    @Output()
    public onEditorKeyup: EventEmitter<any> = new EventEmitter();
    @ViewChild('imageUploader')
    public imageUploader: any;
    public showCustomPreview: boolean;
    private _editor: any;
    private _imgCallback: (blobInfo: any, meta: any) => void;
    constructor(
        private _bsModalSvc: BsModalService,
        private _changeRef: ChangeDetectorRef
    ) { }
    public ngAfterViewInit() {
        const imageUploader: any = this.imageUploader;
        tinymce.init({
            selector: '#' + this.elementId,
            height: '220px',
            statusbar: false,
            plugins: [
                'advlist contextmenu preview autolink lists link image hr anchor pagebreak',
                'searchreplace code',
                'insertdatetime table',
                'textcolor colorpicker textpattern'
            ],
            branding: false,
            skin_url: 'assets/skins/lightgray',
            theme: 'modern',
            toolbar1: `insertfile undo redo | styleselect | bold italic
            | alignleft aligncenter alignright alignjustify | bullist numlist
            outdent indent | link image | tools`,
            toolbar2: 'forecolor backcolor fontsizeselect fontselect preview',
            /* menu: {
                file: { title: 'File', items: 'newdocument' },
                edit: { title: 'Edit', items: 'undo redo |
                cut copy paste pastetext | selectall' },
                insert: { title: 'Insert', items: 'link media | template hr' },
                view: { title: 'View', items: 'visualaid' },
                format: { title: 'Format', items: 'bold italic underline strikethrough superscript
                subscript | formats | removeformat' },
                table: { title: 'Table', items: 'inserttable tableprops deletetable |
                cell row column' },
                tools: { title: 'Tools', items: 'spellchecker code' }
            }, */
            fontsize_formats: '10pt 11pt 12pt 14pt 16pt 18pt 24pt 30pt 36pt',
            relative_urls: true,
            theme_advanced_statusbar_location: 'none',
            images_upload_handler: (blobInfo, success, failure) => {
                const content: any = this._editor.getContent();
                this.onEditorKeyup
                    .emit(content);
            },
            init_instance_callback: (editor: any) => {
                editor.setContent(this.content);
            },
            file_picker_callback: (callback: () => void, value: any, meta: any): void => {
                if ('image' !== meta.filetype) {
                    return;
                }
                imageUploader.nativeElement.click();
                this._imgCallback = callback;
            },
            file_browser_callback_types: 'image',
            setup: (editor) => {
                this.showCustomPreview = false;
                this._changeRef.detectChanges();
                this._editor = editor;
                editor.on('keyup Change ObjectResized', () => {
                    const content = editor.getContent();
                    this.onEditorKeyup.emit(content);
                });
            },
        });
        setTimeout(() => {
            if (!this._editor) {
                this.showCustomPreview = true;
            }
        }, 50);
    }

    public onFileSelect($event: any): void {
        const file: File = $event.target.files[0];
        const fileType: string = file.type;
        const type: string = fileType.substr(0, fileType.lastIndexOf('/'));
        if ('image' !== type) {
            return;
        }
        const reader = new FileReader();
        const callback: (blobInfo: any, meta: any) => void = this._imgCallback;
        reader.onload = function(e) {
            const target: any = e.target;
            callback(target.result, {
                alt: ''
            });
        };
        reader.readAsDataURL(file);
    }

    public preivewEmailBody(): void {
        const emailModal: BsModalRef = this._bsModalSvc.show(
            EmailPreviewModalComponent, {
                class: 'pilm-popup email-preview'
            }
        );
        emailModal.content.emailBody = this._editor ? this._editor.getContent() : this.content;
    }

    public clearHtml() {
        this.content = '';
        this._editor.setContent('');
        this.onEditorKeyup.emit(undefined);
    }

    public updateContent(): void {
        this.onEditorKeyup.emit(this.content);
    }

    public ngOnDestroy() {
        tinymce.remove(this._editor);
    }
}
