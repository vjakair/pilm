/*****************************************************************
 * Proprietary & Confidential  |  © 2017 PhaseZero Ventures LLC  *
 * This is part of PhaseZero Ventures LLC and cannot be copied,  *
 * modified and/or distributed without the express permission of *
 * PhaseZero Ventures LLC                                        *
 *****************************************************************/
/**
 * @author: Irshadahmed
 */
import {
    Component
} from '@angular/core';
// Import Custom Services
import {
    CommonUtilService
} from '../../core/commonUtil.service';
import {
    EventEmitterService
} from '../../core/event-emitter';
@Component({
    selector: 'alert',
    template: `<div *ngIf="alertType" class="alert-wrapper">
    <div class="alert {{alertType}} pilm-alert alert-dismissible" role="alert">
    <i class="fa {{alertIcon}}"></i><span translate="{{msgKey}}"></span></div>
</div>`,
    styleUrls: ['./alert.style.scss']
})
export class AlertComponent {
    public alertType: string = null;
    public alertIcon: string = null;
    public msgKey: string;
    constructor(
        private _commonUtil: CommonUtilService,
        eventEmitter: EventEmitterService
    ) {
        eventEmitter.onAlert.subscribe(
            (alert: any) => this._displayAlert(alert)
        );
    }
    private _displayAlert(alert: any) {
        if (!this._commonUtil.isObject(alert)) {
            return;
        }
        const alertType: string = alert.type;
        this.alertType = alertType;
        this.msgKey = alert.msgKey;
        this.alertIcon =
            'alert-success' === alertType
                ? 'fa-check-circle'
                : 'fa-exclamation-triangle';
        this._commonUtil
        .delayCallback('hideAlert', () => {
            this.alertType = null;
        }, 5000);
    }
}
