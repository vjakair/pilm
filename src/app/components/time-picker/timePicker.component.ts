/*****************************************************************
 * Proprietary & Confidential  |  © 2017 PhaseZero Ventures LLC  *
 * This is part of PhaseZero Ventures LLC and cannot be copied,  *
 * modified and/or distributed without the express permission of *
 * PhaseZero Ventures LLC                                        *
 *****************************************************************/
/**
 * @author: Irshadahmed
 */
import {
    Component,
    Input,
    OnInit
} from '@angular/core';
import {
    DateUtilService
} from '../../core/dateUtil.service';
@Component({
    selector: 'time-picker',
    template: `
        <div class="select-box-wrap"><select-box
        [(defaultValue)]="selectedTime.hours" [disabled]="disabled"
        [name]="'hours'"
        (itemSelect)="onTimeChange($event)"
        [items]="hours">
        </select-box></div>
        <span class="time-separator">:</span>
        <div class="select-box-wrap"><select-box
        [(defaultValue)]="selectedTime.min" [disabled]="disabled"
        [name]="'min'"
        (itemSelect)="onTimeChange($event)"
        [items]="minutes"></select-box></div>
        <span class="time-separator">:</span>
        <div class="select-box-wrap"><select-box
        [(defaultValue)]="selectedTime.ampm" [disabled]="disabled"
        [name]="'ampm'"
        (itemSelect)="onTimeChange($event)"
        [items]="ampm">
        </select-box></div>
    `,
    styles: [`
        .select-box-wrap {
            float: left;
            width: 51.2px;
        }
        .time-separator {
            font-weight: bolder;
            font-size: 18px;
            position: relative;
            top: 2px;
            float: left;
            left: 2px;
            margin: 0 4px;
        }
    `]
})
export class TimePickerComponent {
    @Input()
    public tpConfig: any = {};
    @Input()
    public disabled: boolean;
    public hours: string[];
    public minutes: string[];
    public ampm: string[] = ['AM', 'PM'];
    @Input()
    public selectedTime: any;
    constructor(dateUtilSvc: DateUtilService) {
        this.hours = dateUtilSvc.getHours();
        this.minutes = dateUtilSvc.getMinutes();
    }

    public onTimeChange(timeObj: any): void {
        this.selectedTime[timeObj.name] = timeObj.item;
    }
}
