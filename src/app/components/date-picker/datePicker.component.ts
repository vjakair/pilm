/*****************************************************************
 * Proprietary & Confidential  |  © 2017 PhaseZero Ventures LLC  *
 * This is part of PhaseZero Ventures LLC and cannot be copied,  *
 * modified and/or distributed without the express permission of *
 * PhaseZero Ventures LLC                                        *
 *****************************************************************/
/**
 * @author: Irshadahmed
 */
import {
    ChangeDetectorRef,
    Component,
    ElementRef,
    EventEmitter,
    HostListener,
    Input,
    Output,
    ViewChild,
    OnChanges,
    OnDestroy,
    OnInit
} from '@angular/core';
import {
    FormControl
} from '@angular/forms';
import {
    DateUtilService
} from '../../core/dateUtil.service';
import {
    EventEmitterService
} from '../../core/event-emitter';
import {
    SubscriptionHistory
} from '../../helper/subscriptionHistory.helper';
import { BsDatepickerConfig } from 'ngx-bootstrap/datepicker';
import {defineLocale} from 'ngx-bootstrap/bs-moment';
import {es} from 'ngx-bootstrap/locale';
import { AppConfig } from '../../config/app.config';

@Component({
    selector: 'date-picker',
    template: `
    <label for="{{dpName}}"><span translate="{{label}}"></span> *</label>
    <div class="input-group" [ngClass]="{'invalid': isDateInvalid}">
    <input *ngIf="newMinDate" type="text" class="form-control pilm-form-control invisible"
    bsDatepicker #bsDp="bsDatepicker" (bsValueChange)="onDateSelected($event)"
    [placement]="placement"
    (onShown)="onDpShown($event)"
    (onHidden)="onDpHidden()"
    [bsValue]="date"
    [minDate]="newMinDate"
    [maxDate]="newMaxDate"
    [bsConfig]="dpConfig">
    <img src="assets/img/cal.jpg" class="clickable" (click)="!disabled && bsDp.show()" width="13"
    height="14" />
    <input type="text"  class="form-control pilm-form-control"
    placeholder="MM/DD/YYYY"
    #piDp="ngModel"
    [(ngModel)]="selectedDate"
    [disabled]="disabled"
    (click)="bsDp.show()"
    (focus)="showDatepicker()"
    [attr.name]="dpName"
    autocomplete="off"
    (keyup)="onDateChange($event)"
    required></div>
    `,
    styles: [`
        .time-separator {
            font-weight: bolder;
            font-size: 18px;
            position: relative;
            top: 2px;
        }
    `]
})
export class DatePickerComponent extends SubscriptionHistory implements OnChanges, OnDestroy, OnInit {
    @Input()
    public autoSelected: boolean;
    @Input()
    public date: Date;
    @Input()
    public dateInvalid: boolean = false;
    @Input()
    public dpClass: string;
    @Input()
    public dpConfig: any = {};
    @Input()
    public dpName: string;
    @Input()
    public label: string;
    @Input()
    public minDate: Date;
    @Input()
    public maxDate: Date;

    public newMaxDate: Date;
    public newMinDate: Date;
    @Input()
    public disabled: boolean;
    @ViewChild('bsDp')
    public bsDp: any;
    @ViewChild('piDp')
    public piDp: FormControl;
    public placement: string;
    @Input()
    public isPlaceBottom: boolean;
    public selectedDate: string;
    @Output()
    public dateChanged: EventEmitter<Date | boolean> = new EventEmitter();
    public dateDisabled: Date[] = [];
    private _defDpClass: string = 'pilm-dp';
    private _isOpen: boolean = false;
    private _timer: any;
    constructor(
        private _eleRef: ElementRef,
        private _ref: ChangeDetectorRef,
        private _dateUtilSvc: DateUtilService,
        private _eventEmitter: EventEmitterService,
        private _bsDatepickerConfig: BsDatepickerConfig,
        private _appConfig: AppConfig
    ) {
        super();
        this.placement = 'top';
        this._subscribeEvents();
    }
    public ngOnDestroy() {
        this.unsubscribe();
    }
    public ngOnInit() {
        this.placement = this.isPlaceBottom ? 'bottom' : 'top';
        // console.log('isPlacementBottom', this.dpConfig);
        if(this._appConfig.defaultLocaleId) {
            const locale: string = this._appConfig.defaultLocaleId.toLocaleLowerCase();
            if (locale.indexOf('es') > -1) {
                defineLocale(es.abbr, es);
                this._bsDatepickerConfig.locale = 'es';
            }
        }
    }
    public ngOnChanges() {
        if (this.minDate) {
            const day: number = this.minDate.getDay();
            const date: number = this.minDate.getDate();
            const days: number = this._dateUtilSvc.getDaysInMonth(this.minDate);
            if (days === date) {
                this.newMinDate = this._dateUtilSvc.getOtherDate(this.minDate, -1);
            } else {
                this.newMinDate = this.minDate;
            }
            if (this.date && this.minDate > this.date) {
                // this.date = this.minDate;
                this.dateInvalid = true;
            } else if (this.minDate && this.autoSelected) {
                this.date = this.minDate;
            }
        }
        if (this.maxDate) {
            const day: number = this.maxDate.getDay();
            const date: number = this.maxDate.getDate();
            if (1 === date) {
                this.newMaxDate = this._dateUtilSvc.getOtherDate(this.maxDate, 1);
            } else {
                this.newMaxDate = this.maxDate;
            }
            if (this.date && this.maxDate < this.date) {
                // this.date = this.maxDate;
                this.dateInvalid = true;
            }
        }
    }
    get isDateInvalid(): boolean {
        return (
            (this.piDp.invalid || this.dateInvalid) && (this.piDp.dirty || this.piDp.touched)
        );
    }
    @HostListener('document:click', ['$event'])
    public onClick($event) {
        // console.log('----------- this._isOpen=>(%o) ----- ($event.target)-> (%o)', this._isOpen,
        //     this._eleRef.nativeElement.contains($event.target));
        if (!this._isOpen || this._eleRef.nativeElement.contains($event.target)) {
            return;
        }
        setTimeout(() => {
            if (this._isOpen) {
                this.bsDp.hide();
            }
        }, 100);
    }
    public onDateChange($event: Event): void {
        let dateValues: string[];
        try {
            dateValues = this.selectedDate.split('/');
        } catch (e) {
            return;
        }
        if (3 !== dateValues.length || 4 > dateValues[2].length) {
            this.dateInvalid = true;
            this._invalidDate();
            return;
        }
        if (
            !this._dateUtilSvc.isDateFormatCorrect(
                dateValues[1] + '/' + dateValues[0] + '/' + dateValues[2]
            )
        ) {
            this.dateInvalid = true;
            this._invalidDate();
            return;
        }
        const date: Date = new Date(this.selectedDate);
        date.setHours(0, 0, 0, 0);
        if ((this.minDate && date < this.minDate) || (this.maxDate && date > this.maxDate)) {
            this.dateInvalid = true;
            return;
        }
        this.dateInvalid = false;
        this.date = date;
        this.dateChanged.emit(date);
        this._ref.detectChanges();
    }
    public onDpShown($event: any): void {
        this._isOpen = true;
    }
    public onDpHidden($event: any): void {
        this._isOpen = false;
    }
    public onDateSelected(date: Date): void {
        if (!this._dateUtilSvc.isDateValid(date)) {
            this._invalidDate();
            return;
        }
        date.setHours(0, 0, 0, 0);
        const dateStr: string = this._dateUtilSvc.getFormattedDate(date);
        // console.log('*********(%o) and ********** (%o)', dateStr, this.selectedDate);

        if (dateStr === this.selectedDate) {
            return;
        }
        if (this.autoSelected && !this.selectedDate) {
            return;
        }
        // This is hack for handling prev button in datepicker when the selected
        // falls on the last date and day of the week
        if (date < this.minDate) {
            this.date = this.minDate;
            if (!this.bsDp) {
                return;
            }
            this.bsDp._bsValue = this.minDate;
            setTimeout(() => {
                this.bsDp.show();
                // console.log('open 11111111111');
            }, 0);
            return;
        }
        // This is hack for handling next button in datepicker when the selected date
        // falls on the first day of month
        if (this.maxDate < date) {
            if (this._dateUtilSvc.isDateValid(this.date)) {
                this.date = this.maxDate;
                this.bsDp._bsValue = this.maxDate;
            }
            setTimeout(() => {
                this.bsDp.show();
                // console.log('open 22222222222222');
            }, 0);
            return;
        }
        this.dateInvalid = false;
        this.dpConfig.containerClass = 'theme-default';
        // console.log('*********selectedDate **** (%o)', dateStr);
        this.selectedDate = dateStr;
        this.dateChanged.emit(date);
        this._ref.detectChanges();
    }
    public showDatepicker() {
        setTimeout(() => {
            if (!this._isOpen) {
                // console.log('open 333333333333333333');

                this.bsDp.show();
            }
        }, 100);
    }
    private _changeDatePickerOrientaion(placement: string): void {
        if (
            -1 === ['bottom', 'top'].indexOf(placement) ||
            placement === this.placement
        ) {
            return;
        }
        let containerClass: string = this.dpConfig.containerClass;
        containerClass = containerClass.replace('top', '').replace('bottom', '');
        containerClass += ' ' + placement;
        this.dpConfig.containerClass = containerClass;
        this.placement = placement;
        if (this._isOpen) {
            this.bsDp.hide();
            setTimeout(() => {
                // console.log('open 44444444444444444');

                this.bsDp.show();
            }, 50);
        }
    }
    private _invalidDate() {
        if (this._timer) {
            clearTimeout(this._timer);
        }
        this._timer = setTimeout(() => {
            this._timer = undefined;
            if (this.dateInvalid) {
                this.dateChanged.emit(false);
            }
        }, 250);
    }
    private _subscribeEvents() {
        this.subscriptions.push(
            this._eventEmitter
                .onDatePickerOrientationChange
                .subscribe((placement: string) => this._changeDatePickerOrientaion(placement))
        );
    }
}
