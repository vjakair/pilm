import {
    Component,
    Output,
    TemplateRef
} from '@angular/core';

// For bootstrap modal
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/modal-options.class';

// Import modal
import {
    ModalComponent
} from './modal/modal.component';

@Component({
    selector: 'dir-comp',
    templateUrl: './directives.view.html',
    styleUrls: ['./directives.style.scss']
})
export class DirectivesComponent {
    public bsModalRef: BsModalRef;
    public modalRef: BsModalRef;
    constructor(private modalService: BsModalService) { }

    public openModal(template: TemplateRef<any>) {
        this.modalRef = this.modalService.show(template);
    }

    @Output()
    public modalClosed() {
        // console.log('Modal closed');
        this.bsModalRef.hide();
    }

    public openModalWithComponent() {
        const list = ['Open a modal with component', 'Pass your data', 'Do something else', '...'];
        this.bsModalRef = this.modalService.show(ModalComponent);
        this.bsModalRef.content.title = 'Modal with component';
        this.bsModalRef.content.list = list;
        setTimeout(() => {
            list.push('PROFIT!!!');
        }, 2000);
    }
}
