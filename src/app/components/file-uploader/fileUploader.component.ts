/*****************************************************************
 * Proprietary & Confidential  |  © 2017 PhaseZero Ventures LLC  *
 * This is part of PhaseZero Ventures LLC and cannot be copied,  *
 * modified and/or distributed without the express permission of *
 * PhaseZero Ventures LLC                                        *
 *****************************************************************/
/**
 * @author: Irshadahmed
 */
import {
    Component,
    EventEmitter,
    Input,
    OnDestroy,
    OnInit,
    Output,
    ViewChild
} from '@angular/core';
import {
    TranslationService
} from '../../core/translation.service';
import {
    CommonUtilService
} from '../../core/commonUtil.service';
import {
    EventEmitterService
} from '../../core/event-emitter';
import {
    SubscriptionHistory
} from '../../helper/subscriptionHistory.helper';
@Component({
    selector: 'file-uploader',
    templateUrl: './fileUploader.component.html',
    styleUrls: ['./fileUploader.style.scss']
})
export class FileUploaderComponent extends SubscriptionHistory implements OnDestroy, OnInit {
    @Input()
    public accept: string;
    public errorMsg: string;
    @ViewChild('fileInput')
    public fileInput: any;
    @Input()
    public fileName: string;
    @Input()
    public hasEditableField: boolean = true;
    public fileSelected: boolean = false;
    public labelText: string;
    @Input()
    public placeholder: string;
    @Input()
    public options: any = {};
    @Input()
    public isCsvUpload: boolean;
    @Output()
    public uploadFile: EventEmitter<any> = new EventEmitter();
    @Output()
    public cancelUpload: EventEmitter<string> = new EventEmitter();
    @ViewChild('tooltip')
    public tooltip: any;
    constructor(
        private _commonUtil: CommonUtilService,
        private _translate: TranslationService,
        private _eventEmitter: EventEmitterService
    ) {
        super();
        this._subscribeEvents();
    }
    public ngOnInit() {
        const label: string = this.options.label;
        if (!this._commonUtil.isValidString(label)) {
            return;
        }
        this._translate.getTranslation(label).subscribe((labelText: string) => {
            this.labelText = labelText;
        });
    }
    public ngOnDestroy() {
        this.unsubscribe();
    }
    public onFileSelect($event) {
        const file: File = $event.target.files[0];
        const fileType: string = file.type;
        if (this._isFileInValid(fileType)) {
            const errMsg: string = 'image/*' === this.accept ?
                'FILE.ERRORS.IMG_INVALID' : 'FILE.ERRORS.PDF_INVALID';
            this._showErrorMsg(errMsg);
            return;
        }
        if ('image/*' !== this.accept && '.csv' !== this.accept) {
            this._setFileSelected(file);
            return;
        }
        if ('.csv' === this.accept) {
            this.uploadFile.emit({
                label: this.options.label,
                type: this.options.type,
                data: file
            });
            this.fileInput.nativeElement.value = '';
            this.fileName = undefined;
            return;
        }
        const fileSize: number = parseFloat((file.size / 1048576).toFixed(2));
        const imgWidth: string = this.options.w;
        const imgHeight: string = this.options.h;
        if (1 < fileSize) {
            this._showErrorMsg('FILE.ERRORS.IMG_SIZE', {
                size: '1Mb'
            });
            return;
        }
        const img: any = new Image();
        const imgUrl: any = window.URL;
        if (this._commonUtil.isDefined(imgUrl.createObjectURL)) {
            const _self = this;
            img.src = imgUrl.createObjectURL(file);
            img.onload = function () {
                if (imgWidth !== this.width + 'px' || imgHeight !== this.height + 'px') {
                    _self._showErrorMsg('FILE.ERRORS.IMG_DIM_' + _self.options.type.toUpperCase(), {
                        w: imgWidth,
                        h: imgHeight
                    }, _self.labelText);
                    _self.fileInput.nativeElement.value = '';
                    _self.fileName = undefined;
                    return;
                }
                _self._setFileSelected(file);
            };
        } else {
            this._setFileSelected(file);
        }
    }
    public onFileRemove($event: Event): void {
        $event.stopPropagation();
        this.fileInput.nativeElement.value = '';
        this.fileName = undefined;
        this.cancelUpload.emit(this.options.label);
    }
    private _displayServerError(error: any): void {
        if (this.options.type !== error.fileType) {
            return;
        }
        if (error.msgKey) {
            this._showErrorMsg(error.msgKey);
        } else {
            this._showErrorMsg('FILE.ERRORS.SERVER', {
                fileName: this.labelText
            });
        }
        setTimeout(() => {
            this.fileInput.nativeElement.value = '';
            this.fileName = undefined;
        }, 5000);
    }
    private _isFileInValid(fileType: string): boolean {
        if ('application/pdf' === this.accept) {
            if ('application/pdf' !== fileType) {
                return true;
            }
        } else if ('image/*' === this.accept) {
            const type: string = fileType.substr(0, fileType.lastIndexOf('/'));
            if ('image' !== type) {
                return true;
            }
        }
        return false;
    }
    private _setFileSelected(file: File): void {
        const fileName: string = file.name;
        this.fileName = fileName;
        this.uploadFile.emit({
            label: this.options.label,
            type: this.options.type,
            data: file
        });
    }
    private _showErrorMsg(msgKey: string, params?: any, prefix: string = ''): void {
        this._translate.getTranslation(msgKey, params).subscribe((errorMsg: string) => {
            this.errorMsg = prefix + errorMsg;
            this._commonUtil.showTooltip(this.tooltip);
        });
    }
    private _subscribeEvents(): void {
        // Check if file uploading fails
        this.subscriptions.push(
            this._eventEmitter
                .onServerError
                .subscribe((fileType: string) => this._displayServerError(fileType))
        );
    }
}
