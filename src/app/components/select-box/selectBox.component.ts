/*****************************************************************
 * Proprietary & Confidential  |  © 2017 PhaseZero Ventures LLC  *
 * This is part of PhaseZero Ventures LLC and cannot be copied,  *
 * modified and/or distributed without the express permission of *
 * PhaseZero Ventures LLC                                        *
 *****************************************************************/
/**
 * @author: Irshadahmed
 */
import {
    Component,
    ElementRef,
    EventEmitter,
    HostListener,
    Input,
    OnChanges,
    OnInit,
    Output
} from '@angular/core';
import {
    CommonUtilService
} from '../../core/commonUtil.service';
import {
    FilterPipe
} from '../../core/filter.pipe';
/**
 * [ngClass]="{'pilm-form-control': 'Select' === selectedText}"
 */
@Component({
    selector: 'select-box',
    templateUrl: './selectBox.component.html',
    styleUrls: [
        './selectBox.style.scss'
    ]
})
export class SelectBoxComponent implements OnInit, OnChanges {
    public item: any;
    @Input()
    public items: any[];
    @Output()
    public itemSelect: EventEmitter<any> = new EventEmitter();
    @Input()
    public key: string;
    @Input()
    public defaultValue: string;
    @Input()
    public btnClass: string;
    @Input()
    public disabled: boolean;
    public searchTerm: string = '';
    public isOpen: boolean;
    @Input()
    public isDropup: boolean;
    @Input()
    public isFilter: boolean = false;
    @Input()
    public isMultiSelect: boolean;
    @Input()
    public name: string;
    @Output()
    public onDpToggle: EventEmitter<boolean> = new EventEmitter();
    @Input()
    public placeholder: string;
    public selectedCount: number;
    public selectedText: string;
    // public tempSelectedItems:any[] = [];
    private _filterPipe: FilterPipe;
    constructor(
        private _commonUtil: CommonUtilService,
        private _eleRef: ElementRef
    ) {
        this._filterPipe = new FilterPipe();
    }
    public ngOnChanges() {
        if (this._commonUtil.isValidArray(this.items)) {
            this._setDefaultSelectedItems();
        } else {
            this.selectedText = this.defaultValue;
            this.selectedCount = 0;
        }
    }
    public ngOnInit() {
        if (!this.btnClass) {
            this.btnClass = 'btn-default';
        }
        this._setDefaultSelectedItems();
    }
    /* public cancelSelection(keepOpen?:boolean):void {
        if (!this._commonUtil.isValidArray(this.tempSelectedItems)) {
            return;
        }
        this.tempSelectedItems.forEach((item:any) => {
            this.selectItem(item);
        });
        this.tempSelectedItems = [];
        if (!keepOpen) {
            this.isOpen = false;
        }
        this.searchTerm = '';
    } */
    public commitSelection() {
        // this.tempSelectedItems = [];
        this.isOpen = false;
        this.searchTerm = '';
    }
    public deSelectAll(): void {
        const selectedItems: any[] = this._getSelectedItems();
        selectedItems.forEach((item: any): void => {
            if (!item.selected) {
                return;
            }
            this.selectItem(item);
        });
    }
    @HostListener('document:click', ['$event'])
    public onClick($event) {
        if (this._eleRef.nativeElement.contains($event.target)) {
            return;
        }
        this.isOpen = false;
    }
    public onShown(): void {
        this.onDpToggle.emit(true);
    }
    public resetSearchTerm($event): void {
        $event.preventDefault();
        $event.stopPropagation();
        this.searchTerm = '';
        document.getElementById('filter').focus();
    }
    public selectItem(item: any): void {
        if (this.isMultiSelect) {
            // this.tempSelectedItems.push(item);
            item.selected = !item.selected;
            this.selectedText = this._getSelectedText();
        } else {
            this.selectedCount = 1;
            if (this.key) {
                const prevSelectedItem: any = this._commonUtil.getFilteredItem(
                    this.items,
                    'selected',
                    true
                );
                if (this._commonUtil.isObject(prevSelectedItem)) {
                    prevSelectedItem.selected = false;
                }
                item.selected = true;
                this.selectedText = item[this.key];
            } else {
                this.selectedText = item;
            }
            this.isOpen = false;
            this.searchTerm = '';
        }
        this._onItemSelect(item, 'click');
    }
    public selectAll(): void {
        const selectedItems: any[] = this._getSelectedItems();
        selectedItems.forEach((item: any): void => {
            if (item.selected) {
                return;
            }
            this.selectItem(item);
        });
    }

    private _onItemSelect(selectedItem: any, itemType?: string): void {
        this.itemSelect.emit({
            item: selectedItem,
            name: this.name,
            selectedCount: this.selectedCount,
            type: itemType
        });
    }
    private _doGetSelectedText(selectedItems: any): string {
        this.selectedCount = selectedItems.length;
        let selectedText = '';
        selectedItems.forEach((item: any) => {
            const value = item[this.key];
            selectedText += '' === selectedText ? value
                : ', ' + value;
        });
        return selectedText;
    }
    private _getSelectedItems(): any[] {
        let selectedItems: any[];
        if (this._commonUtil.isValidString(this.searchTerm)) {
            selectedItems = this._filterPipe
                .transform(
                this.items,
                'name',
                this.searchTerm
                );
        } else {
            selectedItems = this.items;
        }
        return selectedItems;
    }
    private _getSelectedText(): string {
        const selectedItems: any[] = this._commonUtil.getSelectedItems(this.items);
        this.selectedCount = selectedItems.length;
        if (this.selectedCount) {
            if (this.isMultiSelect) {
                return this._doGetSelectedText(selectedItems);
            } else {
                const selectedItem: any = selectedItems[0];
                return this._commonUtil.isValidString(this.key) ?
                    selectedItem[this.key] : selectedItem;
            }
        } else {
            return this.defaultValue;
        }
    }
    private _setDefaultSelectedItems(): void {
        if (
            !this._commonUtil.isValidArray(this.items) ||
            !this._commonUtil.isObject(this.items[0])
        ) {
            this.selectedText = this.defaultValue;
            return;
        }
        const selectedItems: any[] = this._commonUtil.getSelectedItems(this.items);
        this.selectedCount = selectedItems.length;
        this.selectedText = this._getSelectedText();
    }
}
