/*****************************************************************
 * Proprietary & Confidential  |  © 2017 PhaseZero Ventures LLC  *
 * This is part of PhaseZero Ventures LLC and cannot be copied,  *
 * modified and/or distributed without the express permission of *
 * PhaseZero Ventures LLC                                        *
 *****************************************************************/
/**
 * @author: Irshadahmed
 */
import {
    AfterViewInit,
    Component,
    ElementRef,
    Input,
    OnDestroy
} from '@angular/core';
import {
    Router
} from '@angular/router';
import {
    BsModalService
} from 'ngx-bootstrap/modal';
import {
    BsModalRef
} from 'ngx-bootstrap/modal/modal-options.class';
import {
    AuthService
} from '../../auth/auth.service';
import {
    AdminConsoleModalComponent
} from '../../promotions/admin-console-modal/adminConsoleModal.component';
import {
    CommonUtilService
} from '../../core/commonUtil.service';
import {
    EventEmitterService
} from '../../core/event-emitter';
import {
    PromotionService
} from '../../promotions/promotions.service';
import {
    SubscriptionHistory
} from '../../helper/subscriptionHistory.helper';
import {
    NavigationService
} from '../../core/navigation.service';
import { AppConfig } from '../../config/app.config';

@Component({
    selector: 'header',
    styleUrls: ['./header.style.scss'],
    templateUrl: './header.component.html'
})
export class HeaderComponent extends SubscriptionHistory implements AfterViewInit, OnDestroy {
    @Input()
    public isLoggedIn: boolean;
    public isCollapsed: boolean;
    public logoUrl: string;
    constructor(
        private _authService: AuthService,
        public bsModalSvc: BsModalService,
        private _commonUtil: CommonUtilService,
        private _eventEmitter: EventEmitterService,
        private _el: ElementRef,
        private _navigation: NavigationService,
        private _promoSvc: PromotionService,
        private _router: Router,
        private _appConfig: AppConfig
    ) {
        super();
        this._subscribeEvents();
        this.logoUrl = this._appConfig.getConfig('appInfo').applicationConfig.logoImageLocation;
        // console.log(this._appConfig.getConfig('appInfo').applicationConfig.logoImageLocation)
    }
    public ngAfterViewInit() {
        setTimeout(() => {
            this._commonUtil.headerHeight =
                this._el.nativeElement.offsetHeight;
            this.isCollapsed = true;
        }, 10);
    }
    public ngOnDestroy() {
        this.unsubscribe();
    }
    public goToHome(): void {
        this._navigation
            .navigate('/');
    }
    public logout() {
        const jwtLogin: boolean = this._authService.jwt;
        if (jwtLogin) {
            try {
                window.self.close();
            } catch (e) {
                console.log(e);
            }
            return;
        }
        this._authService.invalidateSession().then(() => {
            this._navigation.navigate('/signin');
        });
    }
    public showAdminConsole(): void {
        this._promoSvc
            .getApproverGroupData()
            .subscribe((approverGroups: any[]): void => {
                const adminConsoleModal: BsModalRef = this.bsModalSvc
                    .show(AdminConsoleModalComponent, {
                        class: 'pilm-popup admin-console-modal'
                    });
                adminConsoleModal.content.approverGroups = approverGroups;
            }, (error: any): void => {
                console.log(error);
                this._eventEmitter
                    .onAlert
                    .emit({
                        type: 'alert-warning',
                        msgKey: 'PROMOTION.ERROR.SERVER'
                    });
            });
    }
    private _subscribeEvents() {
        this.subscriptions.push(
            this._eventEmitter.onSessionExpired.subscribe(
                () => {
                    this._authService.redirectionUrl = this._router.url;
                    this.logout();
                }
            )
        );
    }
}
