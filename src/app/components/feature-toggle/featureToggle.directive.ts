/*****************************************************************
 * Proprietary & Confidential  |  © 2017 PhaseZero Ventures LLC  *
 * This is part of PhaseZero Ventures LLC and cannot be copied,  *
 * modified and/or distributed without the express permission of *
 * PhaseZero Ventures LLC                                        *
 *****************************************************************/
/**
 * @author: Irshadahmed
 */
import {
    Directive,
    ElementRef,
    Renderer2,
    Input,
    OnInit
} from '@angular/core';
import {
    AuthService
} from '../../auth/auth.service';

@Directive({
    selector: '[featureToggle]'
})
export class FeatureToggleDirective implements OnInit {
    @Input()
    public featureToggle: boolean;
    @Input()
    public permission: string;
    constructor(
        private _authSvc: AuthService,
        private _el: ElementRef,
        private _renderer: Renderer2
    ) { }

    public ngOnInit() {
        if (this._authSvc.userHasPermissions(this.permission)) {
            return;
        }
        if (this.featureToggle) {
            this._el.nativeElement.remove();
        } else {
            this._renderer.addClass(this._el.nativeElement, 'disabled');
        }
    }
}
