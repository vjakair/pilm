/*****************************************************************
 * Proprietary & Confidential  |  © 2017 PhaseZero Ventures LLC  *
 * This is part of PhaseZero Ventures LLC and cannot be copied,  *
 * modified and/or distributed without the express permission of *
 * PhaseZero Ventures LLC                                        *
 *****************************************************************/
/**
 * @author: Irshadahmed
 */
import {
    AfterViewInit,
    Directive,
    ElementRef,
    HostListener,
    Input
} from '@angular/core';

import {
    CommonUtilService
} from '../../core/commonUtil.service';
import {
    EventEmitterService
} from '../../core/event-emitter';

@Directive({
    selector: '[resizeContainer]'
})
export class ResizeContainerDirective implements AfterViewInit {
    @Input()
    public mainRef: boolean;
    public delta: number;
    private _winRef: any;
    private _winHeight: number = null;
    constructor(
        private _commonUtil: CommonUtilService,
        private _el: ElementRef,
        private _eventEmitter: EventEmitterService
    ) { }
    public ngAfterViewInit() {
        this._commonUtil.onWindowResize.subscribe(() => {
            this.resizeContainer();
        });
        this._eventEmitter.onNavigationSuccess.subscribe((event) => {
            this.resizeContainer();
        });
    }
    @HostListener('window:resize', ['$event'])
    public onResize(event) {
        this.resizeContainer();
    }
    public resizeContainer() {
        setTimeout(() => {
            this._setWinHieght();
            this._doResizeContainer();
        }, 100);
    }
    private _setWinHieght() {
        this._winRef = this.mainRef ?
            document.getElementsByTagName('main')[0] : null;
        if (null !== this._winRef) {
            const delta: number = this._winRef.scrollHeight - this._winRef.clientHeight;
            console.log('delta', delta);
            this._winHeight = this._winRef.clientHeight;
        } else {
            this._winHeight = window.innerHeight;
        }
    }
    private _doResizeContainer() {
        const offsetTop = this._el.nativeElement.offsetTop;
        this._el.nativeElement.style.height =
            (this._winHeight - (offsetTop +
                this._commonUtil.footerHeight +
                (isNaN(this.delta) ? 3 : Number(this.delta)))) + 'px';
    }
}
