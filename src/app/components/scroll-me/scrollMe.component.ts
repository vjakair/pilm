/*****************************************************************
 * Proprietary & Confidential  |  © 2017 PhaseZero Ventures LLC  *
 * This is part of PhaseZero Ventures LLC and cannot be copied,  *
 * modified and/or distributed without the express permission of *
 * PhaseZero Ventures LLC                                        *
 *****************************************************************/
/**
 * @author: Irshadahmed
 */
import {
    Directive,
    ElementRef,
    OnDestroy
} from '@angular/core';

import {
    EventEmitterService
} from '../../core/event-emitter';
import {
    SubscriptionHistory
} from '../../helper/subscriptionHistory.helper';
@Directive({
    selector: '[scrollMe]'
})
export class ScrollMeDirective extends SubscriptionHistory implements OnDestroy {
    constructor(
        private _el: ElementRef,
        private _eventEmitter: EventEmitterService
    ) {
        super();
        this._subscribeEvents();
    }
    public ngOnDestroy() {
        this.unsubscribe();
    }
    private _subscribeEvents() {
        this.subscriptions.push(
            this._eventEmitter
                .onPageScroll
                .subscribe((scroll: boolean) => {
                    setTimeout(() => {
                        this._el.nativeElement.scrollTop = this._el.nativeElement.scrollHeight;
                    }, 50);
                })
        );
    }
}
