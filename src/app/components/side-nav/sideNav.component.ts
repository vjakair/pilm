/*****************************************************************
 * Proprietary & Confidential  |  © 2017 PhaseZero Ventures LLC  *
 * This is part of PhaseZero Ventures LLC and cannot be copied,  *
 * modified and/or distributed without the express permission of *
 * PhaseZero Ventures LLC                                        *
 *****************************************************************/
/**
 * @author: Irshadahmed
 */
import {
    Component,
    Input
} from '@angular/core';

@Component({
    selector: 'side-nav',
    templateUrl: './sideNav.component.html',
    styleUrls: ['./sideNav.style.scss']
})
export class SideNavComponent {
    @Input()
    public isLoggedIn: boolean;
}
