/*****************************************************************
 * Proprietary & Confidential  |  © 2017 PhaseZero Ventures LLC  *
 * This is part of PhaseZero Ventures LLC and cannot be copied,  *
 * modified and/or distributed without the express permission of *
 * PhaseZero Ventures LLC                                        *
 *****************************************************************/
/**
 * @author: Irshadahmed
 */
import {
    Component,
    ViewEncapsulation
} from '@angular/core';
import {
    BsModalRef
} from 'ngx-bootstrap/modal/modal-options.class';
import {
    CommonUtilService
} from '../../core/commonUtil.service';
@Component({
    selector: 'preview-images',
    templateUrl: './previewImages.component.html',
    styleUrls: [
        './previewImages.style.scss'
    ],
    encapsulation: ViewEncapsulation.None
})
export class PreviewImagesComponent {
    public imageGroups: any[];
    public carouselImages: any[];
    public loadingImage: boolean;
    private _imgLoaded: boolean;
    constructor(
        public bsModalRef: BsModalRef,
        private _commonUtilSvc: CommonUtilService
    ) {
    }

    public setImagePreview(imageGroups: any[], selectedImgGroup: any): void {
        this.imageGroups = imageGroups;
        this.selectImageGroup(selectedImgGroup);
    }

    public onImageLoad(): void {
        this.loadingImage = false;
        this._imgLoaded = true;
    }

    public selectImageGroup(imageGroup): void {
        if (!this._commonUtilSvc.isValidArray(this.imageGroups)) {
            return;
        }
        this._imgLoaded = false;
        if (!this._commonUtilSvc.isObject(imageGroup)) {
            imageGroup = this.imageGroups[0];
        }
        setTimeout(() => {
            if (!this._imgLoaded) {
                this.loadingImage = true;
            }
        }, 250);
        this.imageGroups.forEach((imgGroup: any) => {
            if (imgGroup.label === imageGroup.label) {
                const carouselImages = this._commonUtilSvc.deepCopy(this.carouselImages);
                if (this._commonUtilSvc.isValidArray(carouselImages) &&
                    this._commonUtilSvc.isValidArray(imgGroup.images) &&
                    carouselImages[0] === imgGroup.images[0]) {
                    imgGroup.selected = true;
                    setTimeout(() => {
                        if (this.loadingImage) {
                            this.loadingImage = false;
                        }
                    }, 300);
                    return;
                }
                imgGroup.selected = true;
                this.carouselImages = imgGroup.images;
            } else {
                imgGroup.selected = false;
            }
        });
    }
}
