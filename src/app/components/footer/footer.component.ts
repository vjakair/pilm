/*****************************************************************
 * Proprietary & Confidential  |  © 2017 PhaseZero Ventures LLC  *
 * This is part of PhaseZero Ventures LLC and cannot be copied,  *
 * modified and/or distributed without the express permission of *
 * PhaseZero Ventures LLC                                        *
 *****************************************************************/
/**
 * @author: Irshadahmed
 */
import {
    AfterViewInit,
    Component,
    ElementRef,
    EventEmitter,
    OnDestroy,
    ViewChild
} from '@angular/core';
import { Router } from '@angular/router';

import { CommonUtilService } from '../../core/commonUtil.service';

@Component({
    selector: 'footer',
    styleUrls: ['./footer.style.scss'],
    templateUrl: './footer.component.html'
})
export class FooterComponent implements AfterViewInit, OnDestroy {
    public onFooterLoaded: EventEmitter<boolean> = new EventEmitter();
    constructor(private _commonUtil: CommonUtilService, private _el: ElementRef) { }
    public ngAfterViewInit() {
        setTimeout(() => {
            this._commonUtil.footerHeight =
                this._el.nativeElement.offsetHeight;
        }, 10);
    }
    public ngOnDestroy() {
        // console.log('Do something on destroy.');
    }
}
