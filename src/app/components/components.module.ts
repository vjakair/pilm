/*****************************************************************
 * Proprietary & Confidential  |  © 2017 PhaseZero Ventures LLC  *
 * This is part of PhaseZero Ventures LLC and cannot be copied,  *
 * modified and/or distributed without the express permission of *
 * PhaseZero Ventures LLC                                        *
 *****************************************************************/
/**
 * @author: Irshadahmed
 */
/**
 * Standard Module imports
 */
import {
    CommonModule
} from '@angular/common';
import {
    FormsModule
} from '@angular/forms';
import {
    NgModule
} from '@angular/core';
import {
    RouterModule
} from '@angular/router';

/**
 * Vendor Module imports
 */
// Bootstrap Datepicker
import { 
    CarouselModule 
} from 'ngx-bootstrap/carousel';
import {
    BsDatepickerModule
} from 'ngx-bootstrap/datepicker';
// Bootstrap dropdown
import {
    BsDropdownModule
} from 'ngx-bootstrap/dropdown';
import {
    CollapseModule
} from 'ngx-bootstrap/collapse';
import {
    ModalModule
} from 'ngx-bootstrap/modal';
import {
    TooltipModule
} from 'ngx-bootstrap/tooltip';
import {
    PaginationModule
} from 'ngx-bootstrap/pagination';
// Tranlsate module
import {
    TranslateModule
} from '@ngx-translate/core';

import {
    CoreModule
} from '../core/core.module';

/**
 * Custom Components imports
 */
// Alert Component
import {
    AlertComponent
} from './alert/alert.component';
// Date picker component
import {
    DatePickerComponent
} from './date-picker/datePicker.component';
// Footer Component
import {
    DirectivesComponent
} from './directives.component';
import {
    FeatureToggleDirective
} from './feature-toggle/featureToggle.directive';
import {
    FileUploaderComponent
} from './file-uploader/fileUploader.component';
import {
    FooterComponent
} from './footer/footer.component';
// Header component
import {
    HeaderComponent
} from './header/header.component';
// HTML Editor
import {
    HtmlEditorComponent
} from './html-editor/htmlEditor.component';
// Loading Spinner
import {
    LoadingSpinnerComponent
} from './loading-spinner/loadingSpinner.component';
// Import Modal
import {
    ModalComponent
} from './modal/modal.component';
// Import Navbar Fixed Bottom
import {
    NavbarFixedBottomComponent
} from './navbar-fixed-bottom/navbarFixedBottom.component';
// Preview images component
import {
    PreviewImagesComponent
} from './preview-images/previewImages.component';
// Resize Directive
import {
    ResizeContainerDirective
} from './resize-container/resizeContainer.directive';
// Scroll Me
import {
    ScrollMeDirective
} from './scroll-me/scrollMe.component';
// Select box
import {
    SelectBoxComponent
} from './select-box/selectBox.component';
// Side Nav Component
import {
    SideNavComponent
} from './side-nav/sideNav.component';
import {
    TimePickerComponent
} from './time-picker/timePicker.component';
import {
    ReportModalComponent
} from '../promotions/reports/report-modal/reportModal.component';

@NgModule({
    declarations: [
        /**
         * Components / Directives/ Pipes
         */
        AlertComponent,
        DatePickerComponent,
        DirectivesComponent,
        FeatureToggleDirective,
        FileUploaderComponent,
        FooterComponent,
        HeaderComponent,
        HtmlEditorComponent,
        LoadingSpinnerComponent,
        ModalComponent,
        NavbarFixedBottomComponent,
        PreviewImagesComponent,
        ResizeContainerDirective,
        ScrollMeDirective,
        SelectBoxComponent,
        SideNavComponent,
        TimePickerComponent,
        ReportModalComponent
    ],
    imports: [
        BsDatepickerModule.forRoot(),
        BsDropdownModule.forRoot(),
        CarouselModule.forRoot(),
        CollapseModule.forRoot(),
        CommonModule,
        CoreModule,
        FormsModule,
        ModalModule.forRoot(),
        PaginationModule.forRoot(),
        RouterModule,
        TooltipModule.forRoot(),
        TranslateModule
    ],
    entryComponents: [
        ModalComponent,
        PreviewImagesComponent,
        ReportModalComponent
    ],
    exports: [
        AlertComponent,
        DatePickerComponent,
        FeatureToggleDirective,
        FileUploaderComponent,
        FooterComponent,
        HeaderComponent,
        HtmlEditorComponent,
        LoadingSpinnerComponent,
        NavbarFixedBottomComponent,
        PreviewImagesComponent,
        ResizeContainerDirective,
        ScrollMeDirective,
        SelectBoxComponent,
        SideNavComponent,
        TimePickerComponent,
        ReportModalComponent
    ]
})
export class ComponentsModule {
}
