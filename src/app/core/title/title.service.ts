/*****************************************************************
 * Proprietary & Confidential  |  © 2017 PhaseZero Ventures LLC  *
 * This is part of PhaseZero Ventures LLC and cannot be copied,  *
 * modified and/or distributed without the express permission of *
 * PhaseZero Ventures LLC                                        *
 *****************************************************************/
/**
 * @author: Irshadahmed
 */
import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Title } from '@angular/platform-browser';
import {
    TranslationService
} from '../translation.service';

@Injectable()
export class TitleService {

    public value = 'Angular 2';

    constructor(
        public http: Http,
        private _title: Title,
        private _translationSvc: TranslationService
    ) { }

    public getData() {
        console.log('Title#getData(): Get Data');
        /**
         * return this.http.get('/assets/data.json')
         * .map(res => res.json());
         */
        return {
            value: 'AngularClass'
        };
    }

    public setTitle(title) {
        if (!title) {
            return;
        }
        title = title + '.TITLE';
        this._translationSvc.getTranslation(title).subscribe((pageTitle) => {
            this._title.setTitle(pageTitle);
        });
    }

}
