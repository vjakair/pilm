/*****************************************************************
 * Proprietary & Confidential  |  © 2017 PhaseZero Ventures LLC  *
 * This is part of PhaseZero Ventures LLC and cannot be copied,  *
 * modified and/or distributed without the express permission of *
 * PhaseZero Ventures LLC                                        *
 *****************************************************************/
/**
 * @author: Irshadahmed
 */
import {
    Pipe,
    PipeTransform
} from '@angular/core';
@Pipe({
    name: 'myFilter'
})
export class FilterPipe implements PipeTransform {
    public transform(items: any[], field: string, value: string): any[] {
        if (!items || !value || '' === value) {
            return items;
        }
        return items.filter((item: any) => {
            const itemVal = item[field].toLowerCase();
            return -1 !== itemVal.indexOf(value.toLowerCase());
        });
    }
}
