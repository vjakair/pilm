/*****************************************************************
 * Proprietary & Confidential  |  © 2017 PhaseZero Ventures LLC  *
 * This is part of PhaseZero Ventures LLC and cannot be copied,  *
 * modified and/or distributed without the express permission of *
 * PhaseZero Ventures LLC                                        *
 *****************************************************************/
/**
 * @author: Irshadahmed
 */
import {
    EventEmitter,
    Injectable
} from '@angular/core';
import {
    NavigationEnd
} from '@angular/router';

@Injectable()
export class EventEmitterService {
    public onAlert: EventEmitter<any> = new EventEmitter();
    public onApproverGroupChange: EventEmitter<void> = new EventEmitter();
    public onDatePickerOrientationChange: EventEmitter<string> = new EventEmitter();
    public onLoaderDisabled: EventEmitter<boolean> = new EventEmitter();
    public onLoadingMsg: EventEmitter<string> = new EventEmitter();
    public onServerError: EventEmitter<any> = new EventEmitter();
    public onToggleActions: EventEmitter<boolean> = new EventEmitter();
    public onLoaderToggle: EventEmitter<boolean> = new EventEmitter();
    public onNavigationSuccess: EventEmitter<NavigationEnd> = new EventEmitter();
    public onOverlayLoaded: EventEmitter<boolean> = new EventEmitter();
    public onPageScroll: EventEmitter<boolean> = new EventEmitter();
    public onPromoSave: EventEmitter<any> = new EventEmitter();
    public onPromoSelected: EventEmitter<any> = new EventEmitter();
    public onSessionExpired: EventEmitter<any> = new EventEmitter();
    public onUserLoginStateChange: EventEmitter<boolean> = new EventEmitter();
    public onValidatePromoDetails: EventEmitter<boolean> = new EventEmitter();
    public onDownloadReport: EventEmitter<any> = new EventEmitter();
}
