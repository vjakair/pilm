/*****************************************************************
 * Proprietary & Confidential  |  © 2017 PhaseZero Ventures LLC  *
 * This is part of PhaseZero Ventures LLC and cannot be copied,  *
 * modified and/or distributed without the express permission of *
 * PhaseZero Ventures LLC                                        *
 *****************************************************************/
/**
 * @author: Irshadahmed
 */
import {
    trigger,
    state,
    animate,
    transition,
    style
} from '@angular/animations';

export const fadeInAnimation =
    trigger('fadeInAnimation', [
        state('*', style({
            // the view covers the whole screen with a semi tranparent background
            position: 'absolute',
            top: 0,
            left: 0,
            right: 0,
            bottom: 0,
            backgroundColor: 'rgba(0, 0, 0, 0.8)',
            zIndex: 1
        })),
        // route 'enter' transition
        transition(':enter', [

            // styles at start of transition
            style({ opacity: .5 }),

            // animation and styles at end of transition
            animate('.8s', style({ opacity: 1 }))
        ]),
    ]);
