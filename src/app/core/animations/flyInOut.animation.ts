/*****************************************************************
 * Proprietary & Confidential  |  © 2017 PhaseZero Ventures LLC  *
 * This is part of PhaseZero Ventures LLC and cannot be copied,  *
 * modified and/or distributed without the express permission of *
 * PhaseZero Ventures LLC                                        *
 *****************************************************************/
/**
 * @author: Irshadahmed
 */
import {
    trigger,
    state,
    animate,
    transition,
    style
} from '@angular/animations';

export const flyInOutAnimation = trigger('flyInOutAnimation', [
    state('void', style({
        position: 'absolute',
        width: '100%',
        height: '100%'
    })),
    state('*', style({
        position: 'absolute',
        width: '100%',
        height: '100%'
    })),
    transition('void => *', [
        style({ transform: 'translateX(-100%)' }),
        animate('0.5s ease-in-out', style({ transform: 'translateX(0%)' }))
    ]),
    transition('* => void', [
        style({ transform: 'translateX(0%)' }),
        animate('0.5s ease-in-out', style({ transform: 'translateX(100%)' }))
    ])
]);
