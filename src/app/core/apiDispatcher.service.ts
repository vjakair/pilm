/*****************************************************************
 * Proprietary & Confidential  |  © 2016 PhaseZero Ventures LLC  *
 * This is part of PhaseZero Ventures LLC and cannot be copied,  *
 * modified and/or distributed without the express permission of *
 * PhaseZero Ventures LLC                                        *
 *****************************************************************/
/**
 * @author: Irshadahmed
 */
// Import Core Services
import {
    Injectable
} from '@angular/core';
import {
    Headers,
    Http,
    RequestMethod,
    RequestOptions,
    Response,
    ResponseOptions
} from '@angular/http';
import {
    Observable
} from 'rxjs/Rx';

// Import Custom Services
import {
    AppConfig
} from '../config/app.config';
import {
    ApiUtilService
} from './apiUtil.service';
import {
    CacheFactoryService
} from './cacheFactory.service';
import {
    CommonUtilService
} from './commonUtil.service';

@Injectable()
export class ApiDispatcherService extends ApiUtilService {
    private readonly _successCodes: number[] = [
        200,
        201,
        202,
        203,
        204,
        205
    ];
    constructor(
        appConfig: AppConfig,
        commonUtil: CommonUtilService,
        private _http: Http,
        private _cacheFactory: CacheFactoryService
    ) {
        super(appConfig, commonUtil);
    }
    /**
     * Performs a request with `delete` http method.
     * @param apiKey
     * @param routeParams
     * @param params
     * @param headers
     */
    public doDeleteApiCall(
        apiKey: string,
        routeParams?: any,
        params?: any,
        headers?: any,
        endPoint?: string
    ): Observable<Response> {
        const apiPath: string = this.getApiPath(apiKey, routeParams, endPoint);
        const requestOptions: RequestOptions = this._getReqOptions(headers, true);
        requestOptions.params = params;
        return this._http.delete(apiPath, requestOptions);
    }
    /**
     * Performs a request with `get` http method.
     * @param apiKey
     * @param routeParams
     * @param params
     * @param headers  Observable<Response>
     */
    public doGetApiCall(
        apiKey: string,
        routeParams?: any,
        params?: any,
        cache?: boolean,
        headers?: any,
        endPoint?: string
    ): Observable<any> {
        const apiPath: string = this.getApiPath(apiKey, routeParams, endPoint);
        const requestOptions: RequestOptions = this._getReqOptions(headers, true);
        let isResolved: boolean = false;
        requestOptions.params = params;
        if (cache) {
            const cachedData: any = this._cacheFactory.getItem(apiPath);
            if (this.commonUtil.isObject(cachedData)) {
                isResolved = true;
                return Observable.of(cachedData);
            }
        }
        if (!isResolved) {
            let errorOccured: boolean = false;
            return this._http.get(apiPath, requestOptions)
                .map((res) => res.json())
                .catch((error: any): any => {
                    if (error.status === 401) {
                        console.log('Refreshing token...');
                    } else {
                        errorOccured = true;
                        Observable.throw(error);
                    }
                })
                .do((response: any) => {
                    if (cache && !errorOccured && this.commonUtil.isObject(response) && !response.errorCode) {
                        this._cacheFactory.setItem(apiPath, response);
                    }
                })
                .publishReplay(1)
                .refCount();
        }
    }
    /**
     * Performs a request with `post` http method.
     * @param apiKey
     * @param data
     * @param routeParams
     * @param headers
     * @param endPoint
     * @param responseType
     * @param params
     */
    public doPostApiCall(
        apiKey: string,
        data: any,
        routeParams?: any,
        headers?: any,
        endPoint?: string,
        responseType ?: string,
        params?: any
    ): Observable<Response> {
        const apiPath: string = this.getApiPath(apiKey, routeParams, endPoint);
        const requestOptions: RequestOptions = this._getReqOptions(headers, true, responseType);
        requestOptions.params = params;
        return this._http
            .post(apiPath, data, requestOptions)
            .map((res) => {
                try {
                    return res.json();
                } catch (e) {
                    return {
                        status: res.status
                    };
                }
            })
            .catch((error: any): any => {
                Observable.throw(error);
            });
    }
    /**
     * Performs a request with `put` http method.
     * @param apiKey
     * @param data
     * @param routeParams
     * @param headers
     */
    public doPutApiCall(
        apiKey: string,
        data: any,
        routeParams?: any,
        headers?: any,
        endPoint?: string
    ): Observable<Response> {
        const apiPath: string = this.getApiPath(apiKey, routeParams, endPoint);
        return this._http
            .put(apiPath, data, this._getReqOptions(headers, true))
            .map((res) => {
                try {
                    return res.json();
                } catch (e) {
                    return {
                        status: res.status
                    };
                }
            })
            .catch((error: any): any => {
                Observable.throw(error);
            });
    }

    public uploadFile(
        apiKey: string,
        file: File,
        routeParams?: any,
        headers?: any,
        endPoint?: string
    ): Observable<Response> {
        const formData: FormData = new FormData();
        formData.append('file', file);
        const apiPath: string = this.getApiPath(apiKey, routeParams, endPoint);
        const requestOptions: RequestOptions = this._getReqOptions(headers);
        return this._http
            .post(apiPath, formData, requestOptions)
            .map((res) => {
                if (-1 === this._successCodes.indexOf(res.status)) {
                    return {
                        status: res.status
                    };
                } else {
                    return res.json();
                }
            })
            .catch((error: any): any => {
                Observable.throw(error);
            });
    }

    private _appendHeaders(
        requestOptions: RequestOptions,
        headers: any
    ) {
        if (null === requestOptions.headers) {
            requestOptions.headers = new Headers();
        }
        for (const key in headers) {
            if (!headers.hasOwnProperty(key)) {
                continue;
            }
            requestOptions.headers.append(key, headers[key]);
        }
    }

    private _getReqOptions(headers: any, addContentType?: boolean, responseType?: string): RequestOptions {
        const requestOptions: RequestOptions = new RequestOptions();
        if (!this.commonUtil.isObject(headers)) {
            headers = {};
        }
        if (addContentType) {
            headers['Content-Type'] = 'application/json';
        }
        this._appendHeaders(requestOptions, headers);
        if (responseType) {
            requestOptions.responseType = 3;
        }
        return requestOptions;
    }

    private refreshToken(): Observable<any> {
        const loginInfo: any = this.commonUtil.getCokie('remeberMe');
        const reqPayload = Object.assign(
            {
                user: loginInfo.username,
                password: loginInfo.password
            }
        );
        const apiPath: string = this.getApiPath('login', undefined, undefined);
        // const requestOptions: RequestOptions = this._getReqOptions(headers);
        return this._http
            .post(apiPath, reqPayload)
            .map((res) => res.json())
            .catch((error: any): any => {
                Observable.throw(error);
            });
    }
}
