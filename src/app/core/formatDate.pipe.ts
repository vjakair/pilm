/*****************************************************************
 * Proprietary & Confidential  |  © 2017 PhaseZero Ventures LLC  *
 * This is part of PhaseZero Ventures LLC and cannot be copied,  *
 * modified and/or distributed without the express permission of *
 * PhaseZero Ventures LLC                                        *
 *****************************************************************/
/**
 * @author: Irshadahmed
 */
import {
    Pipe,
    PipeTransform
} from '@angular/core';
import {
    DateUtilService
} from './dateUtil.service';
@Pipe({
    name: 'myFormatDate'
})
export class FormatDatePipe implements PipeTransform {
    public transform(d: any, format: string = 'M/d/y'): string {
        if (!d) {
            return d;
        }
        let dateStr: string;
        const newDate = new Date(d);

        let hours: number = newDate.getHours();
        const minutes: number = newDate.getMinutes();
        let meridian: string = 'AM';
        if (12 < hours) {
            hours = hours - 12;
            meridian = 'PM';
        }

        const month: number = newDate.getMonth() + 1;
        const date: number = newDate.getDate();
        const year: number = newDate.getFullYear();
        switch (format) {
            case 'M/d/y':
                dateStr = month + '/' + date + '/' + year;
                break;
            case 'M.d.y':
                dateStr = month + '.' + date + '.' + year;
                break;
            case 'medium':
                dateStr = month + '.' + date + '.' + year + ' | ' +
                    hours + ':' + (10 > minutes ? '0' + minutes : minutes) + ' ' + meridian;
                break;
        }
        return dateStr;
    }
}
