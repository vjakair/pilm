/*****************************************************************
 * Proprietary & Confidential  |  © 2017 PhaseZero Ventures LLC  *
 * This is part of PhaseZero Ventures LLC and cannot be copied,  *
 * modified and/or distributed without the express permission of *
 * PhaseZero Ventures LLC                                        *
 *****************************************************************/
/**
 * @author: Irshadahmed
 */
import {
    Pipe,
    PipeTransform
} from '@angular/core';
@Pipe({
    name: 'mySearchFilter'
})
export class SearchFilterPipe implements PipeTransform {
    public transform(items: any[], searchTerm: string): any[] {
        if (!items || '' === searchTerm || !searchTerm) {
            return items;
        }
        let keys: string[];
        try {
            keys = Object.keys(items[0]);
        } catch (e) {
            return;
        }
        if (!keys || !keys.length) {
            return;
        }
        return items.filter((item: any) => {
            for (const key of keys) {
                if (!key) {
                    continue;
                }
                const val: string = item[key] && item[key].toLowerCase();
                if (val && -1 !== val.indexOf(searchTerm.toLocaleLowerCase())) {
                    return true;
                }
            }
            return false;
        });
    }
}
