/*****************************************************************
 * Proprietary & Confidential  |  © 2017 PhaseZero Ventures LLC  *
 * This is part of PhaseZero Ventures LLC and cannot be copied,  *
 * modified and/or distributed without the express permission of *
 * PhaseZero Ventures LLC                                        *
 *****************************************************************/
/**
 * @author: Irshadahmed
 */
import {
    FormsModule
} from '@angular/forms';
import {
    NgModule
} from '@angular/core';
import {
    RouterModule
} from '@angular/router';

// Import Custom Services
import {
    ApiDispatcherService
} from './apiDispatcher.service';
import {
    CacheFactoryService
} from './cacheFactory.service';
import {
    CommonUtilService
} from './commonUtil.service';
import {
    DateUtilService
} from './dateUtil.service';
import {
    FilterPipe
} from './filter.pipe';
import {
    ForbiddenValidatorDirective
} from './forbiddenDate.validator';
import {
    FormatDatePipe
} from './formatDate.pipe';
import {
    NavigationService
} from './navigation.service';
import {
    SearchFilterPipe
} from './search.filter';
import {
    TitleService
} from './title';
import {
    TranslationService
} from './translation.service';

@NgModule({
    imports: [
        FormsModule,
        RouterModule
    ],
    declarations: [
        ForbiddenValidatorDirective,
        FilterPipe,
        FormatDatePipe,
        SearchFilterPipe
    ],
    providers: [
        ApiDispatcherService,
        CacheFactoryService,
        CommonUtilService,
        DateUtilService,
        NavigationService,
        TitleService,
        TranslationService
    ],
    exports: [
        ForbiddenValidatorDirective,
        FilterPipe,
        FormatDatePipe,
        SearchFilterPipe
    ]
})
export class CoreModule { }
