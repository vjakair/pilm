import {
    Injectable
} from '@angular/core';
import {
    CommonUtilService
} from './commonUtil.service';
import {
    FormatDatePipe
} from './formatDate.pipe';
@Injectable()
export class DateUtilService {
    private readonly dateRegEx: RegExp = /^(?:(?:31(\/|-|\.)(?:0?[13578]|1[02]))\1|(?:(?:29|30)(\/|-|\.)(?:0?[1,3-9]|1[0-2])\2))(?:(?:1[6-9]|[2-9]\d)?\d{2})$|^(?:29(\/|-|\.)0?2\3(?:(?:(?:1[6-9]|[2-9]\d)?(?:0[48]|[2468][048]|[13579][26])|(?:(?:16|[2468][048]|[3579][26])00))))$|^(?:0?[1-9]|1\d|2[0-8])(\/|-|\.)(?:(?:0?[1-9])|(?:1[0-2]))\4(?:(?:1[6-9]|[2-9]\d)?\d{2})$/;
    private _datePipe: any;
    constructor(
        private _commonUtil: CommonUtilService
    ) {
        this._datePipe = new FormatDatePipe();
    }
    public getFormattedDate(date: Date): string {
        if (!this.isDateValid(date)) {
            return;
        }
        return this._datePipe.transform(date);
    }
    public getHours(meridian: boolean = true): string[] {
        const hours: string[] = [];
        const maxHours: number = meridian ? 12 : 23;
        let starthour: number = 1;
        while (starthour <= maxHours) {
            let hour: string = starthour.toString();
            if (10 > starthour) {
                hour = '0' + hour;
            }
            hours.push(hour);
            ++starthour;
        }
        return hours;
    }
    public getMeridianTime(timestamp: number): any {
        if (isNaN(timestamp)) {
            return {
                hours: '12',
                min: '00'
            };
        }
        const d = new Date(timestamp);
        let hours: number = d.getHours();
        const minutes: number = d.getMinutes();
        const meridian: string = 12 > hours ? 'AM' : 'PM';
        if (12 < hours) {
            hours = hours - 12;
        } else if (0 === hours) {
            hours = 12;
        }
        return {
            hours: 10 > hours ? '0' + hours : hours.toString(),
            min: 10 > minutes ? '0' + minutes : minutes.toString(),
            ampm: meridian
        };
    }
    public getMinutes(minStep: number = 5): string[] {
        const minutes: string[] = [];
        let startMin: number = 0;
        while (startMin < 60) {
            let min: string = startMin.toString();
            if (10 > startMin) {
                min = '0' + min;
            }
            minutes.push(min);
            startMin += minStep;
        }
        return minutes;
    }
    public getDaysInMonth(date: Date): number {
        const d = new Date(date.getFullYear(), date.getMonth() + 1, 0);
        return d.getDate();
    }
    public getToday(): Date {
        const date: Date = new Date();
        date.setHours(0, 0, 0, 0);
        return date;
    }
    public getNextDayDate(date: any): any {
        if (!this.isDateValid(date)) {
            return date;
        }
        const newDate: Date = new Date(date.toString());
        newDate.setDate(newDate.getDate() + 1);
        return newDate;
    }
    public convertWithTimeFormat(date, flag) {
        const d = new Date(date);
        const time = {
            hours: flag === 'from' ? 0 : d.getHours(),
            minutes: flag === 'from' ? 0 : d.getMinutes(),
            secs: flag === 'from' ? 0 : d.getSeconds(),
            miliSecs: flag === 'from' ? 0 : 0
        };
        return new Date(Date.UTC(
            d.getFullYear(),
            d.getMonth(),
            d.getDate(),
            time.hours,
            time.minutes,
            time.secs,
            time.miliSecs
        )).getTime();
    }
    public convertUTCDateToLocalDate(date) {
        try {
            const d = new Date(date);
            return d.setMinutes(d.getMinutes() + d.getTimezoneOffset());
           /* const convert = new Date(d.getTime() - d.getTimezoneOffset() * 60 * 1000);
            return convert.getTime();*/
        } catch(e) {
            return date;
        }
    }
    public getOtherDate(date: Date, days: number): Date {
        const d: Date = new Date(date.getTime());
        d.setDate(date.getDate() + days);
        return d;
    }
    public isDateFormatCorrect(date: string): boolean {
        return 10 >= date.length && this.dateRegEx.test(date);
    }
    /**
     * @param date
     */
    public isDateValid(date: any): boolean {
        return date instanceof Date && 'Invalid Date' !== date.toDateString();
    }
    /**
     * accepts date only in D(D)/M(M)/YY(YY) format
     * @param date
     */
    public isDateInvalid(date: any): boolean {
        return isNaN(Date.parse(date.toString()));
    }
    public getDateFormat(date: any): any {
        if (!this.isDateValid(date)) {
            return date;
        }
        return new Date(date.toString());
    }
}
