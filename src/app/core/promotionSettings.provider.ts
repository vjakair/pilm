import {
    Injectable
} from '@angular/core';
Injectable();
export class PromotionSettings {
    public readonly PROMOTION_STAGES: string[] = [
        'DEFINE_PROMOTION',
        'GOALS',
        'CONFIGURE_RULES',
        'TARGET_MARKET',
        'APPROVAL_PROCESS'
    ];
    public readonly templateSettings: any[] = [
        {
            templateName: 'PROMOTION.DOLLAR_OFF',
            templateImg: 'Dollar_icon@1x.png',
            type: 'dollar-off'
        },
        {
            templateName: 'PROMOTION.PERCENT_OFF',
            templateImg: 'percentage_icon@1x.png',
            type: 'percent-off'
        },
        {
            templateName: 'PROMOTION.FREE_FREIGHT',
            templateImg: 'free_freight_icon@1x.png',
            type: 'free-freight-off'
        },
        {
            templateName: 'PROMOTION.NEW_PROD_LAUNCH',
            templateImg: 'new_product_icon@1x.png',
            type: 'new-prod-launch'
        },
        {
            templateName: 'PROMOTION.GIFT',
            templateImg: 'free_gift_icon.png',
            type: 'free-gift'
        }
    ];
    public readonly STEPS: any[] = [
        {
            id: 'headers',
            title: 'DEFINE_PROMOTION',
            currentRouterLink: '/promotions/:promotionId/define/:templateId',
            BUTTONS: [
                {
                    text: 'BUTTONS.SAVE_DRAFT',
                    action: 'SAVED_AS_DRAFT',
                    step: 0,
                    disabled: false,
                    isPrimary: false,
                    inverse: true,
                    routerLink: '/'
                },
                {
                    text: 'BUTTONS.NXT_STEP',
                    action: 'SAVED_AS_DRAFT',
                    step: 0,
                    disabled: false,
                    isPrimary: true,
                    routerLink: '/promotions/:promotionId/goals/:templateId'
                }
            ]
        },
        {
            id: 'goals',
            title: 'GOALS',
            currentRouterLink: '/promotions/:promotionId/goals/:templateId',
            BUTTONS: [
                {
                    text: 'BUTTONS.SAVE_DRAFT',
                    action: 'SAVED_AS_DRAFT',
                    disabled: false,
                    isPrimary: false,
                    inverse: true,
                    routerLink: '/'
                },
                {
                    text: 'BUTTONS.NXT_STEP',
                    action: 'SAVED_AS_DRAFT',
                    disabled: false,
                    isPrimary: true,
                    routerLink: '/promotions/:promotionId/rules/:templateId'
                }
            ]
        },
        {
            id: 'rules',
            title: 'CONFIGURE_RULES',
            currentRouterLink: '/promotions/:promotionId/rules/:templateId',
            BUTTONS: [
                {
                    text: 'BUTTONS.SAVE_DRAFT',
                    action: 'SAVED_AS_DRAFT',
                    disabled: false,
                    isPrimary: false,
                    inverse: true,
                    routerLink: '/'
                },
                {
                    text: 'BUTTONS.NXT_STEP',
                    action: 'SAVED_AS_DRAFT',
                    disabled: false,
                    isPrimary: true,
                    routerLink: '/promotions/:promotionId/target-market/:templateId'
                }
            ]
        },
        {
            id: 'targets',
            title: 'TARGET_MARKET',
            currentRouterLink: '/promotions/:promotionId/target-market/:templateId',
            BUTTONS: [
                {
                    text: 'BUTTONS.SAVE_DRAFT',
                    action: 'SAVED_AS_DRAFT',
                    disabled: false,
                    isPrimary: false,
                    inverse: true,
                    routerLink: '/'
                },
                {
                    text: 'BUTTONS.NXT_STEP',
                    action: 'SAVED_AS_DRAFT',
                    disabled: false,
                    isPrimary: true,
                    routerLink: '/promotions/:promotionId/approvals/:templateId'
                }
            ]
        },
        {
            id: 'approverData',
            title: 'APPROVAL_PROCESS',
            currentRouterLink: '/promotions/:promotionId/approvals/:templateId',
            BUTTONS: [
                {
                    text: 'BUTTONS.SAVE_DRAFT',
                    action: 'SAVED_AS_DRAFT',
                    disabled: false,
                    isPrimary: false,
                    inverse: true,
                    routerLink: '/'
                },
                {
                    text: 'BUTTONS.DONE',
                    action: 'PENDING_APPROVAL',
                    disabled: false,
                    isPrimary: true,
                    routerLink: '/'
                }
            ]
        },
        {
            id: 'choose',
            title: 'CHOOSE_TEMPLATE',
            BUTTONS: [
                {
                    text: 'BUTTONS.CREATE_PROMO',
                    disabled: true,
                    isPrimary: true,
                    routerLink: '/promotions/:promotionId/define'
                }
            ]
        },
        {
            id: 'choose',
            title: 'PROMO_DETAILS',
            BUTTONS: [
                {
                    text: 'BUTTONS.APPROVE',
                    action: 'approved',
                    actionKey: 'COMMON.CONF_APPR',
                    permission: 'ApprovePromotion',
                    disabled: false,
                    isPrimary: true,
                    hide: false
                },
                {
                    text: 'BUTTONS.REJECT',
                    action: 'rejected',
                    actionKey: 'COMMON.REJ_APPR',
                    permission: 'RejectPromotion',
                    disabled: false,
                    isPrimary: false,
                    showBorder: true,
                    hide: false
                },
                {
                    text: 'BUTTONS.RFI',
                    action: 'rf',
                    actionKey: 'COMMON.REQ_INFO',
                    permission: 'RFIPromotion',
                    disabled: false,
                    isPrimary: false,
                    showBorder: true,
                    hide: false
                },
                {
                    text: 'BUTTONS.ARCHIVE',
                    actionKey: 'COMMON.ARCH_PROMO',
                    action: 'archived',
                    permission: 'ArchivePromotion',
                    disabled: false,
                    isPrimary: false,
                    showBorder: true,
                    hide: false
                },
                {
                    text: 'BUTTONS.EDIT',
                    action: 'edit',
                    disabled: false,
                    permission: 'CreatePromotion',
                    isPrimary: false,
                    showBorder: true,
                    hide: false
                }
            ]
        }
    ];

    public readonly CategoryConfig: any = {
        UNCATEGORIZED1: 'No Category',
        UNCATEGORIZED2: 'No Category',
        UNCATEGORIZED3: 'No Category'
    };
}
