import {
    Directive,
    Input
} from '@angular/core';
import {
    AbstractControl,
    NG_VALIDATORS,
    ValidationErrors,
    Validator,
    ValidatorFn
} from '@angular/forms';

import {
    forbiddenDateValidator
} from '../helper/validation.helper';

@Directive({
    selector: '[forbiddenDate]',
    providers: [{ provide: NG_VALIDATORS, useExisting: ForbiddenValidatorDirective, multi: true }]
})
export class ForbiddenValidatorDirective implements Validator {
    public validate(control: AbstractControl): { [key: string]: any } {
        return forbiddenDateValidator()(control);
    }
}
