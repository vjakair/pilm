/*****************************************************************
 * Proprietary & Confidential  |  © 2017 PhaseZero Ventures LLC  *
 * This is part of PhaseZero Ventures LLC and cannot be copied,  *
 * modified and/or distributed without the express permission of *
 * PhaseZero Ventures LLC                                        *
 *****************************************************************/
/**
 * @author: Irshadahmed
 */
import {
    NgModule,
    APP_INITIALIZER,
    ApplicationRef
} from '@angular/core';
import {
    HttpClientModule,
    HttpClient
} from '@angular/common/http';
// import {
//     removeNgStyles,
//     createNewHosts,
//     createInputTransfer
// } from '@AngularClass/hmr';
import {
    Http,
    HttpModule,
    RequestOptions,
    XHRBackend
} from '@angular/http';
import {
    BrowserModule,
    Title
} from '@angular/platform-browser';
import {
    BrowserAnimationsModule
} from '@angular/platform-browser/animations';
import {
    RouterModule,
    PreloadAllModules
} from '@angular/router';
import {
    TranslateModule,
    TranslateLoader
} from '@ngx-translate/core';
import {
    TranslateHttpLoader
} from '@ngx-translate/http-loader';

/*
 * Platform and Environment providers/directives/pipes
 */
import {
    AppConfig
} from './config/app.config';
import {
    AuthModule
} from './auth/auth.module';
import {
    CoreModule
} from './core/core.module';
import {
    HomeModule
} from './home/home.module';
import {
    HttpInterceptor
} from './config/http.interceptor';
import {
    ComponentsModule
} from './components/components.module';

import {
    ENV_PROVIDERS
} from './environment';
import {
    ROUTES
} from './app.routes';
// App is our top level component
import {
    AppComponent
} from './app.component';
import {
    APP_RESOLVER_PROVIDERS
} from './app.resolver';
import {
    AppState,
    InternalStateType
} from './app.service';
import {
    AboutComponent
} from './about';
import {
    EventEmitterService
} from './core/event-emitter/';
import {
    NoContentComponent
} from './no-content';
import {
    XLargeDirective
} from './home/x-large';
import {
    PromotionModule
} from './promotions/promotion.module';

import '../styles/styles.scss';
import { PaginationModule } from 'ngx-bootstrap/pagination';

// Application wide providers
const APP_PROVIDERS = [
    ...APP_RESOLVER_PROVIDERS,
    AppConfig,
    AppState,
    EventEmitterService,
    HttpInterceptor,
    {
        provide: Http,
        useFactory: (
            backend: XHRBackend,
            defaultOptions: RequestOptions,
            eventEmitter: EventEmitterService,
            _appConfig: AppConfig,
        ) => new HttpInterceptor(backend, defaultOptions, eventEmitter, _appConfig),
        deps: [XHRBackend, RequestOptions, EventEmitterService]
    },
    {
        provide: APP_INITIALIZER,
        useFactory: (appConfig: AppConfig) => () => appConfig.loadAppConfig(),
        deps: [AppConfig],
        multi: true
    }
];

interface StoreType {
    state: InternalStateType;
    restoreInputValues: () => void;
    disposeOldHosts: () => void;
}

export function createTranslateLoader(http: HttpClient) {
    return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}

/**
 * `AppModule` is the main entry point into Angular2's bootstraping process
 */
@NgModule({
    bootstrap: [AppComponent],
    declarations: [
        AppComponent,
        AboutComponent,
        NoContentComponent,
        XLargeDirective
    ],
    /**
     * Import Angular's modules.
     */
    imports: [
        AuthModule,
        BrowserAnimationsModule,
        BrowserModule,
        CoreModule,
        ComponentsModule,
        HttpClientModule,
        HttpModule,
        HomeModule,
        PromotionModule,
        PaginationModule.forRoot(),
        TranslateModule.forRoot({
            loader: {
                provide: TranslateLoader,
                useFactory: (createTranslateLoader),
                deps: [HttpClient]
            }
        }),
        RouterModule.forRoot(ROUTES, { useHash: false, preloadingStrategy: PreloadAllModules })
    ],
    /**
     * Expose our Services and Providers into Angular's dependency injection.
     */
    providers: [
        ENV_PROVIDERS,
        APP_PROVIDERS
    ]
})
export class AppModule {

    constructor(
        public appRef: ApplicationRef,
        public appState: AppState
    ) { }

    public hmrOnInit(store: StoreType) {
        if (!store || !store.state) {
            return;
        }
        console.log('HMR store', JSON.stringify(store, null, 2));
        /**
         * Set state
         */
        this.appState._state = store.state;
        /**
         * Set input values
         */
        if ('restoreInputValues' in store) {
            const restoreInputValues = store.restoreInputValues;
            setTimeout(restoreInputValues);
        }

        this.appRef.tick();
        delete store.state;
        delete store.restoreInputValues;
    }

    public hmrOnDestroy(store: StoreType) {
        const cmpLocation = this.appRef.components.map((cmp) => cmp.location.nativeElement);
        /**
         * Save state
         */
        const state = this.appState._state;
        store.state = state;
        /**
         * Recreate root elements
         */
        // store.disposeOldHosts = createNewHosts(cmpLocation);
        /**
         * Save input values
         */
        // store.restoreInputValues = createInputTransfer();
        /**
         * Remove styles
         */
        // removeNgStyles();
    }

    public hmrAfterDestroy(store: StoreType) {
        /**
         * Display new elements
         */
        store.disposeOldHosts();
        delete store.disposeOldHosts;
    }
}
