/*****************************************************************
 * Proprietary & Confidential  |  © 2017 PhaseZero Ventures LLC  *
 * This is part of PhaseZero Ventures LLC and cannot be copied,  *
 * modified and/or distributed without the express permission of *
 * PhaseZero Ventures LLC                                        *
 *****************************************************************/
/**
 * @author: Irshadahmed
 */
/* import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';

@Injectable()
export class PromoRouteResolver implements Resolve<any> {
    public resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        return Observable.of({
            patterns: [
                /^\/promotions\/.*\/snapshots$/,
                /^\/promotions\/.*\/snapshots$/
            ]
        });
    }
} */

/**
 * An array of services to resolve routes with data.
 */
export const APP_RESOLVER_PROVIDERS = [
    {
        provide: 'PromoRoutePatters',
        useValue: () => {
            return [
                /^\/promotions\/.*\/define\/.*$/,
                /^\/promotions\/.*\/goals\/.*$/,
                /^\/promotions\/.*\/rules\/.*$/,
                /^\/promotions\/.*\/target-market\/.*$/,
                /^\/promotions\/.*\/approvals\/.*$/
            ];
        }
    }
];
