/*****************************************************************
 * Proprietary & Confidential  |  © 2017 PhaseZero Ventures LLC  *
 * This is part of PhaseZero Ventures LLC and cannot be copied,  *
 * modified and/or distributed without the express permission of *
 * PhaseZero Ventures LLC                                        *
 *****************************************************************/
/**
 * @author: Irshadahmed
 */
import {
    ChangeDetectorRef,
    Component,
    ViewChild
} from '@angular/core';
import {
    NgForm
} from '@angular/forms';
// Import Custom Services
import {
    AuthService
} from '../auth.service';
import {
    CommonUtilService
} from '../../core/commonUtil.service';
import {
    EventEmitterService
} from '../../core/event-emitter';
import {
    NavigationService
} from '../../core/navigation.service';
import {
    TitleService
} from '../../core/title';

/**
 * We're loading this component asynchronously
 * We are using some magic with es6-promise-loader that will wrap the module with a Promise
 * see https://github.com/gdi2290/es6-promise-loader for more info
 */

@Component({
    selector: 'login',
    templateUrl: `./login.component.html`,
    styleUrls: ['./signIn.style.scss']
})
export class LoginComponent {
    public doRemeber: boolean;
    public login: any = { username: null, password: null };
    @ViewChild('loginForm')
    private _loginFrm: NgForm;
    private _disabled: boolean = false;
    public constructor(
        private _authService: AuthService,
        private _changeDetection: ChangeDetectorRef,
        private _commonUtil: CommonUtilService,
        private _eventEmitter: EventEmitterService,
        private _navigation: NavigationService,
        titleService: TitleService) {
        const loginInfo = _commonUtil.getCokie('remeberMe');
        if (loginInfo && _commonUtil.isObject(loginInfo)) {
            Object.assign(this.login, loginInfo);
        }
        titleService.setTitle('SIGN_IN');
    }
    public signIn() {
        if (this._loginFrm.invalid) {
            this._commonUtil.setFormDirty(this._loginFrm.controls);
            return;
        }
        if (this.login.doRemeber) {
            this._commonUtil.setCookie('remeberMe', JSON.stringify(this.login),
                365);
        } else {
            this._commonUtil.deleteCokie('remeberMe');
        }
        const reqPayload = {
                user: this.login.username,
                password: this.login.password
            };
        this._authService.login(reqPayload).then((): void => {
            let redirectionUrl: string = this._authService.redirectionUrl;
            if (!this._commonUtil.isValidString(redirectionUrl) || '/signin' === redirectionUrl) {
                redirectionUrl = '/';
            }
            this._navigation.navigate(redirectionUrl);
            setTimeout(() => {
                this._eventEmitter.onUserLoginStateChange.emit(true);
            }, 1000);
        }, (error: any): void => {
            let messageKey: string = 'PROMOTION.ERROR.SERVER';
            if ('INVALID_LOGIN_ID' === error.code) {
                messageKey = 'SIGN_IN.MESSAGES.INVALID';
            } else if (error.status === 'SUCCESS' && error['AUTHORIZED'] === 'NO') {
                messageKey = 'SIGN_IN.MESSAGES.NOTAUTHORIZED';
            }

            this._eventEmitter
                .onAlert
                .emit({
                    type: 'alert-warning',
                    msgKey: messageKey
                });
        });
    }
}
