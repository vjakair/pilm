/*****************************************************************
 * Proprietary & Confidential  |  © 2017 PhaseZero Ventures LLC  *
 * This is part of PhaseZero Ventures LLC and cannot be copied,  *
 * modified and/or distributed without the express permission of *
 * PhaseZero Ventures LLC                                        *
 *****************************************************************/
/**
 * @author: Irshadahmed
 */
import {
    CommonModule
} from '@angular/common';
import {
    NgModule
} from '@angular/core';
import {
    FormsModule
} from '@angular/forms';
import {
    PreloadAllModules,
    RouterModule
} from '@angular/router';
import {
    TranslateModule
} from '@ngx-translate/core';
import {
    CookieService
} from 'ngx-cookie-service';

import {
    ROUTES
} from './auth.routes';
import {
    AuthenticatedGuard
} from './authenticated.guard';
import {
    AuthService
} from './auth.service';
import {
    AuthComponent
} from './auth.component';
import {
    LoginComponent
} from './login/login.component';

@NgModule({
    declarations: [
        /**
         * Components / Directives/ Pipes
         */
        AuthComponent,
        LoginComponent
    ],
    imports: [
        CommonModule,
        FormsModule,
        TranslateModule,
        RouterModule.forRoot(ROUTES, { useHash: false, preloadingStrategy: PreloadAllModules })
    ],
    providers: [
        AuthenticatedGuard,
        AuthService,
        CookieService
    ]
})
export class AuthModule {
}
