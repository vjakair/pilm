/*****************************************************************
 * Proprietary & Confidential  |  © 2017 PhaseZero Ventures LLC  *
 * This is part of PhaseZero Ventures LLC and cannot be copied,  *
 * modified and/or distributed without the express permission of *
 * PhaseZero Ventures LLC                                        *
 *****************************************************************/
/**
 * @author: Irshadahmed
 */
import { Routes } from '@angular/router';
import { AuthComponent } from './auth.component';
import { LoginComponent } from './login/login.component';
import { AuthenticatedGuard } from './authenticated.guard';
/*import { AuthenticatedGuard } from '../auth/authenticated.guard';*/

export const ROUTES: Routes = [
    {
        path: 'auth',
        component: AuthComponent
    },
    {
        path: 'signin',
        component: LoginComponent,
        canActivate: [AuthenticatedGuard]
    }
    /*{
         path: 'auth', component: AuthComponent,
         children: [
             { path: '', redirectTo: 'signin', pathMatch: 'full'},
             { path: 'signin', component: LoginComponent }
         ]
     }*/
];
