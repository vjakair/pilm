/*****************************************************************
 * Proprietary & Confidential  |  © 2017 PhaseZero Ventures LLC  *
 * This is part of PhaseZero Ventures LLC and cannot be copied,  *
 * modified and/or distributed without the express permission of *
 * PhaseZero Ventures LLC                                        *
 *****************************************************************/
/**
 * @author: Irshadahmed
 */
// Import Core Services
import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Observable } from 'rxjs/Observable';
// Import Custom Services
import {
    ApiDispatcherService
} from '../core/apiDispatcher.service';
import {
    CacheFactoryService
} from '../core/cacheFactory.service';
import {
    CommonUtilService
} from '../core/commonUtil.service';
import {
    EventEmitterService
} from '../core/event-emitter';
import {
    LocalStorageService
} from '../helper/localStorage.helper';
import {
    AppConfig
} from '../config/app.config';

@Injectable()
export class AuthService extends LocalStorageService {
    private _approverList: any[];
    private _bearerToken: string = null;
    private _redirectionUrl: string;
    private _userPermissions: string[] = [];
    private _userRoles: string[] = [];
    constructor(
        private _apiDispatcher: ApiDispatcherService,
        public commonUtil: CommonUtilService,
        private _cacheFactory: CacheFactoryService,
        private _eventEmitter: EventEmitterService,
        private _http: Http,
        private _appConfig: AppConfig
    ) {
        super(commonUtil);
    }
    /**
     * Returns bearer token
     */
    get bearerToken(): string {
        if (!this._bearerToken) {
            this._bearerToken = localStorage.getItem('pilm_bearerToken');
        }
        return this._bearerToken;
    }
    /**
     * Check if user is authenticated
     */
    get isAuthenticated() {
        return this.bearerToken ? true : false;
    }

    get jwt(): boolean {
        return this.getItem('pilm_jwt');
    }

    get redirectionUrl(): string {
        return this._redirectionUrl;
    }

    set redirectionUrl(url: string) {
        this._redirectionUrl = url;
    }

    public getRememberMeInfo(): any {
        return this.getItem('remeberMe', true);
    }

    public getUserDetails(key: string): string | any {
        return this._getUserDetails(key);
    }

    /**
     * Invalidate user session
     */
    public invalidateSession(): Promise<boolean> {
        return new Promise((resolve, reject) => {
            this._cacheFactory.deleteAllItems();
            this.deleteAllItems();
            this._bearerToken = null;
            this._eventEmitter
                .onUserLoginStateChange
                .emit(
                false
                );
            resolve(true);
        });
    }
    /**
     * Login from jwt token
     * @param token
     */
    public jwtLogin(token: any): Observable<any> {
        this._setBearerToken(token);
        return this._setUserContext(true);
    }
    /**
     * Login the given user
     * @param reqPayload
     */
    public login(reqPayload: any) {
        return new Promise((resolve, reject) => {
            this._apiDispatcher.doPostApiCall(`login`, reqPayload)
                .subscribe((response: any) => {
                    if (response.status === 'ERROR') {
                        reject(response);
                        return;
                    }
                    this._setBearerToken(response.message);
                    this._setUserContext()
                        .subscribe((userDetails: any) => {
                            resolve(userDetails);
                        }, (error: any) => {
                            reject(error);
                        });
                }, ((error) => {
                    reject(error);
                }));
        });
    }

    /**
     * get details of more than one user
     * ids are an array of user id.
     */
    public getUsersDetailByUserIds(ids: any): Observable<any> {
        return this._apiDispatcher
            .doPostApiCall('usersDetailByUserIds', ids);
    }
    /**
     * get list of all user under by roles
     * roles are an array of different role.
     */
    public getUsersListByRoles(roles: any): Observable<void> {
        return new Observable((subscriber: any) => {
            if (this.commonUtil.isObject(this._approverList)) {
                subscriber.next(this._approverList);
                subscriber.complete();
                return;
            }
            this._apiDispatcher
                .doPostApiCall('usersListByRoles', roles)
                .subscribe((response: any) => {
                    if (response.errorCode) {
                        subscriber.error(response);
                        return;
                    }
                    this._approverList = response;
                    subscriber.next(response);
                    subscriber.complete();
                });
        });
    }

    /**
     * Login from the given auth token
     * @param token
     */
    public loginFromAuthToken(token: string) {
        // Will login from auth token here
    }

    public saveAdminConsoleChanges(approversData: any, application: any): Observable<any> {
        return this._apiDispatcher
            .doPostApiCall('adminConsole', approversData, { application : application });
    }

    /**
     * Check if user has a given permission
     * @param permission
     */
    public userHasPermissions(permission): boolean {
        const userPermissions = this._getUserPermissions();
        return -1 !== userPermissions.indexOf(permission);
    }

    private _getUserPermissions(): string[] {
        if (this.commonUtil.isValidArray(this._userPermissions)) {
            return this._userPermissions;
        }
        const userPermissions: string[] =
            this.getItem('pilm_upKey', true);
        if (null === userPermissions) {
            this.invalidateSession();
        }
        this._userPermissions =
            this.commonUtil.isValidArray(userPermissions)
                ? userPermissions
                : [];
        return this._userPermissions;
    }

    private _setBearerToken(token: string) {
        if (!token) {
            return;
        }
        this._bearerToken = 'Bearer ' + token;
        this.setItem('pilm_bearerToken', this._bearerToken);
    }

    private _setUserContext(jwt: boolean = false): Observable<any> {
        return new Observable((subscriber: any) => {
            this._apiDispatcher.doPostApiCall(`jwtLogin`, undefined)
                .subscribe((response: any) => {
                    if (response.errorCode || 'error' === response.type) {
                        subscriber.error(response);
                        return;
                    }
                    const applicationName = this._appConfig.getApplicationName();
                    let isApplicationFound = false;
                    for (const app of response.applications) {
                        if (app === applicationName) {
                            isApplicationFound = true;
                            break;
                        }
                    }
                    if (!isApplicationFound) {
                        this.setItem('bearerToken', null);
                        subscriber.error(response);
                        return;
                    }
                    this._setUserDetails({
                        fullName: response.firstName + ' ' + response.lastName
                    });
                    this._setUserPermissions(response.permissions);
                    this._setUserRoles(response.roles);
                    this.setItem('pilm_jwt', jwt);
                    subscriber.next(response);
                    subscriber.complete();
                });
        });
    }

    private _setUserDetails(userDetails: any): void {
        this.setItem('pilm_userDetails', userDetails);
    }

    private _getUserDetails(key: string): string {
        const userDetails: any = this.getItem('pilm_userDetails');
        if (!this.commonUtil.isObject(userDetails)) {
            return undefined;
        }
        return this.commonUtil.isValidString(key) ?
            userDetails[key] : userDetails;
    }

    private _setUserPermissions(userPermissions: string[]): void {
        this._userPermissions = userPermissions;
        this.setItem('pilm_upKey', userPermissions, true);
    }

    private _setUserRoles(userRoles: string[]): void {
        this._userRoles = userRoles;
        this.setItem('pilm_urKey', userRoles, true);
    }

    private _getUserRoles(): string[] {
        if (this.commonUtil.isValidArray(this._userRoles)) {
            return this._userRoles;
        }
        const userRoles: string[] =
            this.getItem('pilm_urKey', true);
        this._userRoles =
            this.commonUtil.isValidArray(userRoles)
                ? userRoles
                : [];

        return this._userRoles;
    }
}
