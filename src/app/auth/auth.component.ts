/*****************************************************************
 * Proprietary & Confidential  |  © 2017 PhaseZero Ventures LLC  *
 * This is part of PhaseZero Ventures LLC and cannot be copied,  *
 * modified and/or distributed without the express permission of *
 * PhaseZero Ventures LLC                                        *
 *****************************************************************/
/**
 * @author: Irshadahmed
 */
import {
    Component,
    OnInit
} from '@angular/core';
import {
    Router,
    ActivatedRoute,
    Params
} from '@angular/router';
import {
    AuthService
} from './auth.service';
import {
    CommonUtilService
} from '../core/commonUtil.service';
import {
    NavigationService
} from '../core/navigation.service';
import { EventEmitterService } from '../core/event-emitter';

@Component({
    /**
     * The selector is what angular internally uses
     * for `document.querySelectorAll(selector)` in our index.html
     * where, in this case, selector is the string 'home'.
     */
    selector: 'auth',  // <home></home>
    /**
     * We need to tell Angular's Dependency Injection which providers are in our app.
     */
    providers: [

    ],
    /**
     * Our list of styles in our component. We may add more to compose many styles together.
     */
    styleUrls: ['./auth.style.css'],
    /**
     * Every Angular template is first compiled by the browser before Angular runs it's compiler.
     */
    templateUrl: './auth.component.html'
})

export class AuthComponent implements OnInit {
    private _jwtToken: string;
    constructor(
        private _authService: AuthService,
        private _eventEmitter: EventEmitterService,
        private _commonUtil: CommonUtilService,
        private _navigation: NavigationService,
        private _activatedRoute: ActivatedRoute
    ) {

        this._activatedRoute.queryParams.subscribe((params: Params) => {
            const token: string = params['token'];
            if (_commonUtil.isValidString(token)) {
                this._jwtToken = token;
                const routeTo = params['refUrl'];
                this._jwtLogin(routeTo);
            } else {
                _navigation.navigate('./signin');
            }
        });
    }
    public ngOnInit() {
        console.log('AuthComponent loaded!');
        /**
         * this.title.getData().subscribe(data => this.data = data);
         */
    }
    private _doJwtLogin(routeTo) {
        this._authService
            .jwtLogin(this._jwtToken)
            .subscribe(
                (response) => {
                    if (this.unAuthorizedAccess(response)) {
                        this._authService.invalidateSession()
                            .then(() => {
                                setTimeout(() => {
                                    this._navigation.navigate('/signin');
                                }, 10000);
                            });
                        return;
                    }
                    if (this._commonUtil.isValidString(routeTo)) {
                        this._navigation.navigate(routeTo);
                    } else {
                        this._navigation.navigate('./');
                    }
                    setTimeout(() => {
                        this._eventEmitter.onUserLoginStateChange.emit(true);
                    }, 1000);
                },
                (error) => {
                    this.unAuthorizedAccess(error);
                    this._authService.invalidateSession()
                        .then(() => {
                            this._navigation.navigate('/signin');
                        });
                }
            );
    }

    private unAuthorizedAccess(response) {
        if (response && response.AUTHORIZED === 'NO') {
            const messageKey = 'SIGN_IN.MESSAGES.NOTAUTHORIZED';
            this._eventEmitter
                .onAlert
                .emit({
                    type: 'alert-warning',
                    msgKey: messageKey
                });
            return true;
        } else {
            return false;
        }
    }

    private _jwtLogin(routeTo): void {
        if (this._authService.isAuthenticated) {
            this._authService
                .invalidateSession()
                .then(() => {
                    this._doJwtLogin(routeTo);
                });
        } else {
            this._doJwtLogin(routeTo);
        }
    }
}
