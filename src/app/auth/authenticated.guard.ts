/*****************************************************************
 * Proprietary & Confidential  |  © 2017 PhaseZero Ventures LLC  *
 * This is part of PhaseZero Ventures LLC and cannot be copied,  *
 * modified and/or distributed without the express permission of *
 * PhaseZero Ventures LLC                                        *
 *****************************************************************/
/**
 * @author: Irshadahmed
 */
// Import Core Services
import { Injectable } from '@angular/core';
import {
    ActivatedRouteSnapshot,
    CanActivate,
    Params,
    NavigationExtras,
    RouterStateSnapshot
} from '@angular/router';
// Import Auth Service
import {
    AuthService
} from '../auth/auth.service';
import {
    NavigationService
} from '../core/navigation.service';
@Injectable()
export class AuthenticatedGuard implements CanActivate {
    constructor(
        private _authService: AuthService,
        private _navigation: NavigationService
    ) { }

    public canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const qParams: Params = route.queryParams;
        const rData: Params = route.data;
        const rParams: Params = route.params;
        this._navigation.queryParams = qParams;
        this._navigation.routeData = rData;
        this._navigation.routeParams = rParams;
        const navigationExtras: NavigationExtras = {};
        if (Object.keys(qParams).length) {
            navigationExtras.queryParams = qParams;
        }
        const stateUrl: string = state.url;
        if (this._authService.isAuthenticated) {
            if ('/signin' === stateUrl) {
                this._navigation.navigate('/', navigationExtras);
                return false;
            } else {
                return true;
            }
        } else {
            if ('/signin' === stateUrl || -1 !== stateUrl.indexOf('/signin')) {
                return true;
            } else {
                this._authService.redirectionUrl = stateUrl;
                this._navigation.navigate('/signin', navigationExtras);
                return false;
            }
        }
    }
}
