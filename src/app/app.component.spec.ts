/*****************************************************************
 * Proprietary & Confidential  |  © 2017 PhaseZero Ventures LLC  *
 * This is part of PhaseZero Ventures LLC and cannot be copied,  *
 * modified and/or distributed without the express permission of *
 * PhaseZero Ventures LLC                                        *
 *****************************************************************/
/**
 * @author: Irshadahmed
 */
import { NO_ERRORS_SCHEMA } from '@angular/core';
import {
    inject,
    async,
    TestBed,
    ComponentFixture
} from '@angular/core/testing';
import { Observable } from 'rxjs/Observable';
/**
 * Load the implementations that should be tested
 */
import { AppComponent } from './app.component';
import { TRANSLATE_PROVIDERS, TRANSLATE_CONFIG } from './mockProviders.spec';
import {
    AuthService
} from './auth/auth.service';
import {
    EventEmitterService
} from './core/event-emitter/eventEmitter.service';

/*import { AppState } from './app.service';
import { TranslateService } from '@ngx-translate/core';
import { TranslateStore } from '@ngx-translate/core/src/translate.store';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { Observable } from 'rxjs/Observable';

const translations = {};
class FakeLoader implements TranslateLoader {
    public getTranslation(lang: string): Observable<any> {
        return Observable.of(translations);
    }
}
*/

class MockCompService {
    public isAuthenticated: boolean = true;
    public onUserLoginStateChange: any = {
        subscribe: () => {
            console.log('0');
        }
    };
}

describe(`App`, () => {
    const comp: AppComponent;
    const fixture: ComponentFixture<AppComponent>;

    /**
     * async beforeEach
     */
    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [AppComponent],
            schemas: [NO_ERRORS_SCHEMA],
            imports: [
                TRANSLATE_CONFIG
                /*TranslateModule.forRoot({
                    loader: { provide: TranslateLoader, useClass: FakeLoader }
                })*/
            ],
            providers: [
                TRANSLATE_PROVIDERS,
                {
                    provide: AuthService,
                    useClass: MockCompService
                },
                {
                    provide: EventEmitterService,
                    useClass: MockCompService
                }
            ]
        })
            /**
             * Compile template and css
             */
            .compileComponents();
    }));

    /**
     * Synchronous beforeEach
     */
    beforeEach(() => {
        fixture = TestBed.createComponent(AppComponent);
        comp = fixture.componentInstance;

        /**
         * Trigger initial data binding
         */
        fixture.detectChanges();
    });

    it(`should be readly initialized`, () => {
        expect(fixture).toBeDefined();
        expect(comp).toBeDefined();
    });

  

    /*it('should log ngOnInit', () => {
        spyOn(console, 'log');
        expect(console.log).not.toHaveBeenCalled();

        comp.ngOnInit();
        expect(console.log).toHaveBeenCalled();
    });*/

});
