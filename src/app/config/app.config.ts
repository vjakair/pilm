/*****************************************************************
 * Proprietary & Confidential  |  © 2017 PhaseZero Ventures LLC  *
 * This is part of PhaseZero Ventures LLC and cannot be copied,  *
 * modified and/or distributed without the express permission of *
 * PhaseZero Ventures LLC                                        *
 *****************************************************************/
/**
 * @author: Irshadahmed
 */
import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Observable } from 'rxjs/Rx';

import {
    TranslateService
} from '@ngx-translate/core';
import { CommonUtilService } from '../core/commonUtil.service';

@Injectable()
export class AppConfig {
    private appConfig: any = null;
    private envSettings: any = null;
    private version: number = BUILD.VERSION;
    public defaultLocaleId: any;
    private resolve: any;

    constructor(
        private _http: Http,
        private _translateSvc: TranslateService,
        private _commonUtilService: CommonUtilService
    ) {
    }
    /**
     * Use to get the data found in the second file (config file)
     */
    public getConfig(key: any) {
        return this.appConfig[key];
    }

    public getAppFeatureSettings(featureName: string): any {
        try {
            const appFeatures: any[] = this.appConfig.appInfo.applicationConfig.appFeatures;
            for (const feature of appFeatures) {
                if ('object' !== typeof feature) {
                    continue;
                }
                if (featureName === feature.name) {
                    return feature;
                }
            }
        } catch (e) {
            return;
        }
    }

    public isModuleEnabled(featureName: string): boolean {
        const feature = this.getAppFeatureSettings(featureName);
        try {
            if (this._commonUtilService.isObject(feature)) {
                return feature.enable === 'true' || feature.enable === true;
            } else {
                return false;
            }
        } catch(e) {
            return false;
        }
    }
    public getApplicationName() {
        return this.getConfig('name');
    }
    public getPromotionApproversListByRole() {
        return this.appConfig.appInfo.applicationConfig.appFeatures;
    }

    public setAppFeatureSettings(featureName: string, settings: any): void {
        try {
            const appFeatures: any[] = this.appConfig.appInfo.applicationConfig.appFeatures;
            for (let i = 0; i < appFeatures.length; i++) {
                if (featureName === appFeatures[0].name) {
                    this.appConfig.appInfo.applicationConfig.appFeatures[i].settings = settings;
                    break;
                }
            }
        } catch (e) {
            return;
        }
    }

    public getTenantId() {
        return this.appConfig.tenantId;
    }

    public loadAppConfig() {
        this._setEnvSettings();
        // this._setLanguageSettings();
        let apiPath = '/platform-service/api/v1/application/appconfig/domain';
        if (this.isLocal()) {
            apiPath = this.getProtocol() + '//'
                + this.getHostName()
                + '/platform-service/api/v1/application/appconfig/domain';
        }
        return new Promise((resolve) => {
            this._http.get(apiPath)
                .map((res) => res.json())
                .catch((error: any): any => {
                    console.log('Application configuration could not be loaded');
                    resolve(true);
                    return Observable.throw(error.json().error || 'Server error');
                })
                .subscribe((appConfig) => {
                    this.resolve = resolve;
                    this.appConfig = appConfig;
                    this.setMultiMarketConfig(appConfig.appInfo.applicationConfig);
                    // resolve(true);
                });
        });
    }

    public getHostName() {
        return this.isLocal() ? 'dimnsauat.phasezero.xyz' : window.location.hostname;
    }
    public isLocal() {
        return window.location.hostname === 'localhost';
    }
    public getDomain() {
        return this.getHostName() + '/promotion';
    }
    public getProtocol() {
        return this.isLocal() ? 'https:' : window.location.protocol;
    }
    public getCdnBaseUrl() {
        return this.getConfig('appInfo').applicationConfig.cdnBaseurl;
    }

    private _setEnvSettings(): void {
        this.envSettings = {
            protocol: this.getProtocol(),
            hostName: this.getHostName()
        };
    }

    private _setLanguageSettings() {
        this._translateSvc.addLangs(['en', 'fr']);
        this._translateSvc.setDefaultLang('en_us');
        const browserLang = this._translateSvc.getBrowserLang();
        const LANG = browserLang.match(/en|fr/) ? browserLang : 'en';
        this._translateSvc.use(LANG + '_us');
    }

    public setMultiMarketConfig(config): void {
        const multiMarketConfig = config.micrositeConfig;
        const microsites = multiMarketConfig.microsites;
        let foundDefaultLag = false;

        for (const microsite of microsites) {
            if (microsite && 'object' === typeof microsite && Object.keys(microsite).length > 0) {
                let countryCode = microsite.name;

                if (countryCode === multiMarketConfig.defaultMicrosite) {
                    foundDefaultLag = true;
                    this.setSelectedCountry(microsite);
                    break;
                }
            } else {
                this._setLanguageSettings();
                this.resolve(true);
            }
        }

        if (!foundDefaultLag) {
            this._setLanguageSettings();
            this.resolve(true);
        }
    }

    private setSelectedCountry(microsite): void {
        this.defaultLocaleId = microsite.defaultLocale || 'en_US';
        const FILE_NAME = this.defaultLocaleId.toLowerCase();

        this._translateSvc.getTranslation(FILE_NAME).subscribe(res=> {
            this._translateSvc.use(FILE_NAME);
            this.resolve(true);

        }, error => {
            this._setLanguageSettings();
            this.resolve(true);
        });
    }
}
