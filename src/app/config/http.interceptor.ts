/*****************************************************************
 * Proprietary & Confidential  |  © 2017 PhaseZero Ventures LLC  *
 * This is part of PhaseZero Ventures LLC and cannot be copied,  *
 * modified and/or distributed without the express permission of *
 * PhaseZero Ventures LLC                                        *
 *****************************************************************/
/**
 * @author: Irshadahmed
 */
import {
    Injectable
} from '@angular/core';
import {
    ConnectionBackend,
    RequestOptions,
    Request,
    RequestOptionsArgs,
    Response,
    Http,
    Headers
} from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { EventEmitterService } from '../core/event-emitter/';
import { AppConfig } from './app.config';

@Injectable()
export class HttpInterceptor extends Http {
    private _pendingRequestCount: number = 0;
    constructor(
        backend: ConnectionBackend,
        defaultOptions: RequestOptions,
        private _eventEmitter: EventEmitterService,
        private _appConfig: AppConfig
    ) {
        super(backend, defaultOptions);
    }

    public request(url: string | Request, options?: RequestOptionsArgs): Observable<Response> {
        if (0 >= this._pendingRequestCount) {
            const showLoader = true; // Show the loading spinner on the first request
            this._eventEmitter.onLoaderToggle.emit(showLoader);
        }
        ++this._pendingRequestCount;
        return this._intercept(super.request(url, this._getRequestOptionArgs(options)));
    }

    public get(url: string, options?: RequestOptionsArgs): Observable<Response> {
        return super.get(url, this._getRequestOptionArgs(options));
    }

    public post(url: string, body: string, options?: RequestOptionsArgs): Observable<Response> {
        return super.post(url, body, this._getRequestOptionArgs(options));
    }

    public put(url: string, body: string, options?: RequestOptionsArgs): Observable<Response> {
        return super.put(url, body, this._getRequestOptionArgs(options));
    }

    public delete(url: string, options?: RequestOptionsArgs): Observable<Response> {
        return super.delete(url, this._getRequestOptionArgs(options));
    }

    private _getRequestOptionArgs(options?: RequestOptionsArgs): RequestOptionsArgs {
        if (options == null) {
            options = new RequestOptions();
        }
        if (options.headers == null) {
            options.headers = new Headers();
        }
        const bearerToken: string = localStorage.getItem('pilm_bearerToken');
        if (bearerToken) {
            options.headers.append('Authorization', localStorage.getItem('pilm_bearerToken'));
        }
        const domain: string = window.location.hostname.replace('www.', '');

        // options.headers.append('domain', 'localhost' === domain ? 'cxdemo.phasezero.xyz/promotion' : (domain + '/promotion') );
        if (!options.headers.has('application')) {
            // options.headers.append('domain', this._appConfig.getDomain());
            options.headers.append('domain', 'localhost' === domain ?
                'dimnsauat.phasezero.xyz/promotion' :
                (domain + '/promotion') );
        }
        return options;
    }

    private _intercept(observable: Observable<Response>): Observable<Response> {
        const _currentInstance = this;
        return observable.catch((err, source) => {
            if (401 === err.status) {
                this._eventEmitter.onSessionExpired.emit(err);
                // this._router.navigate(['/login']);
                return Observable.empty();
            }
            return Observable.of(err);
        }).finally(() => {
            // After the request;
            --_currentInstance._pendingRequestCount;
            if (0 >= _currentInstance._pendingRequestCount) {
                const showLoader = false; // Hide the loading spinner on the last request
                _currentInstance._pendingRequestCount = 0;
                _currentInstance._eventEmitter.onLoaderToggle.emit(showLoader);
            }
        });

    }
}
