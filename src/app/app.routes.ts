/*****************************************************************
 * Proprietary & Confidential  |  © 2017 PhaseZero Ventures LLC  *
 * This is part of PhaseZero Ventures LLC and cannot be copied,  *
 * modified and/or distributed without the express permission of *
 * PhaseZero Ventures LLC                                        *
 *****************************************************************/
/**
 * @author: Irshadahmed
 */
import { Routes } from '@angular/router';
import { AboutComponent } from './about';
import {
    DirectivesComponent
} from './components/directives.component';
import { NoContentComponent } from './no-content';

export const ROUTES: Routes = [
    { path: 'about', component: AboutComponent },
    { path: 'dir-comp', component: DirectivesComponent },
    /* { path: 'detail', loadChildren: './+detail#DetailModule' },
    { path: 'barrel', loadChildren: './+barrel#BarrelModule' }, */
    { path: '**', component: NoContentComponent },
];
