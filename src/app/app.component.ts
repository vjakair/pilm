/*****************************************************************
 * Proprietary & Confidential  |  © 2017 PhaseZero Ventures LLC  *
 * This is part of PhaseZero Ventures LLC and cannot be copied,  *
 * modified and/or distributed without the express permission of *
 * PhaseZero Ventures LLC                                        *
 *****************************************************************/
/**
 * @author: Irshadahmed
 */
/**
 * Angular 2 decorators and services
 */
import {
    Component,
    Input,
    OnInit,
    Output,
    ViewEncapsulation,
    ChangeDetectorRef
} from '@angular/core';
import {
    AppState
} from './app.service';
import {
    AuthService
} from './auth/auth.service';
import {
    EventEmitterService
} from './core/event-emitter';

/**
 * App Component
 * Top Level Component
 */
@Component({
    selector: 'app',
    encapsulation: ViewEncapsulation.None,
    styleUrls: [
        './app.component.scss'
    ],
    template: `
    <alert></alert>
    <loading-spinner></loading-spinner>
    <header [isLoggedIn]="isLoggedIn" class="navbar-fixed-top app-header"></header>
    <main resizeContainer  class="clearfix" [ngClass]="{'full-screen': overlayLoaded}">
      <router-outlet></router-outlet>
      <side-nav  [isLoggedIn]="isLoggedIn"></side-nav>
    </main>
    <footer class="app-footer"></footer>
  `
})
export class AppComponent implements OnInit {
    
    public promoDetails: any;
    @Output()
    public isLoggedIn: boolean;
    public overlayLoaded: boolean;
    constructor(
        public appState: AppState,
        authService: AuthService,
        eventEmitter: EventEmitterService,
        private _changeRef: ChangeDetectorRef
    ) {
        this.isLoggedIn = authService.isAuthenticated;
        eventEmitter
            .onUserLoginStateChange
            .subscribe(
            (respose) => this._setLoginStatus(respose)
            );
        eventEmitter
            .onOverlayLoaded
            .subscribe(
            (overlayLoaded) => this.overlayLoaded = overlayLoaded
            );
    }

    public ngOnInit() {
        console.log('App Component initialized.');
    }

    @Output()
    public overlayClosed() {
        setTimeout(() => {
            console.log('closing....', this);
        }, 3000);
    }

    private _setLoginStatus(isUserLogged) {
        this.isLoggedIn = isUserLogged;
        this._changeRef.detectChanges();
    }
}
